#pragma once
class CObj
{
protected:
	


	HDC m_hDC;
	INFO	m_tInfo;
	INFO	m_tCollisionInfo;
	RECT	m_tRect;
	ITEMINFO m_tItemInfo;
	RECT	m_tCollisionRect;

	FRAME	m_tFrame;

	bool	m_bIsInit;
	bool	m_bIsDead;
	float	m_fSpeed;

	float	m_fJumpPow;	// 점프	힘(v)
	float	m_fJumpAcc;	// 점프 가속도(t)
	bool	m_bIsJump;
	bool	m_bDir;

	bool m_bStateLock;//행동을 잠그기위한 변수 
	bool m_bIsGround;

	CObjMgr::OBJECTLAYER	m_eLayer;

public:


	enum ITEM_EQUIPMENT { EQUIP, UNEQUIP, EQUIP_END };
	enum ITEM_TYPE { eMONEY, eHPPOTION, eMPPOTION, eWEAPON, eWEAR, eGLOVES, eSHOES, eEND };

	bool& GetIsGround() { return m_bIsGround; }
	void SetIsGround(bool bIsGround) { m_bIsGround = bIsGround;}
	const FRAME& GetFrame() { return m_tFrame; };
	const INFO& GetInfo() { return m_tInfo; };
	const INFO& GetCollisionInfo() { return m_tCollisionInfo; }
	const ITEMINFO& GetItemInfo() { return m_tItemInfo; }
	const RECT& GetRect() const { return m_tRect; }
	const RECT& GetCollisionRect() { return m_tCollisionRect; }
	void IsDead() { m_bIsDead = true; }
	CObjMgr::OBJECTLAYER GetLayer() { return m_eLayer; }

	const ITEM_TYPE  GetItemType() { return m_eType; }
	const ITEM_EQUIPMENT GetItemEquip() { return m_eEquipment; }
public:	
	CObj();
	virtual ~CObj();
public:
	void SetItemType(int iType) { m_eType = (CObj::ITEM_TYPE)iType; }
	void SetItemEquip(ITEM_EQUIPMENT eEquip) { m_eEquipment = eEquip; }
	void SetDir(bool bDir) { m_bDir = bDir; }
	void SetPos(float fX, float fY) { m_tInfo.fX = fX, m_tInfo.fY = fY; }
	virtual void Initialize() = 0;
	virtual void LateInit();
	void UpdateRect();
	void UpdateCollisionRect(float Lx, float Ty, float Rx, float Ry);
	void UpdateCollisionRect(float x, float y);
	void MoveFrame();
	void EffectFrame();
	virtual int Update() = 0;
	virtual void LateUpdate() = 0;
	virtual void Render(HDC hDC) = 0;
	virtual void Release() = 0;
protected:
	ITEM_TYPE m_eType;
	ITEM_EQUIPMENT m_eEquipment;
	HBRUSH		Brush, oBrush;
	PAINTSTRUCT	ps;
};

