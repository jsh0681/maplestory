#include "stdafx.h"
#include "TileMgr.h"
#include "Tile.h"

TILEVEC m_vecTile;
IMPLEMENT_SINGLETON(CTileMgr)

CTileMgr::CTileMgr()
{
}


CTileMgr::~CTileMgr()
{
	Release();
}

void CTileMgr::Initialize()
{
	m_vecTile.reserve(TILEX * TILEY);

	float fX = 0.f, fY = 0.f;
	CTile* pTile = nullptr;

	for (int i = 0; i < TILEY; ++i)
	{
		for (int j = 0; j < TILEX; ++j)
		{
			fX = j * TILECX + TILECX * 0.5f;
			fY = i * TILECY + TILECY * 0.5f;

			pTile = new CTile;
			pTile->Initialize();
			pTile->SetPos(fX, fY);

			m_vecTile.push_back(pTile);
		}
	}
}

void CTileMgr::Update()
{
	// 스크롤 잠금
	if (0.f > CScrollMgr::GetScrollX())
		CScrollMgr::SetScrollX(0.f);
	
	if (0.f > CScrollMgr::GetScrollY())
		CScrollMgr::SetScrollY(0.f);

	if (TILECX * TILEX - WINCX < CScrollMgr::GetScrollX())
		CScrollMgr::SetScrollX(TILECX * TILEX - WINCX);
	
	if (TILECY * TILEY - WINCY < CScrollMgr::GetScrollY())
		CScrollMgr::SetScrollY(TILECY * TILEY - WINCY);
}

void CTileMgr::Render(HDC hDC)
{
	// 컬링
	// 시야 범위 안에 들어오는 오브젝트들에 대해서만 연산처리하는 최적화 기법.
	int iCullX = int(CScrollMgr::GetScrollX()) / TILECX;
	int iCullY = int(CScrollMgr::GetScrollY()) / TILECY;

	int iCullEndX = iCullX + WINCX / TILECX;
	int iCullEndY = iCullY + WINCY / TILECY;

	for (int i = iCullY; i < iCullEndY + 2; ++i)
	{
		for (int j = iCullX; j < iCullEndX + 2; ++j)
		{
			int iIndex = j + (TILEX * i);

			if (0 > iIndex || m_vecTile.size() <= (size_t)iIndex)
				continue;

			m_vecTile[iIndex]->Render(hDC);
		}
	}
}

void CTileMgr::Release()
{
	for_each(m_vecTile.begin(), m_vecTile.end(), SafeDelete<CTile*>);

	m_vecTile.clear();
	m_vecTile.shrink_to_fit();
}

void CTileMgr::TileChange(const POINT& pt, int iDrawID)
{
	int x = pt.x / TILECX;
	int y = pt.y / TILECY;

	int iIndex = x + (TILEX * y);

	if (0 > iIndex || m_vecTile.size() <= (size_t)iIndex)
		return;

	m_vecTile[iIndex]->SetDrawID(iDrawID);
}

void CTileMgr::TileChange(const POINT& pt, int iDrawID,int iOption)
{
	int x = pt.x / TILECX;
	int y = pt.y / TILECY;

	int iIndex = x + (TILEX * y);

	if (0 > iIndex || m_vecTile.size() <= (size_t)iIndex)
		return;

	m_vecTile[iIndex]->SetDrawID(iDrawID);
	m_vecTile[iIndex]->SetOption(iOption);

}
void CTileMgr::SaveData()
{

	HANDLE hFile = CreateFile(L"../Data/Boss.dat", GENERIC_WRITE, 0, 0,
		CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		MessageBox(g_hWnd, L"Tile Save Failed!", L"Error", MB_OK);
		return;
	}

	DWORD dwByte = 0;

	for (auto& pTile : m_vecTile)
		WriteFile(hFile, &(pTile->GetInfo()), sizeof(TILEINFO), &dwByte, nullptr);

	CloseHandle(hFile);
}


void CTileMgr::LoadTownData()
{
	HANDLE hFile = CreateFile(L"../Data/Town.dat", GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		MessageBox(g_hWnd, L"Tile Load Failed!", L"Error", MB_OK);
		return;
	}

	DWORD dwByte = 0;
	TILEINFO tInfo = {};
	CTile* pTile = nullptr;

	if (!m_vecTile.empty())
		Release();

	while (true)
	{
		ReadFile(hFile, &tInfo, sizeof(TILEINFO), &dwByte, nullptr);

		if (0 == dwByte)
			break;

		pTile = new CTile;
		pTile->Initialize();
		pTile->SetInfo(tInfo);

		m_vecTile.push_back(pTile);
	}

	CloseHandle(hFile);
}


void CTileMgr::LoadDungeonData()
{
	HANDLE hFile = CreateFile(L"../Data/Dungeon.dat", GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		MessageBox(g_hWnd, L"Tile Load Failed!", L"Error", MB_OK);
		return;
	}

	DWORD dwByte = 0;
	TILEINFO tInfo = {};
	CTile* pTile = nullptr;

	if (!m_vecTile.empty())
		Release();

	while (true)
	{
		ReadFile(hFile, &tInfo, sizeof(TILEINFO), &dwByte, nullptr);

		if (0 == dwByte)
			break;

		pTile = new CTile;
		pTile->Initialize();
		pTile->SetInfo(tInfo);

		m_vecTile.push_back(pTile);
	}

	CloseHandle(hFile);
}


void CTileMgr::LoadDungeonTwoData()
{
	HANDLE hFile = CreateFile(L"../Data/DungeonTwo.dat", GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		MessageBox(g_hWnd, L"Tile Load Failed!", L"Error", MB_OK);
		return;
	}

	DWORD dwByte = 0;
	TILEINFO tInfo = {};
	CTile* pTile = nullptr;

	if (!m_vecTile.empty())
		Release();

	while (true)
	{
		ReadFile(hFile, &tInfo, sizeof(TILEINFO), &dwByte, nullptr);

		if (0 == dwByte)
			break;

		pTile = new CTile;
		pTile->Initialize();
		pTile->SetInfo(tInfo);

		m_vecTile.push_back(pTile);
	}

	CloseHandle(hFile);
}


void CTileMgr::LoadBossData()
{
	HANDLE hFile = CreateFile(L"../Data/Boss.dat", GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		MessageBox(g_hWnd, L"Tile Load Failed!", L"Error", MB_OK);
		return;
	}

	DWORD dwByte = 0;
	TILEINFO tInfo = {};
	CTile* pTile = nullptr;

	if (!m_vecTile.empty())
		Release();

	while (true)
	{
		ReadFile(hFile, &tInfo, sizeof(TILEINFO), &dwByte, nullptr);

		if (0 == dwByte)
			break;

		pTile = new CTile;
		pTile->Initialize();
		pTile->SetInfo(tInfo);

		m_vecTile.push_back(pTile);
	}

	CloseHandle(hFile);
}