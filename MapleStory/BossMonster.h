#pragma once
#include "Obj.h"

class CBossMonster :
	public CObj
{
	enum BODYSTATE { eBFIRE, eBSUN, eBICE, eBTHUNDER, eBCURSE, eSKILLEND};
	enum RHANDSTATE { eRTHUNDER, eRFIRE, eRCURSE, eRIDLE};
	enum LHANDSTATE { eLICE, eLSUN, eLIDLE };

public:
	CBossMonster();
	virtual ~CBossMonster();
public:
	// CObj을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual void LateInit() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;

	void DrawHpBar(HDC hDC);

public://겟함수
	const bool GetRigidity() { return m_bRigidity; }//경직 불값

	void DrawBossBody(HDC hDC);
	void DrawBossLHand(HDC hDC);
	void DrawBossRHand(HDC hDC);
public://셋함수
	void SetIsNear(bool bIsNear) { m_bIsNear = bIsNear; }
	void SetRigidity() { m_bRigidity = true; }
	void SetDamage(float fDamage) { m_tBossInfo.fHp -= fDamage; }
	const float GetHp() { return m_tBossInfo.fHp; }
public://이동및 상태 변화 함수 
	void BodyChange();
	void LHandChange();
	void RHandChange();
	void MoveBodyFrame();
	void MoveLHandFrame();
	void MoveRHandFrame();
	void UpdateBodyRect();
	void UpdateLHandRect();
	void UpdateRHandRect();
	void RandMove();
private:
	bool m_bRigidity;//경직불 값
	bool m_bIsNear;
	bool m_bDirChange;	
	
	template<typename T>
	CObj* CreateSkill(int iSkillNum)
	{
		return CAbstactFactory<T>::CreateBossSkill(m_pPlayer->GetInfo().fX, m_pPlayer->GetInfo().fY, iSkillNum, m_tBossInfo.fAtk);
	}
private:
	
	FRAME	m_tBodyFrame;
	FRAME	m_tLHandFrame;
	FRAME	m_tRHandFrame;

	DWORD m_dwStart;
	DWORD m_dwRigidity;

	wstring m_strFrameBody;
	BODYSTATE m_eCurBody;//현재 상태
	BODYSTATE m_ePreBody;//이전 상태


	wstring m_strFrameLHand;
	LHANDSTATE m_eCurLHand;//현재 상태
	LHANDSTATE m_ePreLHand;//이전 상태


	wstring m_strFrameRHand;
	RHANDSTATE m_eCurRHand;//현재 상태
	RHANDSTATE m_ePreRHand;//이전 상태

	OBJINFO m_tBossInfo; 
	OBJINFO m_tBossLHandInfo;
	OBJINFO m_tBossRHandInfo;

	RECT m_tLHandRect;
	RECT m_tRHandRect;

	INFO m_tLHandInfo;
	INFO m_tRHandInfo;

	CObj* m_pPlayer;
};

