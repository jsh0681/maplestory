#pragma once
class CLine
{
public:
	CLine();
	CLine(const LINEINFO& info);
	~CLine();

public:
	const LINEINFO& GetInfo() { return m_tInfo; }

public:
	void Render(HDC hDC);

private:
	LINEINFO m_tInfo;
};

