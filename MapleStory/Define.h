#pragma once

#define WINCX 800
#define WINCY 600

#define ATTACK 0
#define BYOEKRYEOK 1
#define NOESEONG 2
#define SEOMMYEOL  3
#define TAEPUNG 4
#define SEUNGCHUN 5


#define NO_EVENT 0
#define DEAD_OBJ 1

#define FACE_LEFT 0
#define FACE_RIGHT 1

#define TILECX 20
#define TILECY 20

#define TILEX 90
#define TILEY 75

#define TOWN_X 800
#define TOWN_Y 600

#define TOWN_X 800
#define TOWN_Y 600

#define DUNGEON_X 1495
#define DUNGEON_Y 1300

#define DUNGEON_X2 1750
#define DUNGEON_Y2 1450

#define BOSS_DUNGEON_X 1382
#define BOSS_DUNGEON_Y 600






#define WALL 0
#define RADDER 1
#define	BACK 2

#define SKILL_CONST_MP  10


#define REMOVE_PINK RGB(255.f, 0.f, 255.f)
#define REMOVE_WHITE RGB(0.f, 0.f, 0.f)
#define REMOVE_BLUE RGB(0.f, 0.f, 255.f)
#define GRAVITY 9.81f
#define JUMP_TOP 80.f
#define NULL_CHECK(PTR)  if(nullptr==(PTR)) return;
#define NAME_LEN 16

#define BOSS_BODYX 700
#define BOSS_BODYY 700
#define BOSS_LHANDX 300
#define BOSS_LHANDY 300
#define BOSS_RHANDX 360
#define BOSS_RHANDY 300


#define DECLARE_SINGLETON(ClassName)		\
public:										\
	static ClassName* GetInstance()			\
	{										\
		if (nullptr == m_pInstance)			\
			m_pInstance = new ClassName;	\
		return m_pInstance;					\
	}										\
	void DestroyInstance()					\
	{										\
		if (m_pInstance)					\
		{									\
			delete m_pInstance;				\
			m_pInstance = nullptr;			\
		}									\
	}										\
private:									\
static ClassName*	m_pInstance;

#define IMPLEMENT_SINGLETON(ClassName)		\
ClassName*	ClassName::m_pInstance = nullptr;


