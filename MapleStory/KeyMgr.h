#pragma once

#define MAX_KEY 0x100

class CKeyMgr
{
	DECLARE_SINGLETON(CKeyMgr)

private:
	bool m_bKeyState[MAX_KEY];
	
public:

	bool KeyDown(DWORD dwKey);
	bool KeyPressing(DWORD dwKey);
	bool KeyUp(DWORD dwKey);

	void UpdateKey();

	
private:
	CKeyMgr();
	~CKeyMgr();
};

