#pragma once
#include "Obj.h"
class CSkill;
class CInven;


class CPlayer :
	public CObj
{
	enum STATE { eIDLE, eWALK, eJUMP, eDOWN, eSKILL, eSKILL2, eATTACK, eATTACKREST, eDOWN_ATTACK, eUP1, eUP2, eBYEOKRYEOK, eSIT, eEND };
public:

	void CPlayer::SetInven(CObj * pItem)
	{
		pItem->SetPos((float)(402 + 36 * (g_iItemCount % 4)),(float)( 102 + (35 * (g_iItemCount / 4))));
		m_vecInven.push_back(pItem);
		g_iItemCount++;
	}

	CPlayer();
	virtual ~CPlayer();

	// CObj을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual void LateInit() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;

public:
	void LevelUp();
	void ScrollLock();
	void SceneChange();
	void DrawInven(HDC hDC);
	void DrawEquip(HDC hDC);
	void DrawStat(HDC hDC);
	void KeyCheck();
public:
	float fJumpCount;
	void IsJumping();
	void IsAtkMotioning();
	void IsFalling();


	void InvenToEquip(CObj * pItem);


	void EquipToInven(CObj * pItem);


	OBJVEC& GetInven() { return m_vecInven; }
	OBJVEC& GetEquipment() { return m_vecEquipment; }


public:
	template<typename T>
	CObj* CreateSkill(int iSkillNum,bool bDir)
	{
		return CAbstactFactory<T>::CreateObj(m_tInfo.fX, m_tInfo.fY, iSkillNum, bDir, m_tPlayerInfo.fAtk);
	}
public://값을 반환해주는 겟함수

	const OBJINFO& GetPlayerInfo() { return m_tPlayerInfo; }
	const float GetMoney() { return m_tPlayerInfo.fMoney; }
	const float GetDamage() { return m_tPlayerInfo.fAtk; }//데미지 계산

	const float GetHp() { return m_tPlayerInfo.fHp; }//체력반환
	const float GetMaxHp() { return m_tPlayerInfo.fMaxHp; }//체력반환

	const float GetMp() { return m_tPlayerInfo.fMp; }//마나반환
	const float GetMaxMp() { return m_tPlayerInfo.fMaxMp; }//체력반환
	const float GetExp() { return m_tPlayerInfo.fExp; }
	const float GetMaxExp() { return m_tPlayerInfo.fMaxExp; }

	const bool GetRigidity() { return m_bRigidity; }//경직 불값

	const bool& GetEat() { return m_bIsEat; }
	const bool& GetHang() { return m_bIsHang; }

public://값을 바꿔주는 셋함수
	
	void SetSkillList(OBJLIST* pSkillList) { m_pSkillList = pSkillList; }
	void SetMoney(float fMoney) { m_tPlayerInfo.fMoney += fMoney; }
	void SetFalling(bool bFalling) { m_bFalling = bFalling; }
	void SetHang(bool bHang) { m_bIsHang = bHang; }
	void SetExp(float fExp) { m_tPlayerInfo.fExp += fExp; }
	void SetRigidity() { m_bRigidity = true; }
	void SetDamage(float fDamage) { m_tPlayerInfo.fHp -= fDamage; }
	void SetInfo(float fX, float fY) { m_tInfo.fX = fX, m_tInfo.fY = fY; }
	void SetSale(float fPrice) { m_tPlayerInfo.fMoney -= fPrice; }
	bool GetClickItem() { return m_bClickItem; };
	void SetClicKItem(bool bClickItem) { m_bClickItem = bClickItem; }
public:
private:
	int m_iEquipSize;
	int m_iInvenSize;
	OBJVEC m_vecInven;
	OBJVEC m_vecEquipment;
	
	bool m_bClickItem;

	bool m_bHangMoving;
	bool m_bFalling;
	bool m_bStayHang;
	bool m_bHangUp;
	bool m_bHangDown;



	bool m_bAtkMotion;
	bool m_bIsHang;
	bool m_bAttack;
	bool m_bRigidity;//경직불 값
	bool m_bIsUp;
	bool m_bIsDown;
	bool m_bIsEat;
	float m_fJumpSpeed;
	OBJLIST* m_pSkillList;
	OBJINFO m_tPlayerInfo;
protected:

	DWORD m_dwStart;
	DWORD dwAtkCount;
	STATE m_eCurState;//현재 상태
	STATE m_ePreState;//이전 상태
	wstring m_strFrameKey;
};

