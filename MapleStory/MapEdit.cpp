#include "stdafx.h"
#include "MapEdit.h"
#include"Mouse.h"


CMapEdit::CMapEdit()
{
}


CMapEdit::~CMapEdit()
{
	Release();
}

void CMapEdit::Initialize()
{
	//CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Map/Town.bmp", L"Town");
	//CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Map/Dungeon.bmp", L"Dungeon");
	//CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Map/DungeonTwo.bmp", L"DungeonTwo");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Map/BossDungeon.bmp", L"BossDungeon");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/TileRadder5.bmp", L"Tile");

	CTileMgr::GetInstance()->Initialize();
}

void CMapEdit::Update()
{
	KeyCheck();
	CTileMgr::GetInstance()->Update();
}

void CMapEdit::LateUpdate()
{
}

void CMapEdit::Render(HDC hDC)
{
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();

	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"BossDungeon");
	NULL_CHECK(hMemDC);
	BitBlt(hDC, 0, 0, BOSS_DUNGEON_X, BOSS_DUNGEON_Y, hMemDC, (int)fScrollX, (int)fScrollY, SRCCOPY);


		
	/*HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Town");
	NULL_CHECK(hMemDC);
	BitBlt(hDC, 0, 0, TOWN_X, TOWN_Y, hMemDC, (int)fScrollX, (int)fScrollY, SRCCOPY);*/

	
	/*HDC  hMemDC = CBmpMgr::GetInstance()->FindImage(L"Dungeon");
	NULL_CHECK(hMemDC);
	BitBlt(hDC, 0, 0, DUNGEON_X, DUNGEON_Y, hMemDC, (int)fScrollX, (int)fScrollY, SRCCOPY);
	*/
	
	
	/*HDC  hMemDC = CBmpMgr::GetInstance()->FindImage(L"DungeonTwo");
	NULL_CHECK(hMemDC);
	BitBlt(hDC, 0, 0, DUNGEON_X2, DUNGEON_Y2, hMemDC, (int)fScrollX, (int)fScrollY, SRCCOPY);*/
	

	CTileMgr::GetInstance()->Render(hDC);
}

void CMapEdit::Release()
{
	CTileMgr::GetInstance()->DestroyInstance();
}

void CMapEdit::KeyCheck()
{
	if (CKeyMgr::GetInstance()->KeyPressing(VK_LEFT))
		CScrollMgr::MoveScrollX(-5.f);
	if (CKeyMgr::GetInstance()->KeyPressing(VK_RIGHT))
		CScrollMgr::MoveScrollX(5.f);
	if (CKeyMgr::GetInstance()->KeyPressing(VK_UP))
		CScrollMgr::MoveScrollY(-5.f);
	if (CKeyMgr::GetInstance()->KeyPressing(VK_DOWN))
		CScrollMgr::MoveScrollY(5.f);

	if (CKeyMgr::GetInstance()->KeyPressing(VK_LBUTTON))
	{
		POINT pt = CMouse::GetMousePos();
		CTileMgr::GetInstance()->TileChange(pt, 1,WALL);
	}
	
	if (CKeyMgr::GetInstance()->KeyPressing(VK_RBUTTON))
	{
		POINT pt = CMouse::GetMousePos();
		CTileMgr::GetInstance()->TileChange(pt, 2, RADDER);
	}
	if (CKeyMgr::GetInstance()->KeyPressing('A'))
	{
		POINT pt = CMouse::GetMousePos();
		CTileMgr::GetInstance()->TileChange(pt, 1, BACK);
	}
	if (CKeyMgr::GetInstance()->KeyDown('S'))
	{
		CTileMgr::GetInstance()->SaveData();
	}
	if (CKeyMgr::GetInstance()->KeyDown('P'))
	{
		CSceneMgr::GetInstance()->SceneChange(CSceneMgr::MENU);
	}

}
