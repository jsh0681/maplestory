#include "stdafx.h"
#include "Inven.h"

bool g_bInvenWindowOpen = false;

CInven::CInven()
{
}


CInven::~CInven()
{
	Release();
}

void CInven::Initialize()
{
	m_eLayer = CObjMgr::LAYER_UI;
	m_tInfo = { 0,0,172,335 };
	m_tItemInfo = { 0,0,32,32 };
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Ui/Inven.bmp", L"Inven");

}
void CInven::KeyCheck()
{
	if (!g_bInvenWindowOpen)
	{
		if (CKeyMgr::GetInstance()->KeyUp('I'))//인벤창
		{
			CSoundMgr::GetInstance()->PlaySoundw(L"Window_Open.mp3", CSoundMgr::UI);
			g_bInvenWindowOpen = true;
		}
	}
	else
	{
		if (CKeyMgr::GetInstance()->KeyUp('I'))//인벤창
		{
			CSoundMgr::GetInstance()->PlaySoundw(L"Window_Close.mp3", CSoundMgr::UI);
			g_bInvenWindowOpen = false;
		}
	}
}
void CInven::DrawInven(HDC hDC)
{
	if (g_bInvenWindowOpen)
	{
		HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Inven");
		NULL_CHECK(hMemDC);
		GdiTransparentBlt(hDC, 375, 31, m_tInfo.fCX, m_tInfo.fCY, hMemDC, 0, 0, m_tInfo.fCX, m_tInfo.fCY, REMOVE_PINK);
	}
}
void CInven::LateInit()
{
}

int CInven::Update()
{
	CObj::LateInit();
	KeyCheck();
	return 0;
}

void CInven::LateUpdate()
{
}

void CInven::Render(HDC hDC)
{
	CObj::UpdateRect();
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();
	DrawInven(hDC);
}

void CInven::Release()
{
}


