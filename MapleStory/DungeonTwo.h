#pragma once
#include "Scene.h"

class CDungeonTwo :
	public CScene
{
public:
	CDungeonTwo();
	virtual ~CDungeonTwo();

	// CScene을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void LateUpdate() override;
	void LockScroll();
	virtual void Render(HDC hDC) override;
	virtual void Release() override;
};

