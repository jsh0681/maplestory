#include "stdafx.h"
#include "BmpMgr.h"
#include "MyBmp.h"

IMPLEMENT_SINGLETON(CBmpMgr)

CBmpMgr::CBmpMgr()
{
}


CBmpMgr::~CBmpMgr()
{
	Release();
}

HDC CBmpMgr::FindImage(const TCHAR* pImageKey)
{
	auto& iter_find = find_if(m_MapBit.begin(), m_MapBit.end(), [&pImageKey](auto& MyPair) 
	{
		return !lstrcmp(pImageKey, MyPair.first);
	});
	if (m_MapBit.end() == iter_find)
		return nullptr;

	return iter_find->second->GetMemDC();
}


void CBmpMgr::InsertBmp(const TCHAR * pFilePath, const TCHAR * pImageKey)
{
	auto& iter_find = find_if(m_MapBit.begin(), m_MapBit.end(),
		[&pImageKey](auto& MyPair) 
	{
		return !lstrcmp(pImageKey, MyPair.first); 
	});

	if (m_MapBit.end() == iter_find)
	{
		CMyBmp* pBmp = new CMyBmp;
		pBmp->LoadBmp(pFilePath);
		m_MapBit.insert({ pImageKey,pBmp });
	}

}

void CBmpMgr::Release()
{
	for_each(m_MapBit.begin(), m_MapBit.end(), [](auto& MyPair) {
		SafeDelete(MyPair.second); 
	});
	m_MapBit.clear();
}

bool g_bClick[14][10] = {};


void CBmpMgr::LoadTile()
{
	HANDLE hFile = CreateFile(L"../Tile.dat", GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		MessageBox(g_hWnd, L"불러오기 실패!", L"Error", MB_OK);
		return;
	}

	DWORD dwByte = 0;
	while (true)
	{
		ReadFile(hFile, &g_bClick, sizeof(g_bClick), &dwByte, nullptr);
		if (0 == dwByte)//다 읽어와서 파일안에 읽어올 자료가 없을경우 
		{
			break;
		}
		
	}
	CloseHandle(hFile);
}
