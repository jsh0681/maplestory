#pragma once
#include "Obj.h"

class CBlueMushroom :
	public CObj
{
	enum STATE { eDead, eSlapped, eJump, eWalk, eStop, eEnd };
public:
	CBlueMushroom();
	virtual ~CBlueMushroom();
public:
	// CObj을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual void LateInit() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	void DrawHpBar(HDC hDC);
	virtual void Release() override;
public://겟함수
	const float GetDamage() { return m_tMonsterInfo.fAtk; }
	const float GetHp() { return m_tMonsterInfo.fHp; }
	const bool GetIsNear() { return m_bIsNear; }
	const float  GetExp() { return m_tMonsterInfo.fExp; }
	const bool GetRigidity() { return m_bRigidity; }//경직 불값
public://셋함수
	void SetDamage(float fDamage) { m_tMonsterInfo.fHp -= fDamage; }
	void SetState(int iState) { m_eCurState = (STATE)iState; }
	void SetIsNear(bool bIsNear) { m_bIsNear = bIsNear; }
	void SetRigidity() { m_bRigidity = true; }
public://이동및 상태 변화 함수 
	void RandMove();
	void Moving();
	void SceneChange();

private:
	bool m_bRigidity;//경직불 값
	bool m_bIsNear;
	bool m_bDirChange;
private:
	DWORD m_dwStart;
	DWORD m_dwRigidity;

	STATE m_eCurState;//현재 상태
	STATE m_ePreState;//이전 상태
	wstring m_strFrameKey;
	OBJINFO m_tMonsterInfo;
	CObj* m_pPlayer;
};

