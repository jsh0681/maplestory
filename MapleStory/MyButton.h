#pragma once
#include "Obj.h"
class CMyButton :
	public CObj
{
public:
	CMyButton();
	virtual ~CMyButton();

public:
	void SetFrameKey(const wstring& FrameKey) { m_wstrFrameKey = FrameKey; }

public:
	// CObj을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual void LateInit() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;

private:
	bool PtInSphere(const POINT& pt);

private:
	wstring m_wstrFrameKey;
	int		m_iDrawID;
};

