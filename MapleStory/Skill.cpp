#include "stdafx.h"
#include "Skill.h"


CSkill::CSkill() : m_eCurState(eEND), m_ePreState(eEND),m_dwStart(0), m_fSkillDamge(0.f)
{
}


CSkill::~CSkill()
{
	Release();
}

void CSkill::Initialize()
{
	m_eLayer = CObjMgr::LAYER_SKILL;


	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/ByeokRyeokRight.bmp", L"ByeokRyeokRight");//����
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/ByeokRyeokLeft.bmp", L"ByeokRyeokLeft");

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/NoeSeongRight.bmp", L"NoeSeongRight");//����
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/NoeSeongLeft.bmp", L"NoeSeongLeft");

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/SeungChunRight.bmp", L"SeungChunRight");//��õ
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/SeungChunLeft.bmp", L"SeungChunLeft");

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/TaePungRight.bmp", L"TaePungRight");//��ǳ
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/TaePungLeft.bmp", L"TaePungLeft");

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/SeomMyeolRight.bmp", L"SeomMyeolRight");//����
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/SeomMyeolLeft.bmp", L"SeomMyeolLeft");

	m_eCurState = eEND;
	m_ePreState = m_eCurState;
	

}

void CSkill::LateInit()
{
	m_dwStart = GetTickCount();
}

int CSkill::Update()
{
	CObj::LateInit();
	if (m_bIsDead)
		return DEAD_OBJ;
	return NO_EVENT;
}

void CSkill::LateUpdate()
{
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();
	m_eCurState = m_eSkillNum;
	SceneChange();
	CObj::EffectFrame();
}

void CSkill::Render(HDC hDC)
{
	CObj::UpdateRect();
	CObj::UpdateCollisionRect(100,300, 80, 0);

	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();


	//Rectangle(hDC, m_tCollisionRect.left - (int)fScrollX, m_tCollisionRect.top - (int)fScrollY, m_tCollisionRect.right - (int)fScrollX, m_tCollisionRect.bottom - (int)fScrollY);
	

	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(m_strFrameKey.c_str());
	NULL_CHECK(hMemDC);
	
	GdiTransparentBlt(hDC, m_tRect.left - (int)fScrollX, m_tRect.top - (int)fScrollY,
		(int)m_tInfo.fCX, (int)m_tInfo.fCY,
		hMemDC,
		m_tFrame.m_dwFrameStart * (int)m_tInfo.fCX,
		m_tFrame.m_dwFrameScene * (int)m_tInfo.fCY,
		(int)m_tInfo.fCX, (int)m_tInfo.fCY, REMOVE_PINK);
}

void CSkill::Release()
{
}


void CSkill::SceneChange()
{
	if (m_eCurState != m_ePreState)
	{
		switch (m_eCurState)
		{
		case eATTACK:
			m_tFrame.m_dwFrameScene = 0;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 3;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 100;
			break;

		case eBYOEKRYEOK://15���� 10950x427
			if (m_bDir == FACE_LEFT)
			{
				m_strFrameKey = L"ByeokRyeokLeft";

				m_tInfo.fX -= 15;
			}
			else if (m_bDir == FACE_RIGHT)
			{
				m_strFrameKey = L"ByeokRyeokRight";
				m_tInfo.fX += 15;
				
			}
			m_tInfo.fY -= 100;
			m_tInfo.fCX = 730;
			m_tInfo.fCY = 427;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 14;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 25;
			break;

		case eNOESEONG://10���� 5000x300
			if (m_bDir == FACE_LEFT)
				m_strFrameKey = L"NoeSeongLeft";
			else if (m_bDir == FACE_RIGHT)
				m_strFrameKey = L"NoeSeongRight";

			m_tInfo.fCX = 500;
			m_tInfo.fCY = 300;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 9;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 50;
			break;

		case eSEOMMYEOL://13  8190X230
			if (m_bDir == FACE_LEFT)
			{
				m_strFrameKey = L"SeomMyeolLeft";
				m_tInfo.fX -= 130;
			}
			else if (m_bDir == FACE_RIGHT)
			{
				m_strFrameKey = L"SeomMyeolRight";
				m_tInfo.fX += 130;
			}
			CObj::UpdateRect();
			CObj::UpdateCollisionRect(100, 200, 80, 130);
			m_tInfo.fY -= 60;
			m_tInfo.fCX = 630;
			m_tInfo.fCY = 230;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 12;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 60;
			break;

		case eTAEPUNG://21���� 16548x520 
			if (m_bDir == FACE_LEFT)
			{
				m_tInfo.fX -= 290;
				m_strFrameKey = L"TaePungLeft";
			}
			else if (m_bDir == FACE_RIGHT)
			{
				m_strFrameKey = L"TaePungRight";
				m_tInfo.fX += 290;
			}
			m_tInfo.fY -= 150;
			m_tInfo.fCX = 788;
			m_tInfo.fCY = 520;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 20;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 40;
			break;

		case eSEUNGCHUN://10���� 4000x400
			if (m_bDir == FACE_LEFT)
			{
				m_tInfo.fX -= 150;

				m_strFrameKey = L"SeungChunLeft";
			}
			else if (m_bDir == FACE_RIGHT)
			{
				m_strFrameKey = L"SeungChunRight";
				m_tInfo.fX += 150;
			}
			m_tInfo.fY -= 150;
			m_tInfo.fCX = 400;
			m_tInfo.fCY = 400;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 9;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 40;
			break;
		}
		m_ePreState = m_eCurState;

	}
}