#pragma once

// 추상 팩토리(Abstract Factory) 디자인 패턴
// 하나의 부모로부터 파생되는 객체들의 생성 문장들은 대게는 공통된다.
// 이러한 객체 생성에 공통된 문장들을 추상화하여 캡슐화한 디자인 패턴을 말한다.

class CObj;
class CUI;
class CSkill;
class CEffect;
class CBossSkill;
class CUI;
class CItem;

template <typename T>
class CAbstactFactory
{
public:
	static CObj* CreateObj()
	{
		CObj* pObj = new T;
		pObj->Initialize();

		return pObj;
	}




	static CObj* CreateItem(float x, float y,int iType)
	{
		CObj* pObj = new T;
		pObj->Initialize();
		pObj->SetPos(x, y); 
		dynamic_cast<CItem*>(pObj)->SetItemType(iType);
		return pObj;
	}

	static CObj* CreateObj(float x, float y, float iSkillNum)
	{
		CObj* pObj = new T;
		pObj->Initialize();
		pObj->SetPos(x, y);
		dynamic_cast<CEffect*>(pObj)->SetEnum((int)iSkillNum);
		return pObj;
	}

	static CObj* CreateItem(float x, float y,char* pName, char* pClass, float fHp, float fMaxHp, float fMp, float fMaxMp, float fAtk, float fMoney, int iType)
	{
		CObj* pObj = new T;
		dynamic_cast<CItem*>(pObj)->Create(pName, pClass, fHp,fMaxHp, fMp, fMaxMp, fAtk, fMoney, iType);
		pObj->Initialize();
		pObj->SetPos(x, y);
		return pObj;
	}

	static CObj* CreateEffect(float x, float y,int iDamage)
	{
		CObj* pObj = new T;
		pObj->Initialize();
		pObj->SetPos(x, y);
		dynamic_cast<CEffect*>(pObj)->SetDamage(iDamage);
		return pObj;
	}

	static CObj* CreateObj(float x, float y)
	{
		CObj* pObj = new T;
		pObj->Initialize();
		pObj->SetPos(x, y);
		return pObj;
	}
	static CObj* CreateObj(float x, float y, int iSkillNum,bool bDir, float fAtk)
	{
		CObj* pObj = new T;
		pObj->Initialize();
		pObj->SetDir(bDir);
		pObj->SetPos(x, y);
		dynamic_cast<CSkill*>(pObj)->SetEnum(iSkillNum);
		dynamic_cast<CSkill*>(pObj)->SetSkillDamage(fAtk);

		return pObj;
	}

	static CObj* CreateBossSkill(float x, float y, int iSkillNum, float fAtk)
	{
		CObj* pObj = new T;
		pObj->Initialize();
		pObj->SetPos(x, y);
		dynamic_cast<CBossSkill*>(pObj)->SetEnum(iSkillNum);
		dynamic_cast<CBossSkill*>(pObj)->SetSkillDamage(fAtk);
		return pObj;
	}

};