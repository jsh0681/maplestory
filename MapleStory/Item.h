#pragma once
#include "Obj.h"
class CItem :
	public CObj
{
public:
	float fRand = 0;
public:
	CItem();
	virtual ~CItem();
public:
	// CObj을(를) 통해 상속됨
	virtual void Initialize() override;
	void Create(char * pName, char * pClass, float fHp, float fMaxHp, float fMp, float fMaxMp, float fAtk, float fMoney, int iType);
	virtual void LateInit() override;
	virtual int  Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;
public://겟함수
	const float GetAtk() { return m_tItemInfo.fAtk; };
	const float GetMoney() { return m_tItemInfo.fMoney; };

public://셋함수
	void SetAtk(float fAtk) { m_tItemInfo.fAtk = fAtk; }
	void SetMoney(float fMoney) { m_tItemInfo.fMoney = fMoney; }
	
public:
	void SceneChange();

protected:

	ITEM_TYPE m_eCurState;//현재 상태
	ITEM_TYPE m_ePreState;//이전 상태

	


	bool m_bMoney;
	bool m_bItem;


	DWORD m_dwStart;

	wstring m_strFrameKey;
};

