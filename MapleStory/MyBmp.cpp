#include "stdafx.h"
#include "MyBmp.h"


CMyBmp::CMyBmp()
{
}


CMyBmp::~CMyBmp()
{
	Release();
}

void CMyBmp::LoadBmp(const TCHAR * pFilePath)
{
	HDC hDC = GetDC(g_hWnd);

	//CreateCompatibleDC(화면DC) : 화면DC와 호환이 되는 메모리 DC를 할당
	m_hMemDC = CreateCompatibleDC(hDC);
	ReleaseDC(g_hWnd, hDC);
	//비트맵 로드 방식 2가지 
	//1.DIB(Device Independent Bitmap) : 장치에 의존적이지 않은(독립적인) 비트맵
	//	-> 칼라장치에서도 흑백을 출력할 수 있다.
	//1.DDB(Device Dependent Bitmap) : 장치에 의존적인 비트맵
	//	-> 칼라장치에서 흑백을 출력할 수 없다.
	m_hBitMap = (HBITMAP)LoadImage(nullptr, pFilePath, 
		IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
		//첫인자는 그 솔루션폴더에 있으면서 동시에 컴파일하면 디버거에 불러와져있는 이미지를 사용하려고할때 사용 그냥 비트맵파일을 불러오기만할거라면 nullptr
	//메모리 DC에 불러온 비트맵을 미리 기록
	m_hOldMap = (HBITMAP)SelectObject(m_hMemDC, m_hBitMap);

}

void CMyBmp::Release()
{
	SelectObject(m_hMemDC, m_hOldMap);//DC가 선정중인 비트맵은 해제가 불가능 하기때문에 이렇게 아무것도 없는 껍대기를 가리키게 해놓고 나머지를 비운다.
	DeleteObject(m_hBitMap);
	DeleteDC(m_hMemDC);
}

HDC CMyBmp::GetMemDC()
{
	return m_hMemDC;
}
