#include "stdafx.h"
#include "Monster.h"
#include "Player.h"


CMonster::CMonster():m_bDirChange(false), m_bIsNear(false), m_bRigidity(false)
{
	ZeroMemory(&m_tMonsterInfo, sizeof(OBJINFO));
}


CMonster::~CMonster()
{
}

void CMonster::Initialize()
{
	m_eLayer = CObjMgr::LAYER_MONSTER;
	m_tInfo = {0,0,60,60 };
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Character/Monster/GreenMushroom/GreenMushroomLeft.bmp", L"GreenMushroomLeft");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Character/Monster/GreenMushroom/GreenMushroomRight.bmp", L"GreenMushroomRight");
	
	m_tMonsterInfo = { "초록버섯","몬스터",5,(float)(1500) ,(float)(1500) ,0,0,10,100,100,1000 };

	m_fSpeed = 4.f;
	m_strFrameKey = L"GreenMushroomLeft";

	m_eCurState = eWalk;
	m_ePreState = m_eCurState;

	m_tFrame.m_dwFrameScene = 1;
	m_tFrame.m_dwFrameStart = 0;
	m_tFrame.m_dwFrameEnd = 3;
	m_tFrame.m_dwFrameTime = GetTickCount();
	m_tFrame.m_dwFrameSpd = 200;
	m_bIsGround = false;
}

void CMonster::LateInit()
{
	m_pPlayer = CObjMgr::GetInstance()->GetPlayer();
	m_dwStart = GetTickCount();
	m_dwRigidity = GetTickCount();
}

int CMonster::Update()
{
	CObj::LateInit();

	if (m_bIsDead)
	{
		dynamic_cast<CPlayer*>(m_pPlayer)->SetExp(m_tMonsterInfo.fExp);
		return DEAD_OBJ;
	}
	return NO_EVENT;
}


void CMonster::LateUpdate()
{
	if (m_dwStart + 500 < GetTickCount())
	{
		m_bDirChange = true;
		m_dwStart = GetTickCount();
	}
	if (!m_bIsNear)
	{
		if (m_bDirChange)
			RandMove();
		SceneChange();
		Moving();
	}
	else
	{
		if (m_bDir == 0)
		{
			m_eCurState = eWalk;
			m_strFrameKey = L"GreenMushroomLeft";
			m_tInfo.fX -= 1.5f;
		}
		else
		{
			m_eCurState = eWalk;
			m_strFrameKey = L"GreenMushroomRight";
			m_tInfo.fX += 1.5f;	
		}
		m_eCurState = m_ePreState;
	}
	if (m_bRigidity)
	{
		if (m_dwRigidity + 300 < GetTickCount())
		{
			m_bRigidity = false;
			m_dwRigidity = GetTickCount();
		}
	}
	
	CObj::MoveFrame();
	
}



void CMonster::Render(HDC hDC)
{
	CObj::UpdateRect();
	if (!m_bIsGround)
	{
		m_tInfo.fY += m_fSpeed;
	}
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();

	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(m_strFrameKey.c_str());
	NULL_CHECK(hMemDC);

	GdiTransparentBlt(hDC, m_tRect.left - (int)fScrollX, m_tRect.top - (int)fScrollY,
		(int)m_tInfo.fCX, (int)m_tInfo.fCY,
		hMemDC,
		m_tFrame.m_dwFrameStart * (int)m_tInfo.fCX,
		m_tFrame.m_dwFrameScene * (int)m_tInfo.fCY,
		(int)m_tInfo.fCX, (int)m_tInfo.fCY, REMOVE_PINK);

	DrawHpBar(hDC);



}
void CMonster::DrawHpBar(HDC hDC)
{
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();
	Brush = CreateSolidBrush(RGB(69, 69, 69));

	oBrush = (HBRUSH)SelectObject(hDC, Brush);

	Rectangle(hDC, m_tRect.left - (int)fScrollX, m_tRect.top - 17 - (int)fScrollY, m_tRect.right - (int)fScrollX, m_tRect.top - 10 - (int)fScrollY);

	SelectObject(hDC, oBrush);
	DeleteObject(Brush);

	float fMinusDamage = (m_tMonsterInfo.fMaxHp - m_tMonsterInfo.fHp) / m_tMonsterInfo.fMaxHp;
	Brush = CreateSolidBrush(RGB(255, 0, 0));

	oBrush = (HBRUSH)SelectObject(hDC, Brush);
		
	Rectangle(hDC, (int)(m_tRect.left - fScrollX), (int)(m_tRect.top - 17 - fScrollY), (int)((m_tRect.right  - (m_tRect.right - m_tRect.left)*fMinusDamage) - fScrollX), (int)(m_tRect.top - 10 - fScrollY));
	SelectObject(hDC, oBrush);
	DeleteObject(Brush);
}
void CMonster::Release()
{
}
void CMonster::RandMove()
{
	int iRand = ((rand() % 4) + 1);

	int iDir = rand() % 2;
	if (iDir == 0)
	{
		m_strFrameKey = L"GreenMushroomLeft";
	}
	else
	{
		m_strFrameKey = L"GreenMushroomRight";
	}
	m_bDirChange = false;
	if(iRand != 2)
		m_eCurState = (STATE)iRand;
	else if(iRand == 4)
		m_eCurState = (STATE)(iRand-3);
}
void CMonster::Moving()
{
	if (m_eCurState==eWalk )
	{
		if (m_strFrameKey == L"GreenMushroomLeft")
			m_tInfo.fX -= 1.f;
		else if (m_strFrameKey == L"GreenMushroomRight")
			m_tInfo.fX += 1.f;
	}
}


void CMonster::SceneChange()
{
	if (m_eCurState != m_ePreState)
	{
		switch (m_eCurState)
		{
		case eDead:
			m_tFrame.m_dwFrameScene = 0;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 3;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 200;
			break;

		case eWalk:
			m_tFrame.m_dwFrameScene = 1;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 4;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 200;

			break;
		case eSlapped:
			m_tFrame.m_dwFrameScene = 2;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 0;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 200;
			break;

		case eStop:
			m_tFrame.m_dwFrameScene = 3;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 1;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 200;
			break;
		}
		m_ePreState = m_eCurState;
	}
}