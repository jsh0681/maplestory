#include "stdafx.h"
#include "DungeonTwo.h"
#include "Player.h"
#include "Monster.h"
#include "UI.h"
#include"Skill.h"
#include "Mouse.h"
#include"Item.h"
#include"BlueMushroom.h"


CDungeonTwo::CDungeonTwo()
{
}


CDungeonTwo::~CDungeonTwo()
{
	Release();

}

void CDungeonTwo::Initialize()
{
	CObj* pObject = CAbstactFactory<CBlueMushroom>::CreateObj(400, 1000);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::BLUEMUSHROOM);

	pObject = CAbstactFactory<CBlueMushroom>::CreateObj(400, 700);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::BLUEMUSHROOM);

	pObject = CAbstactFactory<CBlueMushroom>::CreateObj(700, 500);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::BLUEMUSHROOM);

	pObject = CAbstactFactory<CBlueMushroom>::CreateObj(400, 400);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::BLUEMUSHROOM);

	pObject = CAbstactFactory<CBlueMushroom>::CreateObj(700, 1000);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::BLUEMUSHROOM);

	pObject = CAbstactFactory<CBlueMushroom>::CreateObj(900, 700);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::BLUEMUSHROOM);

	pObject = CAbstactFactory<CBlueMushroom>::CreateObj(900, 500);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::BLUEMUSHROOM);

	pObject = CAbstactFactory<CBlueMushroom>::CreateObj(800, 400);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::BLUEMUSHROOM);


	pObject = CAbstactFactory<CBlueMushroom>::CreateObj(600, 1000);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::BLUEMUSHROOM);

	pObject = CAbstactFactory<CBlueMushroom>::CreateObj(700, 700);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::BLUEMUSHROOM);

	pObject = CAbstactFactory<CBlueMushroom>::CreateObj(500, 500);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::BLUEMUSHROOM);

	pObject = CAbstactFactory<CBlueMushroom>::CreateObj(300, 400);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::BLUEMUSHROOM);



	CTileMgr::GetInstance()->LoadDungeonTwoData();
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Map/DungeonTwo.bmp", L"DungeonTwo");

	CSoundMgr::GetInstance()->PlayBGM(L"Bgm_DungeonTwo.wav");

	m_tFrame.m_dwFrameStart = 0;//비트맵의 그림 시작번호
	m_tFrame.m_dwFrameEnd = 7;//비트맵에서 그림이 있는 마지막 번호
	m_tFrame.m_dwFrameTime = GetTickCount();
	m_tFrame.m_dwFrameSpd = 200;

	g_bTown = false;
	g_bDungeon = false;
	g_bDungeonTwo = true;
	g_bBoss = false;


}

void CDungeonTwo::Update()
{
	CScene::MoveFrame();
	LockScroll();
	m_pPlayer = CObjMgr::GetInstance()->GetPlayer();

	CTileMgr::GetInstance()->Update();
	CObjMgr::GetInstance()->Update();

}

void CDungeonTwo::LateUpdate()
{
	CObjMgr::GetInstance()->LateUpdate();	
}

void CDungeonTwo::LockScroll()
{
	if (5 >= CScrollMgr::GetScrollX())
		CScrollMgr::SetScrollX(5.f);

	if (5.f > CScrollMgr::GetScrollY())
		CScrollMgr::SetScrollY(5.f);

	if (DUNGEON_X2 - WINCX< CScrollMgr::GetScrollX())
		CScrollMgr::SetScrollX(DUNGEON_X2 - WINCX);

	if (DUNGEON_Y2 - WINCY < CScrollMgr::GetScrollY())
		CScrollMgr::SetScrollY(DUNGEON_Y2 - WINCY);
}

void CDungeonTwo::Render(HDC hDC)
{
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();


	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"DungeonTwo");
	NULL_CHECK(hMemDC);

	GdiTransparentBlt(hDC, 0, 0,
		800, 600,
		hMemDC,
		(int)fScrollX,
		(int)fScrollY,
		800, 600, REMOVE_PINK);


	hMemDC = CBmpMgr::GetInstance()->FindImage(L"Portal");
	NULL_CHECK(hMemDC);
	GdiTransparentBlt(hDC, 1114 - (int)fScrollX, 95 - (int)fScrollY, 70, 202, hMemDC, m_tFrame.m_dwFrameStart * 70, 0, 70, 202, REMOVE_PINK);

	hMemDC = CBmpMgr::GetInstance()->FindImage(L"Portal");
	NULL_CHECK(hMemDC);
	GdiTransparentBlt(hDC, 1600 - (int)fScrollX, 1050 - (int)fScrollY, 70, 202, hMemDC, m_tFrame.m_dwFrameStart * 70, 0, 70, 202, REMOVE_PINK);




	CObjMgr::GetInstance()->Render(hDC);
	
	if (1130 < m_pPlayer->GetInfo().fX &&m_pPlayer->GetInfo().fX < 1170 && 360>m_pPlayer->GetInfo().fY &&m_pPlayer->GetInfo().fY > 160 && g_bDungeonToDungeonTwo)
	{
		if (CKeyMgr::GetInstance()->KeyPressing(VK_UP))
		{
			g_bDungeonToDungeonTwo = false;
			m_pPlayer->SetPos(155, 380);
			CScrollMgr::SetScrollX(2);
			CScrollMgr::SetScrollY(19);
			CSoundMgr::GetInstance()->PlaySoundw(L"Effect_Portal.mp3", CSoundMgr::UI);
			CSoundMgr::GetInstance()->StopSound(CSoundMgr::BGM);
			CSceneMgr::GetInstance()->SceneChange(CSceneMgr::DUNGEON);
			return;
		}
	}
	else if ((1130>m_pPlayer->GetInfo().fX || m_pPlayer->GetInfo().fX > 1170))
	{
		g_bDungeonToDungeonTwo = true;
	}


	if (1620 < m_pPlayer->GetInfo().fX &&m_pPlayer->GetInfo().fX < 1670 && 1320>m_pPlayer->GetInfo().fY &&m_pPlayer->GetInfo().fY > 1120 && g_bDungeonTwoToBoss)
	{
		if (CKeyMgr::GetInstance()->KeyPressing(VK_UP))
		{
			g_bDungeonTwoToBoss = false;
			m_pPlayer->SetPos(123, 400);
			CScrollMgr::SetScrollX(2);
			CScrollMgr::SetScrollY(19);
			CSoundMgr::GetInstance()->PlaySoundw(L"Effect_Portal.mp3", CSoundMgr::UI);
			CSoundMgr::GetInstance()->StopSound(CSoundMgr::BGM);
			CSceneMgr::GetInstance()->SceneChange(CSceneMgr::BOSSDUNGEON);
			return;
		}
	}
	else if ((1620>m_pPlayer->GetInfo().fX || m_pPlayer->GetInfo().fX > 1670))
	{
		g_bDungeonTwoToBoss = true;
	}
}

void CDungeonTwo::Release()
{
	CObjMgr::GetInstance()->DeleteGroup(CObjMgr::BLUEMUSHROOM);
	CTileMgr::GetInstance()->DestroyInstance();
}
