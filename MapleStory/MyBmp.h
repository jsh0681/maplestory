#pragma once
//비트맵 출력
//1. 하드로부터 이미지를 불러들인다.
//2. 불러들인 이미지를 메모리DC에 기록한다.
//3. 메모리DC에 기록한 이미지를 화면DC로 고속 복사하여 출력

class CMyBmp
{
public:
	CMyBmp();
	~CMyBmp();
public:
	void LoadBmp(const TCHAR* pFilePath);
	void Release();
	HDC GetMemDC();
private:
	HDC m_hMemDC; // 비트맵을 미리 기록할 메모리DC
	HBITMAP m_hBitMap;//하드로 부터 불러온 비트맵 정보를 가질 GDI Object
	HBITMAP m_hOldMap;



};

