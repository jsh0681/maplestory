#pragma once
#include "Obj.h"

class CBossSkill :
	public CObj
{
public:
	enum STATE { eTHUNDER, eFIRE, eCURSE, eICE, eSUN, eEND };
public:
	CBossSkill();
	virtual ~CBossSkill();

	// CObj을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual void LateInit() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	void DrawDamage(HDC hDC);
	virtual void Release() override;
public:
	const float GetSkillDamage() { return m_fSkillDamage; }
	void SetEnum(int eSkillNum) { m_eSkillNum = (STATE)eSkillNum; }
	void SetSkillDamage(float fSkillDamage) { m_fSkillDamage = fSkillDamage; }
	void SetDamage(int iDamage) { m_iDamage = iDamage + (rand() % (iDamage / 2)); }
	void SceneChange();
	int m_iDamage;


private:
	FRAME	m_tBossFrame;
	float m_fSkillDamage;
	CObj* m_pPlayer;
	DWORD m_dwStart;

	STATE m_eSkillNum;
	STATE m_eCurState;//현재 상태
	STATE m_ePreState;//이전 상태
	wstring m_strFrameKey;
};

