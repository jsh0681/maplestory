#pragma once
class CObj;

class CCollisionMgr
{
private:
public:
	CCollisionMgr();
	~CCollisionMgr();
public:
	static void CollisionPlayerToMonster(OBJLIST & dstLst, OBJLIST & srcLst);
	static void CollisionPlayerToBlueMushroom(OBJLIST & dstLst, OBJLIST & srcLst);
	static void CollisionPlayerToMoney(OBJLIST & dstLst, OBJLIST & srcLst);
	static void CollisionSkillToMonster(OBJLIST & dstLst, OBJLIST & srcLst);
	static void CollisionSkillToBlueMushroom(OBJLIST & dstLst, OBJLIST & srcLst);
	static void CollisionMouseToItem(OBJLIST & dstLst, OBJLIST & srcLst);
	static void CollisionSkillToBoss(OBJLIST & dstLst, OBJLIST & srcLst);
	static void CollisionBossSkillToPlayer(OBJLIST & dstLst, OBJLIST & srcLst);
	static void CollisionSphere(OBJLIST& dstLst, OBJLIST& srcLst);
	static void CollisionRectEX(OBJLIST & dstLst, OBJLIST & srcLst);
	static void CollisionTile(TILEVEC & dstLst, OBJLIST & srcLst);
	static void CollisionTiletoMonster(TILEVEC & dstLst, OBJLIST & srcLst);
	static bool CheckRect2(CTile * pDst, CObj * pSrc, float * pMoveX, float * pMoveY);
	static bool CheckRect(CTile * pDst, CObj * pSrc, float * pMoveX, float * pMoveY);



	static void CollisionTiletoBlueMushroom(TILEVEC & dstLst, OBJLIST & srcLst);
private:
	static bool CheckRect3(CTile * pDst, CObj * pSrc, float * pMoveX, float * pMoveY);
	static bool CheckSphere(CObj* pDst, CObj* pSrc);
	static bool CheckRect(CObj * pDst, CObj * pSrc, float * pMoveX, float * pMoveY);
	
};

