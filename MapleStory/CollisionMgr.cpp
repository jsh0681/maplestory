#include "stdafx.h"
#include "CollisionMgr.h"
#include "Obj.h"
#include"Tile.h"
#include"Player.h"
#include"Monster.h"
#include"Effect.h"
#include"Skill.h"
#include"Item.h"
#include"BossMonster.h"
#include"BossSkill.h"
#include"UI.h"
#include"Inven.h"
#include"BlueMushroom.h"


CCollisionMgr::CCollisionMgr()
{

}


CCollisionMgr::~CCollisionMgr()
{
}
void CCollisionMgr::CollisionPlayerToMonster(OBJLIST& dstLst, OBJLIST& srcLst)
{
	RECT rc = {};
	for (auto& pDst : dstLst)
	{
		for (auto& pSrc : srcLst)
		{
			if (pDst->GetInfo().fX - 200 < pSrc->GetInfo().fX &&pSrc->GetInfo().fX < pDst->GetInfo().fX + 200
				&& pDst->GetInfo().fY + 30 > pSrc->GetInfo().fY&&pDst->GetInfo().fY - 30 < pSrc->GetInfo().fY)
			{
				dynamic_cast<CMonster*>(pSrc)->SetIsNear(true);
				if (pDst->GetInfo().fX < pSrc->GetInfo().fX)
				{
					pSrc->SetDir(0);
				}
				else
					pSrc->SetDir(1);
				if (IntersectRect(&rc, &(pSrc->GetRect()), &(pDst->GetCollisionRect())))
				{
					if (dynamic_cast<CPlayer*>(pDst)->GetRigidity() == false)
					{
						dynamic_cast<CPlayer*>(pDst)->SetDamage(dynamic_cast<CMonster*>(pSrc)->GetDamage());
						dynamic_cast<CPlayer*>(pDst)->SetRigidity();
					}
				}
			}
			else
				dynamic_cast<CMonster*>(pSrc)->SetIsNear(false);
		}
	}
}



void CCollisionMgr::CollisionPlayerToBlueMushroom(OBJLIST& dstLst, OBJLIST& srcLst)
{
	RECT rc = {};
	for (auto& pDst : dstLst)
	{
		for (auto& pSrc : srcLst)
		{
			if (pDst->GetInfo().fX - 200 < pSrc->GetInfo().fX &&pSrc->GetInfo().fX < pDst->GetInfo().fX + 200
				&& pDst->GetInfo().fY + 30 > pSrc->GetInfo().fY&&pDst->GetInfo().fY - 30 < pSrc->GetInfo().fY)
			{
				dynamic_cast<CBlueMushroom*>(pSrc)->SetIsNear(true);
				if (pDst->GetInfo().fX < pSrc->GetInfo().fX)
				{
					pSrc->SetDir(0);
				}
				else
					pSrc->SetDir(1);
				if (IntersectRect(&rc, &(pSrc->GetRect()), &(pDst->GetCollisionRect())))
				{
					if (dynamic_cast<CPlayer*>(pDst)->GetRigidity() == false)
					{
						dynamic_cast<CPlayer*>(pDst)->SetDamage(dynamic_cast<CBlueMushroom*>(pSrc)->GetDamage());
						dynamic_cast<CPlayer*>(pDst)->SetRigidity();
					}
				}
			}
			else
				dynamic_cast<CBlueMushroom*>(pSrc)->SetIsNear(false);
		}
	}
}


void CCollisionMgr::CollisionSkillToBlueMushroom(OBJLIST& dstLst, OBJLIST& srcLst)
{
	RECT rc = {};
	for (auto& pDst : dstLst)
	{
		for (auto& pSrc : srcLst)
		{
			if (IntersectRect(&rc, &(pSrc->GetRect()), &(pDst->GetCollisionRect())))
			{

				if (dynamic_cast<CBlueMushroom*>(pSrc)->GetRigidity() == false)
				{

					if (pDst->GetFrame().m_dwFrameStart > 1)
					{
						if (pDst->GetFrame().m_dwFrameStart > 2)
						{
							CObj* pObject = CAbstactFactory<CEffect>::CreateObj(pSrc->GetInfo().fX, pSrc->GetInfo().fY, dynamic_cast<CSkill*>(pDst)->GetEnum());
							CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::EFFECT);
						}

						if (dynamic_cast<CBlueMushroom*>(pSrc)->GetHp() <= 0)
						{
						//	CSoundMgr::GetInstance()->PlaySoundw(L"Monster_Dead.mp3", CSoundMgr::MONSTER);
							CObj* pObject = CAbstactFactory<CItem>::CreateItem(pSrc->GetInfo().fX, pSrc->GetInfo().fY + 20, CItem::eMONEY);
							CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::ITEM);
							dynamic_cast<CBlueMushroom*>(pSrc)->SetState(0);//맞은모션
							pSrc->IsDead();
						}
						else
						{
							CObj* pObject = CAbstactFactory<CEffect>::CreateEffect(pSrc->GetInfo().fX, pSrc->GetInfo().fY, (int)dynamic_cast<CSkill*>(pDst)->GetSkillDamage());
							CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::UI);
						//	CSoundMgr::GetInstance()->PlaySoundw(L"Monster_Hit.mp3", CSoundMgr::MONSTER);
							dynamic_cast<CBlueMushroom*>(pSrc)->SetRigidity();
							dynamic_cast<CBlueMushroom*>(pSrc)->SetState(1);
							dynamic_cast<CBlueMushroom*>(pSrc)->SetDamage(dynamic_cast<CSkill*>(pDst)->GetSkillDamage());
						}
					}
				}
			}
		}
	}
}




void CCollisionMgr::CollisionPlayerToMoney(OBJLIST& dstLst, OBJLIST& srcLst)
{
	RECT rc = {};
	for (auto& pDst : dstLst)
	{
		for (auto& pSrc : srcLst)
		{

			if (IntersectRect(&rc, &(pSrc->GetRect()), &(pDst->GetCollisionRect())))
			{
				if (dynamic_cast<CPlayer*>(pDst)->GetEat() == true)
				{
					CSoundMgr::GetInstance()->PlaySoundw(L"Item_Money.mp3",CSoundMgr::EFFECT);
					dynamic_cast<CPlayer*>(pDst)->SetMoney(dynamic_cast<CItem*>(pSrc)->GetMoney());
					pSrc->IsDead();
				}
			}

		}
	}
}



void CCollisionMgr::CollisionSkillToMonster(OBJLIST& dstLst, OBJLIST& srcLst)
{
	RECT rc = {};
	for (auto& pDst : dstLst)
	{
		for (auto& pSrc : srcLst)
		{
			if (IntersectRect(&rc, &(pSrc->GetRect()), &(pDst->GetCollisionRect())))
			{

				if (dynamic_cast<CMonster*>(pSrc)->GetRigidity() == false)
				{

					if (pDst->GetFrame().m_dwFrameStart > 1)
					{
						if (pDst->GetFrame().m_dwFrameStart > 2)
						{
							CObj* pObject = CAbstactFactory<CEffect>::CreateObj(pSrc->GetInfo().fX, pSrc->GetInfo().fY, dynamic_cast<CSkill*>(pDst)->GetEnum());
							CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::EFFECT);
						}
						
						if (dynamic_cast<CMonster*>(pSrc)->GetHp() <= 0)
						{
						//	CSoundMgr::GetInstance()->PlaySoundw(L"Monster_Dead.mp3", CSoundMgr::MONSTER);
							CObj* pObject = CAbstactFactory<CItem>::CreateItem(pSrc->GetInfo().fX, pSrc->GetInfo().fY + 20, CItem::eMONEY);
							CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::ITEM);
							dynamic_cast<CMonster*>(pSrc)->SetState(0);//맞은모션
							pSrc->IsDead();
						}
						else
						{
							CObj* pObject = CAbstactFactory<CEffect>::CreateEffect(pSrc->GetInfo().fX, pSrc->GetInfo().fY, (int)dynamic_cast<CSkill*>(pDst)->GetSkillDamage());
							CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::UI);
					//		CSoundMgr::GetInstance()->PlaySoundw(L"Monster_Hit.mp3", CSoundMgr::MONSTER);
							dynamic_cast<CMonster*>(pSrc)->SetRigidity();
							dynamic_cast<CMonster*>(pSrc)->SetState(2);
							dynamic_cast<CMonster*>(pSrc)->SetDamage(dynamic_cast<CSkill*>(pDst)->GetSkillDamage());
						}
					}
				}
			}
		}
	}
}

void CCollisionMgr::CollisionMouseToItem(OBJLIST& dstLst, OBJLIST& srcLst)
{
	CObj* m_pPlayer = CObjMgr::GetInstance()->GetPlayer();
	CObj* m_pPInven = CObjMgr::GetInstance()->GetInven();

	RECT rc = {};
	for (auto& pDst : dstLst)
	{
		for (auto& pSrc : srcLst)
		{
			if (!dynamic_cast<CPlayer*>(m_pPlayer)->GetClickItem())
			{
				if (IntersectRect(&rc, &(pSrc->GetRect()), &(pDst->GetCollisionRect())))
				{	
					if (CKeyMgr::GetInstance()->KeyPressing(VK_LBUTTON)&&g_bShopWindowOpen&&!g_bEquipWindowOpen)
					{
						CSoundMgr::GetInstance()->PlaySoundw(L"Item_Click.mp3", CSoundMgr::EFFECT);
						dynamic_cast<CPlayer*>(m_pPlayer)->SetSale(dynamic_cast<CItem*>(pSrc)->GetMoney());
						dynamic_cast<CPlayer*>(m_pPlayer)->SetInven(pSrc);
						
					}
					else if (CKeyMgr::GetInstance()->KeyPressing(VK_LBUTTON) &&!g_bShopWindowOpen && g_bInvenWindowOpen&& g_bEquipWindowOpen)
					{
						CSoundMgr::GetInstance()->PlaySoundw(L"Item_Equip.mp3", CSoundMgr::EFFECT);
						if(!(dynamic_cast<CPlayer*>(m_pPlayer)->GetInven().empty()))
							dynamic_cast<CPlayer*>(m_pPlayer)->InvenToEquip(pSrc);
						dynamic_cast<CPlayer*>(m_pPlayer)->SetClicKItem(true);
					}
					else if (CKeyMgr::GetInstance()->KeyPressing(VK_RBUTTON) && !g_bShopWindowOpen && g_bInvenWindowOpen&& g_bEquipWindowOpen)
					{
						CSoundMgr::GetInstance()->PlaySoundw(L"Item_Equip.mp3", CSoundMgr::EFFECT);
						if (!(dynamic_cast<CPlayer*>(m_pPlayer)->GetEquipment().empty()))
							dynamic_cast<CPlayer*>(m_pPlayer)->EquipToInven(pSrc);
						dynamic_cast<CPlayer*>(m_pPlayer)->SetClicKItem(true);
					}
				}
				
			}
			else
			{
				dynamic_cast<CPlayer*>(m_pPlayer)->SetClicKItem(false);
			}
		}
	}
}
void CCollisionMgr::CollisionSkillToBoss(OBJLIST& dstLst, OBJLIST& srcLst)
{
	RECT rc = {};
	for (auto& pDst : dstLst)
	{
		for (auto& pSrc : srcLst)
		{
			if (IntersectRect(&rc, &(pSrc->GetCollisionRect()), &(pDst->GetCollisionRect())))
			{
				if (dynamic_cast<CBossMonster*>(pSrc)->GetRigidity() == false)
				{
					if (pDst->GetFrame().m_dwFrameStart > 1)
					{
						if (pDst->GetFrame().m_dwFrameStart > 2)
						{
							CObj* pObject = CAbstactFactory<CEffect>::CreateObj(pSrc->GetInfo().fX, pSrc->GetInfo().fY+155, dynamic_cast<CSkill*>(pDst)->GetEnum());
							CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::EFFECT);
						}

						if (dynamic_cast<CBossMonster*>(pSrc)->GetHp() <= 0)
						{
							CSoundMgr::GetInstance()->StopSound(CSoundMgr::BGM);
							CSoundMgr::GetInstance()->PlayBGM(L"Clear.wav");
							pSrc->IsDead();
						}
						else
						{
							CObj* pObject = CAbstactFactory<CEffect>::CreateEffect(pSrc->GetInfo().fX, pSrc->GetInfo().fY, (int)dynamic_cast<CSkill*>(pDst)->GetSkillDamage());
							CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::UI);
							dynamic_cast<CBossMonster*>(pSrc)->SetDamage(dynamic_cast<CSkill*>(pDst)->GetSkillDamage());
						}
					}
				}
			}
		}
	}
}

void CCollisionMgr::CollisionBossSkillToPlayer(OBJLIST& dstLst, OBJLIST& srcLst)
{
	RECT rc = {};
	for (auto& pDst : dstLst)
	{
		for (auto& pSrc : srcLst)
		{
			if (IntersectRect(&rc, &(pSrc->GetCollisionRect()), &(pDst->GetRect())))
			{
					if (dynamic_cast<CPlayer*>(pSrc)->GetHp() <= 0)//플레이어 체력
					{
						//CObj* pObject = CAbstactFactory<CItem>::CreateObj(pSrc->GetInfo().fX, pSrc->GetInfo().fY + 20, CItem::eMONEY);
						//CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::ITEM);
					    //pSrc->IsDead();
					}
					else if (dynamic_cast<CPlayer*>(pSrc)->GetRigidity() == false)
					{

						CObj* pObject = CAbstactFactory<CEffect>::CreateEffect(pSrc->GetInfo().fX-80, pSrc->GetInfo().fY-40, (int)dynamic_cast<CBossSkill*>(pDst)->GetSkillDamage());
						CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::UI);
						
						
						
						dynamic_cast<CPlayer*>(pSrc)->SetRigidity();
						dynamic_cast<CPlayer*>(pSrc)->SetDamage(dynamic_cast<CBossSkill*>(pDst)->GetSkillDamage());
					}
			}
		}
	}
}







void CCollisionMgr::CollisionSphere(OBJLIST & dstLst, OBJLIST & srcLst)
{
	for (auto& pDst : dstLst)
	{
		for (auto& pSrc : srcLst)
		{
			if (CheckSphere(pDst, pSrc))
			{
				//		pDst->IsDead();
		//				pSrc->IsDead();
			}
		}
	}
}


bool CCollisionMgr::CheckSphere(CObj * pDst, CObj * pSrc)
{
	// 원 충돌
	// 두 원의 반지름 합이 두 원의 거리보다 커지면 true

	// 반지름의 합
	float fSumRad = (pDst->GetInfo().fCX + pSrc->GetInfo().fCX) * 0.5f;

	// 두 원의 거리
	float w = pDst->GetInfo().fX - pSrc->GetInfo().fX;
	float h = pDst->GetInfo().fY - pSrc->GetInfo().fY;

	// sqrtf(x): x에 대한 제곱근을 구하는 함수. <cmath>에서 제공.
	float fDist = sqrtf(w * w + h * h);	// 피타고라스

	return fSumRad >= fDist;
}




bool CCollisionMgr::CheckRect(CObj* pDst, CObj* pSrc,
	float* pMoveX, float* pMoveY)
{
	// 각 축의 반지름 합을 구한다.
	float fSumRadX = (pDst->GetInfo().fCX + pSrc->GetInfo().fCX - 70) * 0.5f;
	float fSumRadY = (pDst->GetInfo().fCY + pSrc->GetInfo().fCY - 50) * 0.5f;

	// 각 축의 거리를 구한다.
	// fabs(x): <cmath>에서 제공하는 x의 절대 값 구하는 함수.
	float fDistX = fabs(pDst->GetInfo().fX - pSrc->GetInfo().fX);
	float fDistY = fabs(pDst->GetInfo().fY - pSrc->GetInfo().fY);

	// 각 축의 반지름 합이 각 축의 거리보다 크면 충돌!
	if (fSumRadX >= fDistX && fSumRadY >= fDistY)
	{
		// pSrc가 밀려날 거리를 구한다.
		*pMoveX = fSumRadX - fDistX;
		*pMoveY = fSumRadY - fDistY;

		return true;
	}

	return false;
}


void CCollisionMgr::CollisionRectEX(OBJLIST & dstLst, OBJLIST & srcLst)
{
	float fMoveX = 0.f, fMoveY = 0.f;

	for (auto& pDst : dstLst)
	{
		for (auto& pSrc : srcLst)
		{
			if (CheckRect(pDst, pSrc, &fMoveX, &fMoveY))
			{
				float fX = pSrc->GetInfo().fX;
				float fY = pSrc->GetInfo().fY;

				if (fMoveX > fMoveY)	// y축으로 밀어냄
				{
					if (pDst->GetInfo().fY > fY)
						fMoveY *= -1.f;

					pSrc->SetPos(fX, fY + fMoveY);
				}
				else // x축으로 밀어냄
				{
					if (pDst->GetInfo().fX > fX)
						fMoveX *= -1.f;

					pSrc->SetPos(fX + fMoveX, fY);
				}
			}
		}
	}
}




void CCollisionMgr::CollisionTile(TILEVEC & dstLst, OBJLIST & srcLst)
{
	float fMoveY;
	float fMoveX;
	RECT rc = {};
	for (auto& pDst : dstLst)
	{
		for (auto& pSrc : srcLst)
		{
			if (CheckRect(pDst, pSrc, &fMoveX, &fMoveY))
			{
					if (pDst->GetInfo().fY > pSrc->GetInfo().fY + 20)
					{
						if (pDst->GetInfo().iOption == WALL)
						{
							dynamic_cast<CPlayer*>(pSrc)->SetHang(false);
							pSrc->SetIsGround(true);
							float fX = pSrc->GetInfo().fX;
							float fY = pSrc->GetInfo().fY;
							if (fMoveX < fMoveY)
							{
								if (pDst->GetInfo().fX > fX)
									fMoveX *= -1.f;

								pSrc->SetPos(fX + fMoveX, fY);

							}
							else
							{
								if (pDst->GetInfo().fY > fY)
									fMoveY *= -1.f;

								pSrc->SetPos(fX, fY + fMoveY);
							}

						}
						else if (pDst->GetInfo().iOption == RADDER)
						{
							dynamic_cast<CPlayer*>(pSrc)->SetHang(true);
						}
						else
						{
							pSrc->SetIsGround(false);
							dynamic_cast<CPlayer*>(pSrc)->SetHang(false);
						}
					}
				
			}
		}
	}
}

void CCollisionMgr::CollisionTiletoMonster(TILEVEC & dstLst, OBJLIST & srcLst)
{
	float fMoveY;
	float fMoveX;
	RECT rc = {}; 
	for (auto& pDst : dstLst)
	{
		for (auto& pSrc : srcLst)
		{
			
				if (CheckRect2(pDst, pSrc, &fMoveX, &fMoveY))
				{
					if (pDst->GetInfo().fY > pSrc->GetInfo().fY + 20)
					{
						if (pDst->GetInfo().iOption == WALL || pDst->GetInfo().iOption == RADDER)
						{
							pSrc->SetIsGround(true);
							float fX = pSrc->GetInfo().fX;
							float fY = pSrc->GetInfo().fY;
							if (fMoveX < fMoveY)
							{
								if (pDst->GetInfo().fX > fX)
									fMoveX *= -1.f;

								pSrc->SetPos(fX + fMoveX, fY);

							}
							else
							{
								if (pDst->GetInfo().fY > fY)
									fMoveY *= -1.f;

								pSrc->SetPos(fX, fY + fMoveY);
							}

						}
						else
						{
							pSrc->SetIsGround(false);
						}
					}
				
			}
		}

	}
}

bool CCollisionMgr::CheckRect2(CTile* pDst, CObj* pSrc,
	float* pMoveX, float* pMoveY)
{
	// 각 축의 반지름 합을 구한다.
	float fSumRadX = (pDst->GetInfo().fCX + (pSrc->GetInfo().fCX)) * 0.5f;
	float fSumRadY = (pDst->GetInfo().fCY + (pSrc->GetInfo().fCY-30)) * 0.5f;

	// 각 축의 거리를 구한다.
	// fabs(x): <cmath>에서 제공하는 x의 절대 값 구하는 함수.
	float fDistX = fabs(pDst->GetInfo().fX - pSrc->GetInfo().fX);
	float fDistY = fabs(pDst->GetInfo().fY - pSrc->GetInfo().fY);

	// 각 축의 반지름 합이 각 축의 거리보다 크면 충돌!
	if (fSumRadX >= fDistX && fSumRadY >= fDistY)
	{
		// pSrc가 밀려날 거리를 구한다.
		*pMoveX = fSumRadX - fDistX;
		*pMoveY = fSumRadY - fDistY;

		return true;
	}

	return false;
}

bool CCollisionMgr::CheckRect(CTile* pDst, CObj* pSrc,
	float* pMoveX, float* pMoveY)
{
	// 각 축의 반지름 합을 구한다.
	float fSumRadX = (pDst->GetInfo().fCX + (pSrc->GetInfo().fCX - 70)) * 0.5f;
	float fSumRadY = (pDst->GetInfo().fCY + (pSrc->GetInfo().fCY - 60)) * 0.5f;

	// 각 축의 거리를 구한다.
	// fabs(x): <cmath>에서 제공하는 x의 절대 값 구하는 함수.
	float fDistX = fabs(pDst->GetInfo().fX - pSrc->GetInfo().fX);
	float fDistY = fabs(pDst->GetInfo().fY - pSrc->GetInfo().fY);

	// 각 축의 반지름 합이 각 축의 거리보다 크면 충돌!
	if (fSumRadX >= fDistX && fSumRadY >= fDistY)
	{
		// pSrc가 밀려날 거리를 구한다.
		*pMoveX = fSumRadX - fDistX;
		*pMoveY = fSumRadY - fDistY;

		return true;
	}
	return false;
}






void CCollisionMgr::CollisionTiletoBlueMushroom(TILEVEC & dstLst, OBJLIST & srcLst)
{
	float fMoveY;
	float fMoveX;
	RECT rc = {};
	for (auto& pDst : dstLst)
	{
		for (auto& pSrc : srcLst)
		{

			if (CheckRect3(pDst, pSrc, &fMoveX, &fMoveY))
			{
				if (pDst->GetInfo().fY > pSrc->GetInfo().fY + 20)
				{
					if (pDst->GetInfo().iOption == WALL || pDst->GetInfo().iOption == RADDER)
					{
						pSrc->SetIsGround(true);
						float fX = pSrc->GetInfo().fX;
						float fY = pSrc->GetInfo().fY;
						if (fMoveX < fMoveY)
						{
							if (pDst->GetInfo().fX > fX)
								fMoveX *= -1.f;

							pSrc->SetPos(fX + fMoveX, fY);

						}
						else
						{
							if (pDst->GetInfo().fY > fY)
								fMoveY *= -1.f;

							pSrc->SetPos(fX, fY + fMoveY);
						}

					}
					else
					{
						pSrc->SetIsGround(false);
					}
				}

			}
		}

	}
}

bool CCollisionMgr::CheckRect3(CTile* pDst, CObj* pSrc,
	float* pMoveX, float* pMoveY)
{
	// 각 축의 반지름 합을 구한다.
	float fSumRadX = (pDst->GetInfo().fCX + (pSrc->GetInfo().fCX)) * 0.5f;
	float fSumRadY = (pDst->GetInfo().fCY + (pSrc->GetInfo().fCY - 20)) * 0.5f;

	// 각 축의 거리를 구한다.
	// fabs(x): <cmath>에서 제공하는 x의 절대 값 구하는 함수.
	float fDistX = fabs(pDst->GetInfo().fX - pSrc->GetInfo().fX);
	float fDistY = fabs(pDst->GetInfo().fY - pSrc->GetInfo().fY);

	// 각 축의 반지름 합이 각 축의 거리보다 크면 충돌!
	if (fSumRadX >= fDistX && fSumRadY >= fDistY)
	{
		// pSrc가 밀려날 거리를 구한다.
		*pMoveX = fSumRadX - fDistX;
		*pMoveY = fSumRadY - fDistY;

		return true;
	}

	return false;
}