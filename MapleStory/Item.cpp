#include "stdafx.h"
#include "Item.h"


CItem::CItem():m_eCurState(eEND),m_ePreState(eEND), fRand(0), m_bItem(false),m_bMoney(false)
{
	ZeroMemory(&m_tItemInfo, sizeof(OBJINFO)); 
	
}


CItem::~CItem()
{
	Release();
}

void CItem::Initialize()
{
	m_tInfo = { 0,0,32,32 };
	m_eLayer = CObjMgr::LAYER_ITEM;
	m_eCurState = m_eType;



}
void CItem::Create(char * pName,char* pClass,float fHp, float fMaxHp, float fMp, float fMaxMp, float fAtk, float fMoney, int iType)
{
	strcpy_s(m_tItemInfo.strName, strlen(pName) + 1, pName);
	strcpy_s(m_tItemInfo.strClass, strlen(pClass) + 1, pClass);
	m_tItemInfo.fAtk = fAtk;
	m_tItemInfo.fHp = fHp;
	m_tItemInfo.fMaxHp = fMaxHp;
	m_tItemInfo.fMp = fMp;
	m_tItemInfo.fMaxMp = fMaxMp;
	m_tItemInfo.fMoney = fMoney;
	m_eType = (ITEM_TYPE)iType;
	m_eEquipment = UNEQUIP;
}


void CItem::LateInit()
{
}
int CItem::Update()
{
	CObj::LateInit();

	if (m_bIsDead)
		return DEAD_OBJ;

	return NO_EVENT;
}



void CItem::LateUpdate()
{
	SceneChange();
	CObj::MoveFrame();
}

void CItem::Render(HDC hDC)
{
	CObj::UpdateRect();
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();

	if(m_eType==eMONEY)
	{
		HDC hMemDC = CBmpMgr::GetInstance()->FindImage(m_strFrameKey.c_str());
		NULL_CHECK(hMemDC);
		GdiTransparentBlt(hDC, m_tRect.left - (int)fScrollX, m_tRect.top - (int)fScrollY,
			(int)m_tInfo.fCX, (int)m_tInfo.fCY,
			hMemDC,
			m_tFrame.m_dwFrameStart * (int)m_tInfo.fCX,
			m_tFrame.m_dwFrameScene * (int)m_tInfo.fCY,
			(int)m_tInfo.fCX, (int)m_tInfo.fCY, REMOVE_PINK);
	}


	if (g_bShopWindowOpen)
	{
		if (m_eType == eHPPOTION)
		{
			HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"HpPotion");
			NULL_CHECK(hMemDC);
			GdiTransparentBlt(hDC, 112 - (int)fScrollX, 157 - (int)fScrollY,
				(int)m_tInfo.fCX, (int)m_tInfo.fCY,
				hMemDC, 0, 0,
				(int)m_tInfo.fCX, (int)m_tInfo.fCY, REMOVE_PINK);
		}	if (m_eType == eMPPOTION)
		{
			HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"MpPotion");
			NULL_CHECK(hMemDC);
			GdiTransparentBlt(hDC, 112 - (int)fScrollX, 199 - (int)fScrollY,
				(int)m_tInfo.fCX, (int)m_tInfo.fCY,
				hMemDC, 0, 0,
				(int)m_tInfo.fCX, (int)m_tInfo.fCY, REMOVE_PINK);
		}
		if (m_eType == eGLOVES)
		{
			HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Glove");
			NULL_CHECK(hMemDC);
			GdiTransparentBlt(hDC, 112 - (int)fScrollX, 241 - (int)fScrollY,
				(int)m_tInfo.fCX, (int)m_tInfo.fCY,
				hMemDC, 0, 0,
				(int)m_tInfo.fCX, (int)m_tInfo.fCY, REMOVE_PINK);
		}
		if (m_eType == eSHOES)
		{
			HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Shoes");
			NULL_CHECK(hMemDC);
			GdiTransparentBlt(hDC, 112 - (int)fScrollX, 283 - (int)fScrollY,
				(int)m_tInfo.fCX, (int)m_tInfo.fCY,
				hMemDC, 0, 0,
				(int)m_tInfo.fCX, (int)m_tInfo.fCY, REMOVE_PINK);
		}
		if (m_eType == eWEAPON)
		{
			HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Weapon");
			NULL_CHECK(hMemDC);
			GdiTransparentBlt(hDC, 112 - (int)fScrollX, 325 - (int)fScrollY,
				(int)m_tInfo.fCX, (int)m_tInfo.fCY,
				hMemDC, 0, 0,
				(int)m_tInfo.fCX, (int)m_tInfo.fCY, REMOVE_PINK);
		}


		if (m_eType == eWEAR)
		{
			HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Wear");
			NULL_CHECK(hMemDC);
			GdiTransparentBlt(hDC, 112 - (int)fScrollX, 367 - (int)fScrollY,
				(int)m_tInfo.fCX, (int)m_tInfo.fCY,
				hMemDC, 0, 0,
				(int)m_tInfo.fCX, (int)m_tInfo.fCY, REMOVE_PINK);
		}
	
	}
}

void CItem::Release()
{
}


void CItem::SceneChange()
{
	float fRand = (rand()%700+1)+300;
	if (m_eCurState != m_ePreState)
	{
		switch (m_eCurState)
		{
		case eMONEY:
			
			m_bMoney = true;
			m_strFrameKey = L"Money";
			m_tItemInfo = { "메소","아이템",0,0,0,0,0,fRand};
			if (fRand < 400)
			{
				m_tFrame.m_dwFrameScene = 0;
			}
			else if (fRand < 800)
			{
				m_tFrame.m_dwFrameScene = 1;
			}
			else if (fRand>=800)
			{
				m_tFrame.m_dwFrameScene = 2;
			}

			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 3;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 200;
			break;

		case eHPPOTION:
			m_strFrameKey = L"HpPotion";
			break;

		case eMPPOTION:
			m_strFrameKey = L"MpPotion";
			break;

		case eWEAPON:
			m_strFrameKey = L"Weapon";
			break;

		case eWEAR:
			m_strFrameKey = L"Wear";
			break; 
		case eGLOVES:
			m_strFrameKey = L"Glove";
			break;
			
		case eSHOES:
			m_strFrameKey = L"Shoes";
			break;
		}
		m_ePreState = m_eCurState;
	}
}