#pragma once

class CMaingame
{
private:
	HDC m_hDC;

public:
	CMaingame();
	~CMaingame();

public:
	void Initialize();
	void Update();
	void LateUpdate();
	void Render();
	void Release();
};

