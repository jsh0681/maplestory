#include "stdafx.h"
#include "Player.h"
#include"Skill.h"


int g_iItemCount = 0;

CPlayer::CPlayer() : m_eCurState(eEND), m_ePreState(eEND), fJumpCount(0), m_fJumpSpeed(5.f), m_bRigidity(false), m_dwStart(0), m_bIsUp(false), m_bIsDown(false), m_bAtkMotion(false)
, m_bIsEat(false), m_bHangUp(false), m_bHangDown(false), m_bStayHang(false), m_bFalling(true), m_bHangMoving(true), m_bClickItem(false)
{
	ZeroMemory(&m_tPlayerInfo, sizeof(OBJINFO));
	m_iEquipSize = 24;
	m_iInvenSize = 24;
	m_vecInven.reserve(m_iInvenSize);
	m_vecEquipment.reserve(m_iEquipSize);
}


CPlayer::~CPlayer()
{
	Release();
}

void CPlayer::Initialize()
{

	m_eLayer = CObjMgr::LAYER_PLAYER;
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Character/Player/wear_left.bmp", L"basic_left");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Character/Player/wear_right.bmp", L"basic_right");


	m_tPlayerInfo = { "정성호","스트라이커",1,100,100,100,100,250,0,100,10000 };

	m_strFrameKey = L"basic_left";
	m_eCurState = eIDLE;
	m_ePreState = m_eCurState;

	m_tFrame.m_dwFrameStart = 0;//비트맵의 그림 시작번호
	m_tFrame.m_dwFrameEnd = 3;//비트맵에서 그림이 있는 마지막 번호
	m_tFrame.m_dwFrameScene = 0;//줄수를 의미함
	m_tFrame.m_dwFrameTime = GetTickCount();
	m_tFrame.m_dwFrameSpd = 200;



	m_tInfo = { 100,100,100,100 };
	m_fSpeed = 3.f;

	m_bIsJump = false;
	m_bAttack = false;
	m_bStateLock = false;
	m_bIsGround = false;

}

void CPlayer::LateInit()
{
	m_dwStart = GetTickCount();
	dwAtkCount = GetTickCount();
}

int CPlayer::Update()
{
	if (m_bIsDead)
		//return DEAD_OBJ;

	CObj::LateInit();
	ScrollLock();
	KeyCheck();
	if (m_bIsJump)
		IsJumping();
	IsFalling();
	return NO_EVENT;

}

void CPlayer::LateUpdate()
{
	CObj::MoveFrame(); 
	SceneChange();
	LevelUp();
	IsAtkMotioning();
	if (m_tPlayerInfo.fHp <= 0.f)
		m_bIsDead = true;

	if (m_bRigidity)
	{
		if (m_dwStart + 2000 < GetTickCount())
		{
			m_bRigidity = false;
			m_dwStart = GetTickCount();
		}
	}
}

void CPlayer::LevelUp()
{
	if (m_tPlayerInfo.fExp >= m_tPlayerInfo.fMaxExp)
	{
		++m_tPlayerInfo.iLv;
		m_tPlayerInfo.fMaxExp *= 1.4f;
		m_tPlayerInfo.fAtk *= 1.1f;
		m_tPlayerInfo.fMaxHp *= 1.2f;
		m_tPlayerInfo.fHp = m_tPlayerInfo.fMaxHp;
		m_tPlayerInfo.fMaxMp *= 1.2f;
		m_tPlayerInfo.fMp = m_tPlayerInfo.fMaxMp;
		m_tPlayerInfo.fMaxExp *= 1.5f;
		m_tPlayerInfo.fExp = 0;
	}
}

void CPlayer::Render(HDC hDC)
{
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();
	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(m_strFrameKey.c_str());
	NULL_CHECK(hMemDC);

	CObj::UpdateRect();
	CObj::UpdateCollisionRect(70.f, 50.f, 70.f, 50.f);

	if (g_bInvenWindowOpen )
		DrawInven(hDC);
		
	if(g_bEquipWindowOpen)
		DrawEquip(hDC);
	
	if (g_bStatWindowOpen)
		DrawStat(hDC);



	GdiTransparentBlt(hDC, m_tRect.left - (int)fScrollX, m_tRect.top - (int)fScrollY,
		(int)m_tInfo.fCX, (int)m_tInfo.fCY,
		hMemDC,
		m_tFrame.m_dwFrameStart * (int)m_tInfo.fCX,
		m_tFrame.m_dwFrameScene * (int)m_tInfo.fCY,
		(int)m_tInfo.fCX, (int)m_tInfo.fCY, REMOVE_PINK);


}

void CPlayer::Release()
{

}

void CPlayer::ScrollLock()
{
	float fRightOffSet = WINCX*0.5f + 100.f;
	float fLeftOffSet = WINCX*0.5f - 100.f;

	float fTopOffSet = WINCY*0.5f - 130.f;//200
	float fBottomOffSet = WINCY*0.5f + 100.f;//400

	if (fRightOffSet <= m_tInfo.fX - CScrollMgr::GetScrollX())
		CScrollMgr::MoveScrollX(m_fSpeed);

	if (fLeftOffSet >= m_tInfo.fX - CScrollMgr::GetScrollX())
		CScrollMgr::MoveScrollX(-m_fSpeed);

	if (fBottomOffSet <= m_tInfo.fY - CScrollMgr::GetScrollY())
		CScrollMgr::MoveScrollY(m_fSpeed);

	if (fTopOffSet >= m_tInfo.fY - CScrollMgr::GetScrollY())
		CScrollMgr::MoveScrollY(-m_fSpeed);

}

float jumpY = 0;

void CPlayer::IsJumping()
{
	if (m_bIsUp && !m_bIsDown && m_bStateLock && fJumpCount < JUMP_TOP)
	{

		m_tInfo.fY -= m_fJumpSpeed;
		fJumpCount += m_fJumpSpeed;
	}
	else if (m_bStateLock && JUMP_TOP <= fJumpCount)
	{
		m_bIsUp = false;
		m_bIsDown = true;
	}
}

void CPlayer::IsAtkMotioning()
{
	if (m_bAtkMotion&&dwAtkCount + 1000 > GetTickCount())
	{
		m_bAtkMotion = true;
	}
	else if (m_bAtkMotion&&dwAtkCount + 1000 <= GetTickCount())
	{
		m_bAtkMotion = false;
		dwAtkCount = GetTickCount();
	}
}
void CPlayer::IsFalling()
{
	if (!m_bIsUp&&m_bIsDown && m_bStateLock&& fJumpCount>0&&(!m_bHangUp||!m_bHangDown))
	{
		m_tInfo.fY += m_fJumpSpeed;

		fJumpCount -= m_fJumpSpeed;

	}
	else if (fJumpCount <= 0 && !m_bHangUp && !m_bHangDown)
	{
		m_bStateLock = false;
		m_bIsDown = false;
		m_bIsJump = false;
	}

}




void CPlayer::KeyCheck()
{
	if (CKeyMgr::GetInstance()->KeyDown('Z'))
		m_bIsEat = true;
	else
		m_bIsEat = false;
	

	if (m_bIsHang)//줄에 닿았을때와 땅에 있지않다는 조건이 만족할때만 줄을 탄다 
	{
		if (CKeyMgr::GetInstance()->KeyPressing(VK_UP))
		{
			m_bStayHang = true;
			m_bHangUp = true;
			m_bHangDown = false;
			m_tInfo.fY -= 2.f;
			m_eCurState = eUP1;
		}
		else if (CKeyMgr::GetInstance()->KeyPressing(VK_DOWN))
		{
			m_bStayHang = true;
			m_bHangDown = true;
			m_bHangUp = false;
			m_tInfo.fY += 2.f;
			m_eCurState = eUP1;
		}
		else
			m_bHangDown = false;
			m_bHangUp = false;
		
	}

	if (!m_bIsJump)//점프 상태일때는 점프를 다시 누를수없게 
	{
		if (CKeyMgr::GetInstance()->KeyDown(VK_SPACE))
		{
			CSoundMgr::GetInstance()->PlaySoundw(L"Player_Jump.mp3", CSoundMgr::PLAYER);
			if (CKeyMgr::GetInstance()->KeyPressing(VK_RIGHT))
			{
				m_tInfo.fX += m_fSpeed;
				m_bStayHang = false;
			}
			else if (CKeyMgr::GetInstance()->KeyPressing(VK_LEFT))
			{
				m_tInfo.fX -= m_fSpeed;
				m_bStayHang = false;
			}
			m_eCurState = eJUMP;
			m_bIsJump = true;
			m_bIsUp = true;
			m_bStateLock = true;
		}
	}

	
	if (m_bStateLock)//점프중이면서 동시에 밧줄에 매달려있지않을때 
	{
		if (CKeyMgr::GetInstance()->KeyPressing(VK_RIGHT))
		{
			m_bDir = FACE_RIGHT;
			m_strFrameKey = L"basic_right";
			
			m_tInfo.fX += 3.f;
			m_eCurState = eJUMP;
		}
		else if (CKeyMgr::GetInstance()->KeyPressing(VK_LEFT))
		{
			m_bDir = FACE_LEFT;
			m_tInfo.fX -= 3.f;
			m_strFrameKey = L"basic_left";
			m_eCurState = eJUMP;
		}
	}

	else if (!m_bStateLock && !m_bAtkMotion)//점프중일때는 다른 키 입력 불가능하면너 동시에 밧줄에 없을때 움직임 
	{
		if (!m_bIsHang&&m_bIsGround&&CKeyMgr::GetInstance()->KeyPressing(VK_DOWN))
		{
			m_eCurState = eDOWN;
		}
		else if (CKeyMgr::GetInstance()->KeyPressing(VK_RIGHT) && (!m_bHangUp || !m_bHangDown))
		{
			if (!m_bIsGround && !m_bIsHang)
				m_tInfo.fY += m_fJumpSpeed + 2.f;
			m_bDir = FACE_RIGHT;
			m_strFrameKey = L"basic_right";
			if (!m_bStayHang)
			{
				m_eCurState = eWALK;
				m_tInfo.fX += m_fSpeed;
			}
		}
		else if (CKeyMgr::GetInstance()->KeyPressing(VK_LEFT) && (!m_bHangUp || !m_bHangDown))
		{	
			if (!m_bIsGround && !m_bIsHang)
				m_tInfo.fY += m_fJumpSpeed + 2.f;

			m_bDir = FACE_LEFT;
			m_strFrameKey = L"basic_left";	
			if (!m_bStayHang)
			{
				m_eCurState = eWALK;
				m_tInfo.fX -= m_fSpeed;
			}
		}
		else if (!m_bIsGround && !m_bHangDown && !m_bHangUp && !m_bStayHang)//땅에 있지도 않고 매달린채로 위아래키도 안누르고 있고 스테이도 없을때만 
		{
			m_eCurState = eJUMP;
			m_tInfo.fY += m_fJumpSpeed + 2.f;
		}
		else if (!m_bIsHang)
			m_eCurState = eIDLE;

	}
	/////////////////////////////////////////////////////////////////

	if (!m_bIsHang&&m_tPlayerInfo.fMp >= 0 && !m_bAtkMotion)
	{
		if (CKeyMgr::GetInstance()->KeyDown(VK_LSHIFT))
		{
				m_bAtkMotion = true; 
				m_eCurState = eBYEOKRYEOK;
				
	CSoundMgr::GetInstance()->PlaySoundw(L"Effect_ByeokRyeok.mp3", CSoundMgr::PLAYER);
				CObjMgr::GetInstance()->AddObject(CreateSkill<CSkill>(BYOEKRYEOK, m_bDir), CObjMgr::SKILL);
				m_tPlayerInfo.fMp -= SKILL_CONST_MP;
		}
		else if (CKeyMgr::GetInstance()->KeyDown('S'))
		{
			m_bAtkMotion = true;
			m_eCurState = eBYEOKRYEOK;
			CObjMgr::GetInstance()->AddObject(CreateSkill<CSkill>(NOESEONG, m_bDir), CObjMgr::SKILL);

			m_tPlayerInfo.fMp -= SKILL_CONST_MP;
		}
		else if (CKeyMgr::GetInstance()->KeyDown('D'))
		{
			m_bAtkMotion = true;
			m_eCurState = eBYEOKRYEOK;

			CSoundMgr::GetInstance()->PlaySoundw(L"Effect_TaePung.mp3", CSoundMgr::PLAYER);
			CObjMgr::GetInstance()->AddObject(CreateSkill<CSkill>(TAEPUNG, m_bDir), CObjMgr::SKILL);
			m_tPlayerInfo.fMp -= SKILL_CONST_MP;
		}
		else if (CKeyMgr::GetInstance()->KeyDown(VK_LCONTROL))
		{
			m_bAtkMotion = true;
			m_eCurState = eBYEOKRYEOK;

			CSoundMgr::GetInstance()->PlaySoundw(L"Effect_SeomMyeol.mp3", CSoundMgr::PLAYER);
			CObjMgr::GetInstance()->AddObject(CreateSkill<CSkill>(SEOMMYEOL, m_bDir), CObjMgr::SKILL);
			m_tPlayerInfo.fMp -= SKILL_CONST_MP;

		}
		else if (CKeyMgr::GetInstance()->KeyDown('A'))
		{
			m_bAtkMotion = true;
			m_eCurState = eBYEOKRYEOK;

			CSoundMgr::GetInstance()->PlaySoundw(L"Effect_SeungChun.mp3", CSoundMgr::PLAYER);
			CObjMgr::GetInstance()->AddObject(CreateSkill<CSkill>(SEUNGCHUN, m_bDir), CObjMgr::SKILL);
			m_tPlayerInfo.fMp -= SKILL_CONST_MP;
		}
	}
	if (CKeyMgr::GetInstance()->KeyUp('Q'))
	{
		for (int i = 0; i < m_vecEquipment.size(); i++)
		{
			if (strcmp(m_vecEquipment[i]->GetItemInfo().strName, "체력포션 "))
			{
				if (m_tPlayerInfo.fHp > m_tPlayerInfo.fMaxHp)
					m_tPlayerInfo.fHp = m_tPlayerInfo.fMaxHp;
				else
				{
					m_tPlayerInfo.fHp += m_vecEquipment[i]->GetItemInfo().fHp;
					if (m_tPlayerInfo.fHp > m_tPlayerInfo.fMaxHp)
					{
						m_tPlayerInfo.fHp = m_tPlayerInfo.fMaxHp;
					}
				}
				CSoundMgr::GetInstance()->PlaySoundw(L"Item_Potion.mp3", CSoundMgr::EFFECT);
			}
		}
	}
	if (CKeyMgr::GetInstance()->KeyUp('W'))
	{
		for (int i = 0; i < m_vecEquipment.size(); i++)
		{
			if (strcmp(m_vecEquipment[i]->GetItemInfo().strName, "마나포션 "))
			{
				if (m_tPlayerInfo.fMp > m_tPlayerInfo.fMaxMp)
					m_tPlayerInfo.fMp = m_tPlayerInfo.fMaxMp;
				else
				{
					m_tPlayerInfo.fMp += m_vecEquipment[i]->GetItemInfo().fMp;
					if (m_tPlayerInfo.fMp > m_tPlayerInfo.fMaxMp)
					{
						m_tPlayerInfo.fMp = m_tPlayerInfo.fMaxMp;
					}
				}
				CSoundMgr::GetInstance()->PlaySoundw(L"Item_Potion.mp3", CSoundMgr::EFFECT);
			}
		}
	}
}



void CPlayer::SceneChange()
{
	if (m_eCurState != m_ePreState)
	{
		switch (m_eCurState)
		{
		case eIDLE:
			m_tFrame.m_dwFrameScene = 0;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 3;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 200;
			break;

		case eWALK:
			m_tFrame.m_dwFrameScene = 1;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 3;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 200;
			break;

		case eJUMP:
			m_tFrame.m_dwFrameScene = 2;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 0;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 0;
			break;

		case eDOWN:
			m_tFrame.m_dwFrameScene = 3;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 0;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 200;
			break;

		case eATTACK:
			m_tFrame.m_dwFrameScene = 6;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 2;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 200;
			break;

		case eUP1:
			m_tFrame.m_dwFrameScene = 9;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 1;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 200;
			break;
		case eUP2:
			m_tFrame.m_dwFrameScene = 10;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 1;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 200;
			break;

		case eSKILL:
			m_tFrame.m_dwFrameScene = 4;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 1;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 200;
			break;

		case eBYEOKRYEOK:
			m_tFrame.m_dwFrameScene = 5;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 1;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 200;
			break;
		}
		m_ePreState = m_eCurState;
	}
}

void CPlayer::DrawInven(HDC hDC)
{
	if (!m_vecInven.empty())
	{
		for (int i = 0; i < m_vecInven.size(); i++)
		{
			HDC hMemDC = nullptr;
			if (m_vecInven[i]->GetItemType() == eHPPOTION)
			{
				hMemDC = CBmpMgr::GetInstance()->FindImage(L"HpPotion");
				NULL_CHECK(hMemDC);
			}
			else if (m_vecInven[i]->GetItemType() == eMPPOTION)
			{
				hMemDC = CBmpMgr::GetInstance()->FindImage(L"MpPotion");
				NULL_CHECK(hMemDC);
			}
			else if (m_vecInven[i]->GetItemType() == eWEAR)
			{
				hMemDC = CBmpMgr::GetInstance()->FindImage(L"Wear");
				NULL_CHECK(hMemDC);
			}
			else if (m_vecInven[i]->GetItemType() == eGLOVES)
			{
				hMemDC = CBmpMgr::GetInstance()->FindImage(L"Glove");
				NULL_CHECK(hMemDC);
			}
			else  if (m_vecInven[i]->GetItemType() == eSHOES)
			{
				hMemDC = CBmpMgr::GetInstance()->FindImage(L"Shoes");
				NULL_CHECK(hMemDC);
			}
			else if (m_vecInven[i]->GetItemType() == eWEAPON)
			{
				hMemDC = CBmpMgr::GetInstance()->FindImage(L"Weapon");
				NULL_CHECK(hMemDC);
			}
			GdiTransparentBlt(hDC, m_vecInven[i]->GetRect().left  , m_vecInven[i]->GetRect().top ,
				32, 32,
				hMemDC, 0, 0,
				32, 32, REMOVE_PINK);
		}
	}
	TCHAR szBuf[64] = L"";
	RECT rc = { 460, 318, 470, 318 };
	swprintf_s(szBuf, L"%d 메소", (int)m_tPlayerInfo.fMoney);
	DrawText(hDC, szBuf, lstrlen(szBuf), &rc, DT_NOCLIP);
}
void CPlayer::DrawEquip(HDC hDC)
{

	if (!m_vecEquipment.empty())
	{
		HDC hMemDC = nullptr;
		for (int i = 0; i < m_vecEquipment.size(); i++)
		{
			if (m_vecEquipment[i]->GetItemType() == eHPPOTION)
			{
				 hMemDC = CBmpMgr::GetInstance()->FindImage(L"HpPotion");
				NULL_CHECK(hMemDC);
			}
			 if (m_vecEquipment[i]->GetItemType() == eMPPOTION)
			{
				 hMemDC = CBmpMgr::GetInstance()->FindImage(L"MpPotion");
				NULL_CHECK(hMemDC);
			
			}
			if (m_vecEquipment[i]->GetItemType() == eWEAR)
			{
				 hMemDC = CBmpMgr::GetInstance()->FindImage(L"Wear");
				NULL_CHECK(hMemDC);
			
			}
			 if (m_vecEquipment[i]->GetItemType() == eGLOVES)
			{
				 hMemDC = CBmpMgr::GetInstance()->FindImage(L"Glove");
				NULL_CHECK(hMemDC);
				
			}
			  if (m_vecEquipment[i]->GetItemType() == eSHOES)
			{
				 hMemDC = CBmpMgr::GetInstance()->FindImage(L"Shoes");
				NULL_CHECK(hMemDC);
			
			}
			 if (m_vecEquipment[i]->GetItemType() == eWEAPON)
			{
				 hMemDC = CBmpMgr::GetInstance()->FindImage(L"Weapon");
				NULL_CHECK(hMemDC);
			
			}	
			 GdiTransparentBlt(hDC, m_vecEquipment[i]->GetRect().left , m_vecEquipment[i]->GetRect().top ,
				32, 32,
				hMemDC, 0, 0,
				32, 32, REMOVE_PINK);
		}
	}
}
void CPlayer::DrawStat(HDC hDC)
{
		TCHAR szBuf[64] = L"";
		RECT rc = { 215, 134, 225, 134 };
		swprintf_s(szBuf, L"정성호");
		DrawText(hDC, szBuf, lstrlen(szBuf), &rc, DT_NOCLIP);
		rc = { 198, 162, 225, 162 };
		swprintf_s(szBuf, L"스트라이커");
		DrawText(hDC, szBuf, lstrlen(szBuf), &rc, DT_NOCLIP);
		rc = { 230, 186, 270, 186 };
		swprintf_s(szBuf, L"%d", (int)m_tPlayerInfo.fAtk);
		DrawText(hDC, szBuf, lstrlen(szBuf), &rc, DT_NOCLIP);
		rc = { 230, 210, 270, 210 };
		swprintf_s(szBuf, L"%d", (int)m_tPlayerInfo.fMaxHp);
		DrawText(hDC, szBuf, lstrlen(szBuf), &rc, DT_NOCLIP);
		rc = { 230, 234, 270, 234 };
		swprintf_s(szBuf, L"%d", (int)m_tPlayerInfo.fMaxMp);
		DrawText(hDC, szBuf, lstrlen(szBuf), &rc, DT_NOCLIP);
}

void CPlayer::InvenToEquip(CObj * pItem)
{
	if (pItem->GetItemType() == eHPPOTION)
	{
		pItem->SetPos(573, 75);
	}
	else if (pItem->GetItemType() == eMPPOTION)
	{
		pItem->SetPos(573, 106);
	}
	else if (pItem->GetItemType() == eWEAPON)
	{
		pItem->SetPos(673, 173);
	}
	else if (pItem->GetItemType() == eWEAR)
	{
		pItem->SetPos(606, 174);
	}
	else if (pItem->GetItemType() == eGLOVES)
	{
		pItem->SetPos(573, 206);
	}
	else if (pItem->GetItemType() == eSHOES)
	{
		pItem->SetPos(638, 239);
	}
	m_tPlayerInfo.fAtk += pItem->GetItemInfo().fAtk;
	m_tPlayerInfo.fMaxHp += pItem->GetItemInfo().fMaxHp;
	m_tPlayerInfo.fMaxMp += pItem->GetItemInfo().fMaxMp;
	for (auto& iter = m_vecInven.begin(); iter != m_vecInven.end();)
	{
		if ((*iter)->GetItemInfo().strName == pItem->GetItemInfo().strName)
			iter = m_vecInven.erase(iter);
		
		else
			++iter;
	}
	m_vecEquipment.push_back(pItem);

	g_iItemCount = 0;
}

void CPlayer::EquipToInven(CObj* pItem)
{
	SetInven(pItem);
	m_tPlayerInfo.fAtk -= pItem->GetItemInfo().fAtk;
	m_tPlayerInfo.fMaxHp -= pItem->GetItemInfo().fMaxHp;
	m_tPlayerInfo.fMaxMp -= pItem->GetItemInfo().fMaxMp;
	for (auto& iter = m_vecEquipment.begin(); iter != m_vecEquipment.end();)
	{
		if ((*iter)->GetItemInfo().strName == pItem->GetItemInfo().strName)
			iter = m_vecEquipment.erase(iter);

		else
			++iter;
	}
	m_vecInven.push_back(pItem);
}




