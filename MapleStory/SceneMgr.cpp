#include "stdafx.h"
#include "SceneMgr.h"

#include"Logo.h"
#include"Town.h"
#include"Dungeon.h"
#include"Title.h"
#include"MapEdit.h"
#include"MyMenu.h"
#include"DungeonTwo.h"
#include"BossDungeon.h"

IMPLEMENT_SINGLETON(CSceneMgr)

CSceneMgr::CSceneMgr()
	: m_pScene(nullptr), m_eCurScene(end), m_ePreScene(end)
{
}


CSceneMgr::~CSceneMgr()
{
	Release();
}

void CSceneMgr::SceneChange(SCENE_ID eID)
{
	m_eCurScene = eID;
	//State Pattern(상태 변환 패턴)
	//상속과 다형성을 이용하여 FSM을 보다 확장한 디자인 패턴.
	if (m_eCurScene != m_ePreScene)
	{
		SafeDelete(m_pScene);
		switch (m_eCurScene)
		{
		case LOGO:
			m_pScene = new CLogo;
			break;
		case TITLE:
			m_pScene = new CTitle;
			break;
		case CREATE:
			m_pScene = new CTitle;
			break;
		case TOWN:
			m_pScene = new CTown;
			break;
		case DUNGEON:
			m_pScene = new CDungeon;
			break;
		case DUNGEONTWO:
			m_pScene = new CDungeonTwo;
			break;
		case BOSSDUNGEON:
			m_pScene = new CBossDungeon;
			break;

		case EDIT:
			m_pScene = new CMapEdit;
			break;
		case MENU:
			m_pScene = new CMyMenu;
			break;
		}
		m_pScene->Initialize();
		m_ePreScene = m_eCurScene;
	}
}

void CSceneMgr::Update()
{
	m_pScene->Update();
}

void CSceneMgr::LateUpdate()
{
	m_pScene->LateUpdate();
}

void CSceneMgr::Render(HDC hDC)
{
	m_pScene->Render(hDC);
}

void CSceneMgr::Release()
{
	SafeDelete(m_pScene);
}
