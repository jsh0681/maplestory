#include "stdafx.h"
#include "Mouse.h"


CMouse::CMouse(): m_eCurState(end), m_ePreState(end)
{
}


CMouse::~CMouse()
{
	Release();
}

void CMouse::Initialize()
{
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Mouse/Cursor.bmp", L"Cursor");
	m_tInfo = { 0.f,0.f,32.f,32.f };


	m_eLayer = CObjMgr::LAYER_MOUSE;
	m_strFrameKey = L"Cursor";
	m_eCurState = IDLE;
	m_ePreState = m_eCurState;

	m_tFrame.m_dwFrameStart = 0;//비트맵의 그림 시작번호
	m_tFrame.m_dwFrameEnd = 1;//비트맵에서 그림이 있는 마지막 번호
	m_tFrame.m_dwFrameScene = 0;//줄수를 의미함

}

void CMouse::LateInit()
{
}

int CMouse::Update()
{
	CObj::LateInit();
	POINT pt = {};
	GetCursorPos(&pt);
	ScreenToClient(g_hWnd, &pt);

	m_tInfo.fX = (float)pt.x;
	m_tInfo.fY = (float)pt.y;

	return NO_EVENT;
}

void CMouse::LateUpdate()
{
	MouseCheck();
	StateChange();
}

void CMouse::Render(HDC hDC)
{
	CObj::UpdateRect(); 
	CObj::UpdateCollisionRect(5,5 ,40,40);
	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(m_strFrameKey.c_str());
	NULL_CHECK(hMemDC);

	GdiTransparentBlt(hDC, m_tCollisionRect.left, m_tCollisionRect.top,
			 (int)m_tInfo.fCX, (int)m_tInfo.fCY,
			hMemDC,
			 m_tFrame.m_dwFrameStart * (int)m_tInfo.fCX,
			 m_tFrame.m_dwFrameScene * (int)m_tInfo.fCY,
			 (int)m_tInfo.fCX, (int)m_tInfo.fCY, REMOVE_PINK);

}

void CMouse::Release()
{
	
}

void CMouse::MouseCheck()
{
	if (CKeyMgr::GetInstance()->KeyUp(VK_LBUTTON))
		m_eCurState = IDLE;
	else if (CKeyMgr::GetInstance()->KeyPressing(VK_LBUTTON))
		m_eCurState = PRESSING;
	else if (CKeyMgr::GetInstance()->KeyDown(VK_LBUTTON))
		m_eCurState = CLICK;
	else
		m_eCurState = IDLE;
}

void CMouse::StateChange()
{
	if (m_eCurState != m_ePreState)
	{
		switch (m_eCurState)
		{
		case IDLE:
			m_tFrame.m_dwFrameScene = 0;
			break;

		case CLICK:
			m_tFrame.m_dwFrameScene = 1;
			break;

		case PRESSING:
			m_tFrame.m_dwFrameScene = 2;
			break;
		}
		m_ePreState = m_eCurState;
	}
}

POINT CMouse::GetMousePos()
{
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();

	POINT pt = {};
	GetCursorPos(&pt);
	ScreenToClient(g_hWnd, &pt);

	pt.x += (int)fScrollX;
	pt.y += (int)fScrollY;


	return pt;
}
