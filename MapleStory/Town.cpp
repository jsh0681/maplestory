#include "stdafx.h"
#include "Town.h"
#include "Player.h"
#include "Monster.h"
#include"BossMonster.h"
#include "Mouse.h"
#include"UI.h"
#include"Item.h"
#include"Shop.h"
#include"Inven.h"



 bool g_bTownToDungeon=true;
 bool g_bDungeonToDungeonTwo=true;
 bool g_bDungeonTwoToBoss=true ;

CTown::CTown()
{
}


CTown::~CTown()
{
	Release();
}

void CTown::Initialize()
{
	ShowCursor(false);

	m_tInfo = { 0,0,58,76 };
	CTileMgr::GetInstance()->LoadTownData();

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Map/Town.bmp", L"Town");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Ui/Portal3.bmp", L"Portal");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/NPC/PotionNpc.bmp", L"PotionNpc");

	CSoundMgr::GetInstance()->PlayBGM(L"Bgm_Town.wav");

	m_tFrame.m_dwFrameStart = 0;//비트맵의 그림 시작번호
	m_tFrame.m_dwFrameEnd = 7;//비트맵에서 그림이 있는 마지막 번호
	m_tFrame.m_dwFrameTime = GetTickCount();
	m_tFrame.m_dwFrameSpd = 200;
	g_bTown = true;
	g_bDungeon = false;
	g_bDungeonTwo = false;
	g_bBoss = false;
}

void CTown::Update()
{
	m_pPlayer = CObjMgr::GetInstance()->GetPlayer();
	CScene::MoveFrame();
	CTileMgr::GetInstance()->Update();
	CObjMgr::GetInstance()->Update();
	ScrollLock();
}

void CTown::LateUpdate()
{
	CObjMgr::GetInstance()->LateUpdate(); 

}

void CTown::Render(HDC hDC)
{
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();
	

	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Town");
	NULL_CHECK(hMemDC);
	BitBlt(hDC, 0, 0, WINCX, WINCY, hMemDC, (int)fScrollX, (int)fScrollY, SRCCOPY);

	hMemDC = CBmpMgr::GetInstance()->FindImage(L"Portal");
	NULL_CHECK(hMemDC);
	GdiTransparentBlt(hDC, 84 - (int)fScrollX, 296 - (int)fScrollY, 70, 202, hMemDC, m_tFrame.m_dwFrameStart * 70, 0, 70, 202, REMOVE_PINK);

	hMemDC = CBmpMgr::GetInstance()->FindImage(L"PotionNpc");
	NULL_CHECK(hMemDC);
	GdiTransparentBlt(hDC, 440 - (int)fScrollX, 420 - (int)fScrollY, 58, 76, hMemDC, m_tFrame.m_dwFrameStart * 58, 0, 58, 76, REMOVE_PINK);

	
	CObjMgr::GetInstance()->Render(hDC);

	if (430 < m_pPlayer->GetInfo().fX &&m_pPlayer->GetInfo().fX < 490)
	{
		if (CKeyMgr::GetInstance()->KeyPressing(VK_UP)&&!g_bShopWindowOpen)
		{
			g_bShopWindowOpen = true;
			CSoundMgr::GetInstance()->PlaySoundw(L"Window_Open.mp3", CSoundMgr::UI);
		}
	}
	else if((440 > m_pPlayer->GetInfo().fX ||m_pPlayer->GetInfo().fX > 480)&&g_bShopWindowOpen)
	{
		CSoundMgr::GetInstance()->PlaySoundw(L"Window_Close.mp3", CSoundMgr::UI);
		g_bShopWindowOpen = false;
	}



	if (100<m_pPlayer->GetInfo().fX &&m_pPlayer->GetInfo().fX < 140 && 510>m_pPlayer->GetInfo().fY &&m_pPlayer->GetInfo().fY > 410&& g_bTownToDungeon)
	{
	
		if (CKeyMgr::GetInstance()->KeyPressing(VK_UP))
		{
			m_pPlayer->SetPos(1430, 1100);
			CScrollMgr::SetScrollX(698);
			CScrollMgr::SetScrollY(703);
			g_bTownToDungeon = false;
			CSoundMgr::GetInstance()->PlaySoundw(L"Effect_Portal.mp3", CSoundMgr::UI);
			CSoundMgr::GetInstance()->StopSound(CSoundMgr::BGM);
			CSceneMgr::GetInstance()->SceneChange(CSceneMgr::DUNGEON);
			return;
		}
	}
	else if (100 > m_pPlayer->GetInfo().fX || m_pPlayer->GetInfo().fX > 140)
	{
		g_bTownToDungeon = true;
	}

}

void CTown::Release()
{
	CTileMgr::GetInstance()->DestroyInstance();
}

void CTown::ScrollLock()
{
	if (5 >= CScrollMgr::GetScrollX())
		CScrollMgr::SetScrollX(5.f);

	if (0.f > CScrollMgr::GetScrollY())
		CScrollMgr::SetScrollY(0.f);

	if (TOWN_X - WINCX< CScrollMgr::GetScrollX())
		CScrollMgr::SetScrollX(TOWN_X - WINCX);

	if (TOWN_Y - WINCY < CScrollMgr::GetScrollY())
		CScrollMgr::SetScrollY(TOWN_Y - WINCY);
}
