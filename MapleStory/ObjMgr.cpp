#include "stdafx.h"
#include "ObjMgr.h"
#include "Obj.h"

IMPLEMENT_SINGLETON(CObjMgr)

CObjMgr::CObjMgr()
{
}


CObjMgr::~CObjMgr()
{
	Release();
}

CObj * CObjMgr::GetPlayer()
{
	if (m_ObjList[PLAYER].empty())
		return nullptr;

	return m_ObjList[PLAYER].front();
}

CObj * CObjMgr::GetInven()
{
	if (m_ObjList[INVEN].empty())
		return nullptr;

	return m_ObjList[INVEN].front();
}

CObj * CObjMgr::GetTarget(CObj* pSrc, OBJECTID eID)
{
	if (m_ObjList[eID].empty())
		return nullptr;

	CObj* pTarget = m_ObjList[eID].front();
	float fDistance = CMathMgr::CalcDistance(pTarget, pSrc);

	for (auto& pObject : m_ObjList[eID])
	{
		float fCmpDist = CMathMgr::CalcDistance(pObject, pSrc);

		if (fDistance > fCmpDist)
		{
			pTarget = pObject;
			fDistance = fCmpDist;
		}
	}

	return pTarget;
}

void CObjMgr::AddObject(CObj * pObj, OBJECTID eID)
{
		m_ObjList[eID].push_back(pObj);
}

void CObjMgr::Update()
{
	for (int i = 0; i < END; ++i)
	{
		for (OBJITER iter = m_ObjList[i].begin(); iter != m_ObjList[i].end(); )
		{
			int iEvent = (*iter)->Update();

			if (DEAD_OBJ == iEvent)
			{
				SafeDelete(*iter);
				iter = m_ObjList[i].erase(iter);
			}
			else
				++iter;	
			if (m_ObjList[i].empty())
				break;
		}
	}
}

void CObjMgr::LateUpdate()
{
	for (int i = 0; i < END; ++i)
	{
		for (auto& pObject : m_ObjList[i])
		{
			pObject->LateUpdate(); 
			
			if (m_ObjList[i].empty())
				break;

			OBJECTLAYER eLayerID = pObject->GetLayer();
			m_RenderLst[eLayerID].push_back(pObject);
		}
	}
	if (g_bDungeon )
	{
		CCollisionMgr::CollisionTiletoMonster(m_vecTile, m_ObjList[MONSTER]);
		CCollisionMgr::CollisionPlayerToMonster(m_ObjList[PLAYER], m_ObjList[MONSTER]);
		CCollisionMgr::CollisionSkillToMonster(m_ObjList[SKILL], m_ObjList[MONSTER]);
		CCollisionMgr::CollisionPlayerToMoney(m_ObjList[PLAYER], m_ObjList[ITEM]);
	}
	if (g_bDungeonTwo)
	{
		CCollisionMgr::CollisionTiletoBlueMushroom(m_vecTile, m_ObjList[BLUEMUSHROOM]);
		CCollisionMgr::CollisionPlayerToBlueMushroom(m_ObjList[PLAYER], m_ObjList[BLUEMUSHROOM]);
		CCollisionMgr::CollisionSkillToBlueMushroom(m_ObjList[SKILL], m_ObjList[BLUEMUSHROOM]);
		CCollisionMgr::CollisionPlayerToMoney(m_ObjList[PLAYER], m_ObjList[ITEM]);
	}
	if (g_bBoss)
	{
		CCollisionMgr::CollisionSkillToBoss(m_ObjList[SKILL], m_ObjList[BOSS]);
		CCollisionMgr::CollisionBossSkillToPlayer(m_ObjList[BOSSSKILL], m_ObjList[PLAYER]);
	}
	CCollisionMgr::CollisionTile(m_vecTile, m_ObjList[PLAYER]);
	CCollisionMgr::CollisionMouseToItem(m_ObjList[MOUSE], m_ObjList[ITEM]);

}

void CObjMgr::Render(HDC hDC)
{
	for (int i = 0; i < LAYER_END; ++i)
	{
		// y값 기준으로 오브젝트 렌더링 정렬.
		m_RenderLst[i].sort([](auto& pDst, auto& pSrc)/*->bool*/
		{
			return pDst->GetInfo().fY < pSrc->GetInfo().fY;	// 오름차순
		});

		for (auto& pObject : m_RenderLst[i])
			pObject->Render(hDC);

		m_RenderLst[i].clear();	// 누적방지
	}
/*
	for (int i = 0; i < LAYER_END; ++i)
	{
		for (auto& pObject : m_ObjList[i])
			pObject->Render(hDC);
	}*/
}

void CObjMgr::Release()
{
	for (int i = 0; i < END; ++i)
	{
		for_each(m_ObjList[i].begin(), m_ObjList[i].end(),
			[](auto& ptr)
		{
			if (ptr)
			{
				delete ptr;
				ptr = nullptr;
			}
		});
		{
			m_ObjList[i].clear();
		}
	}
}

void CObjMgr::DeleteGroup(OBJECTID eID)
{
	for_each(m_ObjList[eID].begin(), m_ObjList[eID].end(),
		[](auto& ptr)
	{
		if (ptr)
		{
			delete ptr;
			ptr = nullptr;
		}
	});
	m_ObjList[eID].clear();
}
