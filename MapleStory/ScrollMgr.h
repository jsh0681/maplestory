#pragma once
class CScrollMgr
{
public:
	CScrollMgr();
	~CScrollMgr();

public:
	static float GetScrollX() { return m_fScrollX; }
	static float GetScrollY() { return m_fScrollY; }

public:
	static void SetScrollX(float x) { m_fScrollX = x; }
	static void SetScrollY(float y) { m_fScrollY = y; }

public:
	static void MoveScrollX(float x) { m_fScrollX += x; }
	static void MoveScrollY(float y) { m_fScrollY += y; }

private:
	static float m_fScrollX;
	static float m_fScrollY;
};

