#include "stdafx.h"
#include "Maingame.h"
#include "Player.h"
#include "Monster.h"
#include "ObjMgr.h"


CMaingame::CMaingame()
{
}


CMaingame::~CMaingame()
{
	Release();
}

void CMaingame::Initialize()
{
	m_hDC = GetDC(g_hWnd);
	srand(unsigned(time(nullptr)));
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Back.bmp", L"Back");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/BackBuffer.bmp", L"BackBuffer");
	
	CSoundMgr::GetInstance()->Initialize();

	CSceneMgr::GetInstance()->SceneChange(CSceneMgr::MENU);

}

void CMaingame::Update()
{
	CSceneMgr::GetInstance()->Update();
	CSoundMgr::GetInstance()->Update();
}

void CMaingame::LateUpdate()
{
	CSceneMgr::GetInstance()->LateUpdate();
	CKeyMgr::GetInstance()->UpdateKey();
}

void CMaingame::Render()
{

	HDC hBackBufferDC = CBmpMgr::GetInstance()->FindImage(L"BackBuffer");
	NULL_CHECK(hBackBufferDC);

	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Back");
	NULL_CHECK(hMemDC);

	BitBlt(hBackBufferDC, 0, 0, WINCX, WINCY, hMemDC, 0, 0, SRCCOPY);

	CSceneMgr::GetInstance()->Render(hBackBufferDC);

	BitBlt(m_hDC, 0, 0, WINCX, WINCY, hBackBufferDC, 0, 0, SRCCOPY);
}


void CMaingame::Release()
{
	ReleaseDC(g_hWnd, m_hDC);
	CSoundMgr::GetInstance()->DestroyInstance();
	CSceneMgr::GetInstance()->DestroyInstance();
	CBmpMgr::GetInstance()->DestroyInstance();
	CKeyMgr::GetInstance()->DestroyInstance();
	CObjMgr::GetInstance()->DestroyInstance();

}
