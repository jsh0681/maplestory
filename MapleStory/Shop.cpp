#include "stdafx.h"
#include "Shop.h"
#include"Item.h"

bool g_bShopWindowOpen = false;

CShop::CShop():m_bShopWindowOpen(false)
{
	
}


CShop::~CShop()
{
	Release();
}

void CShop::Initialize()
{
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Item/Money2.bmp", L"Money");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Item/HpPotion.bmp", L"HpPotion");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Item/MpPotion.bmp", L"MpPotion");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Item/Wear.bmp", L"Wear");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Item/Glove.bmp", L"Glove");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Item/Weapon.bmp", L"Weapon");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Item/Shoes.bmp", L"Shoes");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Ui/Shop.bmp", L"Shop");
	m_tInfo = { 100,30,276,505 };//상점 크기 
	m_eLayer = CObjMgr::LAYER_UI;

}

void CShop::LateInit()
{
	CObj* pObject = CAbstactFactory<CItem>::CreateItem(m_tInfo.fX + 28, m_tInfo.fY + 143,"체력포션","아이템",50,0,0,0,0,500,eHPPOTION);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::ITEM);

	pObject = CAbstactFactory<CItem>::CreateItem(m_tInfo.fX + 28, m_tInfo.fY + 185, "마나포션", "아이템", 0,0,50,0, 0, 500, eMPPOTION);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::ITEM);

	pObject = CAbstactFactory<CItem>::CreateItem(m_tInfo.fX + 28, m_tInfo.fY + 227, "장갑", "아이템", 0,50,0, 0, 50, 300, eGLOVES);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::ITEM);

	pObject = CAbstactFactory<CItem>::CreateItem(m_tInfo.fX + 28, m_tInfo.fY + 269, "신발", "아이템", 0,50, 0,0, 50, 300, eSHOES);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::ITEM);

	pObject = CAbstactFactory<CItem>::CreateItem(m_tInfo.fX + 28, m_tInfo.fY + 311, "무기", "아이템", 0, 0,0,0, 100, 500, eWEAPON);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::ITEM);

	pObject = CAbstactFactory<CItem>::CreateItem(m_tInfo.fX + 28, m_tInfo.fY + 353, "갑옷", "아이템", 0,100,0, 100, 0, 500, eWEAR);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::ITEM);
}

int CShop::Update()
{
	CObj::LateInit();
	if (m_bIsDead)
		return DEAD_OBJ;

	return NO_EVENT;
}

void CShop::LateUpdate()
{
	
}


void CShop::Render(HDC hDC)
{
	CObj::UpdateRect();
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY(); 
	DrawShop(hDC);
}
void CShop::DrawShop(HDC hDC)
{
	if (g_bShopWindowOpen)
	{
		HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Shop");
		NULL_CHECK(hMemDC);
		GdiTransparentBlt(hDC, m_tInfo.fX, m_tInfo.fY, m_tInfo.fCX, m_tInfo.fCY, hMemDC, 0, 0, m_tInfo.fCX, m_tInfo.fCY, REMOVE_PINK);
	}
}

void CShop::Release()
{

}

