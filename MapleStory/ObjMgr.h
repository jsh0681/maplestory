#pragma once
class CObj;

class CObjMgr
{
	DECLARE_SINGLETON(CObjMgr)

public:

	enum OBJECTID { PLAYER, MONSTER,BLUEMUSHROOM, BOSS , MOUSE, ITEM , UI, BOSSSKILL,SHOP,INVEN , EFFECT, SKILL ,END };

	enum OBJECTLAYER { LAYER_BACK, LAYER_BOSS,  LAYER_MONSTER, LAYER_UI, LAYER_PLAYER,  LAYER_EFFECT, LAYER_SKILL,  LAYER_ITEM,LAYER_MOUSE, LAYER_END };

private:
	OBJLIST m_ObjList[END];
	OBJLIST	m_RenderLst[LAYER_END];
public:

	CObj * GetPlayer();

	CObj * GetInven();

	CObj * GetTarget(CObj * pSrc, OBJECTID eID);

private:
	CObjMgr();
	~CObjMgr();


public:
	void AddObject(CObj* pObj, OBJECTID eID);
	void Update();
	void LateUpdate();
	void Render(HDC hDC);
	void Release();

	void DeleteGroup(OBJECTID eID);

};

