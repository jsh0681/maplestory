#include "stdafx.h"
#include "LineMgr.h"
#include "Line.h"

IMPLEMENT_SINGLETON(CLineMgr)

CLineMgr::CLineMgr()
{
}


CLineMgr::~CLineMgr()
{
	Release();
}

void CLineMgr::Initialize()
{
	LoadData();

}

void CLineMgr::Render(HDC hDC)
{
	for (auto& pLine : m_LineLst)
		pLine->Render(hDC);
}

void CLineMgr::Release()
{
	for_each(m_LineLst.begin(), m_LineLst.end(), SafeDelete<CLine*>);
	m_LineLst.clear();
}


bool CLineMgr::CollisionLine(float fInX, float fInY, float * pOutY)
{
	if (m_LineLst.empty())
		return false;

	CLine* pTarget = nullptr;

	for (auto& pLine : m_LineLst)
	{
		float fMiddleY = (pLine->GetInfo().tLeft.fY + pLine->GetInfo().tRight.fY) / 2;
		float fYYY = fMiddleY - pLine->GetInfo().tLeft.fY;
		if (fMiddleY-50 <= fInY && fInY <= fMiddleY+50
			&&pLine->GetInfo().tLeft.fX <= fInX && fInX <= pLine->GetInfo().tRight.fX)
		{
			pTarget = pLine;
			break;
		}

	/*	if (pLine->GetInfo().tLeft.fY > pLine->GetInfo().tRight.fY)
		{
			if (pLine->GetInfo().tRight.fY <= fInY && fInY <= pLine->GetInfo().tLeft.fY
				&&pLine->GetInfo().tLeft.fX <= fInX && fInX <= pLine->GetInfo().tRight.fX)
			{
				pTarget = pLine;
				break;
			}
		}
		if (pLine->GetInfo().tLeft.fY < pLine->GetInfo().tRight.fY)
		{
			if (pLine->GetInfo().tLeft.fY <= fInY && fInY <= pLine->GetInfo().tRight.fY
				&&pLine->GetInfo().tLeft.fX <= fInX && fInX <= pLine->GetInfo().tRight.fX)
			{
				pTarget = pLine;
				break;
			}
		}*/
	}
	if (nullptr == pTarget)
		return false;

		float x1 = pTarget->GetInfo().tLeft.fX;
		float x2 = pTarget->GetInfo().tRight.fX;
		float y1 = pTarget->GetInfo().tLeft.fY;
		float y2 = pTarget->GetInfo().tRight.fY;
	
		*pOutY = (y2 - y1) / (x2 - x1) * (fInX - x1) + y1;
		return true;
		
}



bool CLineMgr::CollisionLine(float fInX, float * pOutY)
{
	if(m_LineLst.empty())
		return false;

	CLine* pTarget = nullptr;

	for (auto& pLine : m_LineLst)
	{
		if (pLine->GetInfo().tLeft.fX <= fInX && fInX <= pLine->GetInfo().tRight.fX)
		{
			pTarget = pLine;
			break;
		}
	}
	if (nullptr == pTarget)
		return false;
	float x1 = pTarget->GetInfo().tLeft.fX;
	float y1 = pTarget->GetInfo().tLeft.fY;
	float x2 = pTarget->GetInfo().tRight.fX;
	float y2 = pTarget->GetInfo().tRight.fY;

	*pOutY = (y2 - y1) / (x2 - x1) * (fInX - x1) + y1;
	return true;
}

void CLineMgr::LoadData()
{
	HANDLE hFile = CreateFile(L"../Line.dat", GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		MessageBox(g_hWnd, L"불러오기 실패!", L"Error", MB_OK);
		return;
	}
	LINEINFO tInfo = {};
	DWORD dwByte = 0;
	while (true)
	{
		ReadFile(hFile, &tInfo, sizeof(LINEINFO), &dwByte, nullptr);
		if (0 == dwByte)//다 읽어와서 파일안에 읽어올 자료가 없을경우 
		{
			break;
		}
		m_LineLst.push_back(new CLine(tInfo));
	}
	CloseHandle(hFile);
}