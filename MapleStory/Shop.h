#pragma once
#include "Obj.h"

class CShop
	:public CObj
{
public:
	CShop();
	~CShop();

public:
	virtual void Initialize()override;
	virtual  void LateInit();
	virtual int Update() override;
	virtual void LateUpdate();
	virtual void Render(HDC hDC)override;
	void DrawShop(HDC hDC);
	virtual void Release()override;
public:

	CObj* m_pPlayer;

	bool m_bShopWindowOpen;
};

