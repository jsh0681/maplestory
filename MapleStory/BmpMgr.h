#pragma once
class CMyBmp;

class CBmpMgr
{
	DECLARE_SINGLETON(CBmpMgr)
private:
	CBmpMgr();
	~CBmpMgr();
public:
	HDC FindImage(const TCHAR * pImageKey);
	void InsertBmp(const TCHAR* pFilePath, const TCHAR* pImageKey);

	void Release();

	void LoadTile();

public:
	map<const TCHAR*, CMyBmp*> m_MapBit;
};

