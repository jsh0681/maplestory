#include "stdafx.h"
#include "BossSkill.h"
#include "Player.h"

CBossSkill::CBossSkill() :m_eCurState(eEND), m_ePreState(eEND), m_dwStart(0), m_iDamage(0), m_pPlayer(nullptr)
{
	
}


CBossSkill::~CBossSkill()
{
	Release();
}

void CBossSkill::Initialize()
{
	m_tInfo = { 0,0,0,0 };
	m_eLayer = CObjMgr::LAYER_EFFECT;
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/BossSkill/BossCurse.bmp", L"BossCurse");//��õ

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/BossSkill/BossFire2.bmp", L"BossFire");//��ǳ

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/BossSkill/BossIce2.bmp", L"BossIce");//����

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/BossSkill/BossThunder.bmp", L"BossThunder");//����

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Skill/BossSkill/BossSun2.bmp", L"BossSun");//����

	m_eCurState = eEND;
	m_ePreState = m_eCurState;

}

void CBossSkill::LateInit()
{
	m_dwStart = GetTickCount();
}

int CBossSkill::Update()
{
	CObj::LateInit();

	if (m_bIsDead)
		return DEAD_OBJ;

	return NO_EVENT;
}

void CBossSkill::LateUpdate()
{
	m_pPlayer = CObjMgr::GetInstance()->GetPlayer();
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();
	m_eCurState = m_eSkillNum;
	SceneChange();
	CObj::EffectFrame();
}

void CBossSkill::Render(HDC hDC)
{
	CObj::UpdateRect();
	srand(unsigned(time(NULL)));

	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();
	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"DamageEffect");
	NULL_CHECK(hMemDC);
	if (m_iDamage / 10000 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 75, (int)(m_tInfo.fY - fScrollY) - 120, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 100000) / 10000), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 1000 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 50, (int)(m_tInfo.fY - fScrollY) - 120, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 10000) / 1000), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 100 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 25, (int)(m_tInfo.fY - fScrollY) - 120, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 1000) / 100), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 10 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX), (int)(m_tInfo.fY - fScrollY) - 120, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 100) / 10), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) + 25, (int)(m_tInfo.fY - fScrollY) - 120, 149, 69, hMemDC, 149 * ((m_iDamage + (rand() % (m_iDamage / 2))) % 10), 0, 149, 69, REMOVE_PINK);

	if (m_iDamage / 10000 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 75, (int)(m_tInfo.fY - fScrollY) - 90, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 100000) / 10000), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 1000 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 50, (int)(m_tInfo.fY - fScrollY) - 90, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 10000) / 1000), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 100 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 25, (int)(m_tInfo.fY - fScrollY) - 90, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 1000) / 100), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 10 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX), (int)(m_tInfo.fY - fScrollY) - 90, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 100) / 10), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) + 25, (int)(m_tInfo.fY - fScrollY) - 90, 149, 69, hMemDC, 149 * ((m_iDamage + (rand() % (m_iDamage / 2))) % 10), 0, 149, 69, REMOVE_PINK);

	if (m_iDamage / 10000 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 75, (int)(m_tInfo.fY - fScrollY) - 60, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 100000) / 10000), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 1000 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 50, (int)(m_tInfo.fY - fScrollY) - 60, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 10000) / 1000), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 100 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 25, (int)(m_tInfo.fY - fScrollY) - 60, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 1000) / 100), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 10 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX), (int)(m_tInfo.fY - fScrollY) - 60, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 100) / 10), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) + 25, (int)(m_tInfo.fY - fScrollY) - 60, 149, 69, hMemDC, 149 * ((m_iDamage + (rand() % (m_iDamage / 2))) % 10), 0, 149, 69, REMOVE_PINK);


	hMemDC = CBmpMgr::GetInstance()->FindImage(m_strFrameKey.c_str());
	NULL_CHECK(hMemDC);
	GdiTransparentBlt(hDC, m_tRect.left - (int)fScrollX, m_tRect.top - (int)fScrollY,
		(int)m_tInfo.fCX, (int)m_tInfo.fCY,
		hMemDC,
		m_tFrame.m_dwFrameStart * (int)m_tInfo.fCX,
		m_tFrame.m_dwFrameScene * (int)m_tInfo.fCY,
		(int)m_tInfo.fCX, (int)m_tInfo.fCY, REMOVE_PINK);
}

void CBossSkill::Release()
{
}

void CBossSkill::SceneChange()
{
	if (m_eCurState != m_ePreState)
	{
		switch (m_eCurState)
		{
		case eFIRE:
			m_strFrameKey = L"BossFire";
			m_tInfo.fY -= 5;
			m_tInfo.fCX = 134;
			m_tInfo.fCY = 99;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 8;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 100;
			break;
		case eICE://6���� 300x200
			m_strFrameKey = L"BossIce";
			m_tInfo.fY -= 20;
			m_tInfo.fCX = 147;
			m_tInfo.fCY = 139;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 13;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 40;
			break;

		case eSUN://5����  200 x210
			m_strFrameKey = L"BossSun";
			m_tInfo.fCX = 162;
			m_tInfo.fCY = 135;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 5;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 30;
			break;

		case eCURSE://6���� 250 300
			m_strFrameKey = L"BossCurse";
			m_tInfo.fCX = 80;
			m_tInfo.fCY = 90;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 2;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 70;//
			break;

		case eTHUNDER://8���� 256x256 
			m_tInfo.fY -= 170;
			m_strFrameKey = L"BossThunder";
			m_tInfo.fCX = 94;
			m_tInfo.fCY = 447;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 4;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 50;//800
			break;
		}
		m_ePreState = m_eCurState;

	}
}

