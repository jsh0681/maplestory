#pragma once
#include "Obj.h"
class CSkill :
	public CObj
{
public:
	enum STATE { eATTACK, eBYOEKRYEOK, eNOESEONG, eSEOMMYEOL, eTAEPUNG, eSEUNGCHUN ,eEND};//���� ���� ���� ��ǳ ��õ 
public:
	CSkill();
	virtual ~CSkill();

	// CObj��(��) ���� ��ӵ�
	virtual void Initialize() override;
	virtual void LateInit() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;
public:
	const float GetEnum() {return (float)m_eSkillNum;}
	const float GetSkillDamage() { return m_fSkillDamge; }
public:
	void SetEnum(int eSkillNum) { m_eSkillNum = (STATE)eSkillNum; }
	

	void SceneChange();
	void SetSkillDamage(float fSkillDamge) { m_fSkillDamge = fSkillDamge; };
private:
	float m_fSkillDamge;
	DWORD m_dwStart;

	STATE m_eSkillNum;
	STATE m_eCurState;//���� ����
	STATE m_ePreState;//���� ����
	wstring m_strFrameKey;
};

