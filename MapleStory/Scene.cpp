#include "stdafx.h"
#include "Scene.h"


CScene::CScene()
{
}


CScene::~CScene()
{
}


void CScene::MoveFrame()
{
	if (m_tFrame.m_dwFrameTime + m_tFrame.m_dwFrameSpd < GetTickCount())
	{
		m_tFrame.m_dwFrameStart++;
		m_tFrame.m_dwFrameTime = GetTickCount();
	}
	if (m_tFrame.m_dwFrameStart > m_tFrame.m_dwFrameEnd)
		m_tFrame.m_dwFrameStart = 0;
}
