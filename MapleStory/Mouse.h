#pragma once
#include "Obj.h"
class CMouse :
	public CObj
{
public:
	CMouse();
	virtual ~CMouse();
	enum STATE { IDLE, CLICK, PRESSING, end };


	// CObj을(를) 통해 상속됨
	virtual void Initialize() override;

	virtual void LateInit() override;
	virtual int Update() override;

	virtual void LateUpdate() override;

	virtual void Render(HDC hDC) override;

	virtual void Release() override;
	void MouseCheck();
	void StateChange();
public:
	static POINT GetMousePos();
	bool m_bMouseDown;
	bool m_bMouseUp;
	bool m_bMousePressing;
private:

	STATE m_eCurState;//현재 상태
	STATE m_ePreState;//이전 상태

	wstring m_strFrameKey;

};

