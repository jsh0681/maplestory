#pragma once
#include "Obj.h"

class CUI :
	public CObj
{
public:

	CUI();
	virtual ~CUI();
public:

	// CObj을(를) 통해 상속됨
	virtual void Initialize() override;
	virtual void LateInit() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;
public:
	void DrawUI(HDC hDC);
	void DrawLV(HDC hDC);
	void KeyCheck();
public:
	void SetPortalPos(float fX, float fY) { m_tPortalInfo.fX = fX, m_tPortalInfo.fY; }
	void SetDamage(float fDamage) { m_iDamage = fDamage; }
public:
	 int DrawHpBar();
	 int DrawMpBar();
	 int DrawExpBar();

private:
	float m_iDamage;
	bool m_bSkillWindowOpen;
	bool m_bStatWindowOpen;

	INFO m_tPortalInfo;
	INFO m_tStatWindowInfo;
	INFO m_tSkillWindowInfo;
	INFO m_tEquipWindowInfo; 

	CObj* m_pPlayer;
};

