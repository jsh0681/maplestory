#include "stdafx.h"
#include "KeyMgr.h"

IMPLEMENT_SINGLETON(CKeyMgr)

CKeyMgr::CKeyMgr()
{
	ZeroMemory(m_bKeyState, sizeof(m_bKeyState));
}


CKeyMgr::~CKeyMgr()
{
}

bool CKeyMgr::KeyPressing(DWORD dwKey)
{
	if (GetAsyncKeyState(dwKey) & 0x8000)
		return true;

	return false;
}

bool CKeyMgr::KeyDown(DWORD dwKey)
{
	if (!m_bKeyState[dwKey] && GetAsyncKeyState(dwKey) & 0x8000)
	{
		m_bKeyState[dwKey] = !m_bKeyState[dwKey];
		return true;
	}

	return false;
}

bool CKeyMgr::KeyUp(DWORD dwKey)
{
	if (m_bKeyState[dwKey] && !(GetAsyncKeyState(dwKey) & 0x8000))
	{
		m_bKeyState[dwKey] = !m_bKeyState[dwKey];
		return true;
	}

	return false;
}

void CKeyMgr::UpdateKey()
{
	for (int i = 0; i < MAX_KEY; ++i)
	{
		if (m_bKeyState[i] && !(GetAsyncKeyState(i) & 0x8000))
			m_bKeyState[i] = !m_bKeyState[i];
		if (!m_bKeyState[i] && GetAsyncKeyState(i) & 0x8000)
			m_bKeyState[i] = !m_bKeyState[i];
	}
}