#pragma once
class CObj;
class CTile;

typedef list<CObj*>				OBJLIST;
typedef list<CObj*>::iterator	OBJITER;
typedef vector<CObj*>			OBJVEC;

typedef	vector<CTile*>      TILEVEC;