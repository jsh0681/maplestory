#include "stdafx.h"
#include "Obj.h"


CObj::CObj() : m_bIsInit(false), m_bIsDead(false), m_fSpeed(0.f)
, m_eLayer(CObjMgr::LAYER_END)
{
	ZeroMemory(&m_tInfo, sizeof(INFO));
	ZeroMemory(&m_tRect, sizeof(RECT));
}


CObj::~CObj()
{
}

void CObj::LateInit()
{
	if (!m_bIsInit)
	{
		this->LateInit();
		m_bIsInit = true;
	}
}
void CObj::UpdateRect()
{
	m_tRect.left = LONG(m_tInfo.fX - m_tInfo.fCX * 0.5f);
	m_tRect.top = LONG(m_tInfo.fY - m_tInfo.fCY * 0.5f);
	m_tRect.right = LONG(m_tInfo.fX + m_tInfo.fCX * 0.5f);
	m_tRect.bottom = LONG(m_tInfo.fY + m_tInfo.fCY * 0.5f);
}

void CObj::UpdateCollisionRect(float Lx, float Ty,float Rx,float By)
{
	m_tCollisionRect.left = LONG(m_tInfo.fX - (m_tInfo.fCX- Lx) * 0.5f);
	m_tCollisionRect.top = LONG(m_tInfo.fY - (m_tInfo.fCY - Ty) * 0.5f);
	m_tCollisionRect.right = LONG(m_tInfo.fX + (m_tInfo.fCX - Rx) * 0.5f);
	m_tCollisionRect.bottom = LONG(m_tInfo.fY + (m_tInfo.fCY - By) * 0.5f);
}

void CObj::UpdateCollisionRect(float x, float y)
{
	m_tCollisionRect.left = LONG(m_tInfo.fX - (m_tInfo.fCX - x) * 0.5f);
	m_tCollisionRect.top = LONG(m_tInfo.fY - (m_tInfo.fCY - y) * 0.5f);
	m_tCollisionRect.right = LONG(m_tInfo.fX + (m_tInfo.fCX - x) * 0.5f);
	m_tCollisionRect.bottom = LONG(m_tInfo.fY + (m_tInfo.fCY - y) * 0.5f);
}


void CObj::MoveFrame()
{
	if (m_tFrame.m_dwFrameTime + m_tFrame.m_dwFrameSpd < GetTickCount())
	{
		m_tFrame.m_dwFrameStart++;
		m_tFrame.m_dwFrameTime = GetTickCount();
	}
	if (m_tFrame.m_dwFrameStart > m_tFrame.m_dwFrameEnd)
		m_tFrame.m_dwFrameStart = 0;
}

void CObj::EffectFrame()
{
	if (m_tFrame.m_dwFrameTime + m_tFrame.m_dwFrameSpd < GetTickCount())
	{
		m_tFrame.m_dwFrameStart++;
		m_tFrame.m_dwFrameTime = GetTickCount();
	}
	if (m_tFrame.m_dwFrameStart > m_tFrame.m_dwFrameEnd)
	{
		m_bIsDead = true;
	}
}