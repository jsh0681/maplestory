#include "stdafx.h"
#include "Line.h"


CLine::CLine()
{
}

CLine::CLine(const LINEINFO & info):m_tInfo(info)
{
}


CLine::~CLine()
{
}

void CLine::Render(HDC hDC)
{
	float fScrollX = CScrollMgr::GetScrollX();

	MoveToEx(hDC, (int)m_tInfo.tLeft.fX- (int)fScrollX, (int)m_tInfo.tLeft.fY, nullptr);
	LineTo(hDC, (int)m_tInfo.tRight.fX- (int)fScrollX, (int)m_tInfo.tRight.fY);
}
