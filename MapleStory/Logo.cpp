#include "stdafx.h"
#include "Logo.h"


CLogo::CLogo()
{
}


CLogo::~CLogo()
{
	Release();
}

void CLogo::Initialize()
{
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Logo/Logo.bmp", L"Logo");

	CSoundMgr::GetInstance()->PlayBGM(L"Bgm_Logo.mp3");

	m_tInfo = { 0,0,800,600};
	m_tFrame.m_dwFrameStart = 0;//비트맵의 그림 시작번호
	m_tFrame.m_dwFrameEnd = 6;//비트맵에서 그림이 있는 마지막 번호
	m_tFrame.m_dwFrameScene = 0;//줄수를 의미함
	m_tFrame.m_dwFrameTime = GetTickCount();
	m_tFrame.m_dwFrameSpd = 200;


	m_strFrameKey = L"Logo";


}

void CLogo::Update()
{
	if (CKeyMgr::GetInstance()->KeyDown(VK_RETURN))
	{

		CSceneMgr::GetInstance()->SceneChange(CSceneMgr::TITLE);
		return;
	}
}

void CLogo::LateUpdate()
{
	MoveFrame();
}

void CLogo::Render(HDC hDC)
{
	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(m_strFrameKey.c_str());
	NULL_CHECK(hMemDC);

	//GdiTransparentBlt : 사용자가 원하는 색상을 제거하여 출력.
	GdiTransparentBlt(hDC, 0, 0, (int)m_tInfo.fCX,(int)m_tInfo.fCY,
		hMemDC,m_tFrame.m_dwFrameStart * (int)m_tInfo.fCX,
		m_tFrame.m_dwFrameScene * (int)m_tInfo.fCY,
		(int)m_tInfo.fCX, (int)m_tInfo.fCY, RGB(232, 323, 32));
}

void CLogo::Release()
{
}
void CLogo::MoveFrame()
{
	if (m_tFrame.m_dwFrameTime + m_tFrame.m_dwFrameSpd < GetTickCount())
	{
		m_tFrame.m_dwFrameStart++;
		m_tFrame.m_dwFrameTime = GetTickCount();
	}
	if (m_tFrame.m_dwFrameStart > m_tFrame.m_dwFrameEnd)
		m_tFrame.m_dwFrameStart = m_tFrame.m_dwFrameEnd;
}
