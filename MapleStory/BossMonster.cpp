#include "stdafx.h"
#include "BossMonster.h"
#include"BossSkill.h"
#include "Player.h"


CBossMonster::CBossMonster() :m_bDirChange(false), m_bIsNear(false), m_bRigidity(false), m_pPlayer(nullptr)
{
	ZeroMemory(&m_tBossInfo, sizeof(OBJINFO));
	ZeroMemory(&m_tBossLHandInfo, sizeof(OBJINFO));
	ZeroMemory(&m_tBossRHandInfo, sizeof(OBJINFO));
}


CBossMonster::~CBossMonster()
{
}

void CBossMonster::Initialize()
{
	m_eLayer = CObjMgr::LAYER_BOSS;

	m_tInfo = { 800,237,BOSS_BODYX,BOSS_BODYY };
	m_tLHandInfo = { 563,297,BOSS_LHANDX, BOSS_LHANDY };
	m_tRHandInfo = { 980,357,BOSS_RHANDX ,BOSS_RHANDY };

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Character/Monster/Boss/BossBody.bmp", L"BossBody");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Character/Monster/Boss/BossLeftHand2.bmp", L"BossLeftHand");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Character/Monster/Boss/BossRightHand2.bmp", L"BossRightHand");

	m_tBossInfo = { "���� �߷�","����",100,(float)1000000 ,(float)1000000  ,0,0,30,100000,100000,250000 };

	m_fSpeed = 5.f;

	m_strFrameBody = L"BossBody";
	m_eCurBody = eBFIRE;

	m_strFrameLHand = L"BossLeftHand";
	m_eCurLHand = eLIDLE;

	m_strFrameRHand = L"BossRightHand";
	m_eCurRHand = eRIDLE;

}

void CBossMonster::LateInit()
{
	m_pPlayer = CObjMgr::GetInstance()->GetPlayer();
	m_dwStart = GetTickCount();
	m_dwRigidity = GetTickCount();
}

int CBossMonster::Update()
{
	CObj::LateInit();
	if (m_bIsDead)
	{
		return DEAD_OBJ;
	}
	return NO_EVENT;
}


void CBossMonster::LateUpdate()
{
	m_pPlayer = CObjMgr::GetInstance()->GetPlayer();
	BodyChange();
	LHandChange();
	RHandChange();
	///////////////////////////////////
	MoveBodyFrame();
	MoveLHandFrame();
	MoveRHandFrame();
	////////////////////////////////////

	if (m_bRigidity)
	{
		if (m_dwRigidity + 300 < GetTickCount())
		{
			m_bRigidity = false;
			m_dwRigidity = GetTickCount();
		}
	}


}

void CBossMonster::Render(HDC hDC)
{
	m_tRect.left = LONG(m_tInfo.fX - m_tInfo.fCX * 0.5f);
	m_tRect.top = LONG(m_tInfo.fY - m_tInfo.fCY * 0.5f);
	m_tRect.right = LONG(m_tInfo.fX + m_tInfo.fCX * 0.5f);
	m_tRect.bottom = LONG(m_tInfo.fY + m_tInfo.fCY * 0.5f);

	DrawBossBody(hDC);
	DrawBossLHand(hDC);
	DrawBossRHand(hDC); 
	DrawHpBar(hDC);
	
}

void CBossMonster::Release()
{
}

void CBossMonster::DrawHpBar(HDC hDC)
{
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();
	Brush = CreateSolidBrush(RGB(69, 69, 69));

	oBrush = (HBRUSH)SelectObject(hDC, Brush);

	Rectangle(hDC, m_tRect.left - (int)fScrollX, m_tRect.top + 130 - (int)fScrollY, m_tRect.right - (int)fScrollX, m_tRect.top + 150 - (int)fScrollY);

	SelectObject(hDC, oBrush);
	DeleteObject(Brush);


	float fMinusDamage = (m_tBossInfo.fMaxHp - m_tBossInfo.fHp) / m_tBossInfo.fMaxHp;
	Brush = CreateSolidBrush(RGB(255, 0, 0));
	oBrush = (HBRUSH)SelectObject(hDC, Brush);

	Rectangle(hDC, m_tRect.left - (int)fScrollX, m_tRect.top + 130 - (int)fScrollY, (m_tRect.right - (m_tRect.right - m_tRect.left)*fMinusDamage) - (int)fScrollX, m_tRect.top + 150 - (int)fScrollY);

}

void CBossMonster::MoveBodyFrame()
{
	if (m_tBodyFrame.m_dwFrameTime + m_tBodyFrame.m_dwFrameSpd < GetTickCount())
	{
		m_tBodyFrame.m_dwFrameStart++;
		m_tBodyFrame.m_dwFrameTime = GetTickCount();
	}
	if (m_tBodyFrame.m_dwFrameStart > m_tBodyFrame.m_dwFrameEnd)
	{
		m_tLHandFrame.m_dwFrameStart = 0;
		RandMove();
	}
}

void CBossMonster::MoveLHandFrame()
{
	if (m_tLHandFrame.m_dwFrameTime + m_tLHandFrame.m_dwFrameSpd < GetTickCount())
	{
		m_tLHandFrame.m_dwFrameStart++;
		m_tLHandFrame.m_dwFrameTime = GetTickCount();
	}
	if (m_tLHandFrame.m_dwFrameStart > m_tLHandFrame.m_dwFrameEnd)
		m_tLHandFrame.m_dwFrameStart = 0;

	if (m_eCurLHand == eLICE && m_tLHandFrame.m_dwFrameStart == 18)
	{
		CSoundMgr::GetInstance()->PlaySoundw(L"Ice2.mp3", CSoundMgr::MONSTER);
		CObjMgr::GetInstance()->AddObject(CreateSkill<CBossSkill>(m_eCurLHand+3), CObjMgr::BOSSSKILL);
	}
	if (m_eCurLHand == eLSUN && m_tLHandFrame.m_dwFrameStart == 16)
	{
		CSoundMgr::GetInstance()->PlaySoundw(L"Sun2.mp3", CSoundMgr::MONSTER);
		CObjMgr::GetInstance()->AddObject(CreateSkill<CBossSkill>(m_eCurLHand+3), CObjMgr::BOSSSKILL);
	}

}

void CBossMonster::MoveRHandFrame()
{
	if (m_tRHandFrame.m_dwFrameTime + m_tRHandFrame.m_dwFrameSpd < GetTickCount())
	{
		m_tRHandFrame.m_dwFrameStart++;
		m_tRHandFrame.m_dwFrameTime = GetTickCount();
	}
	if (m_tRHandFrame.m_dwFrameStart > m_tRHandFrame.m_dwFrameEnd)
		m_tRHandFrame.m_dwFrameStart = 0;
	if (m_eCurRHand == eRTHUNDER && m_tRHandFrame.m_dwFrameStart == 13)
	{
		CSoundMgr::GetInstance()->PlaySoundw(L"Thunder.mp3", CSoundMgr::MONSTER);
		CObjMgr::GetInstance()->AddObject(CreateSkill<CBossSkill>(m_eCurRHand), CObjMgr::BOSSSKILL);
	}	
	if (m_eCurRHand == eRCURSE && m_tRHandFrame.m_dwFrameStart == 11)
	{
		CSoundMgr::GetInstance()->PlaySoundw(L"Curse2.mp3", CSoundMgr::MONSTER);
		CObjMgr::GetInstance()->AddObject(CreateSkill<CBossSkill>(m_eCurRHand), CObjMgr::BOSSSKILL);
	}	
	if (m_eCurRHand == eRFIRE && m_tRHandFrame.m_dwFrameStart == 14)
	{
		CSoundMgr::GetInstance()->PlaySoundw(L"Fire2.mp3", CSoundMgr::MONSTER);
		CObjMgr::GetInstance()->AddObject(CreateSkill<CBossSkill>(m_eCurRHand), CObjMgr::BOSSSKILL);
	}
	

}


void CBossMonster::UpdateBodyRect()
{
	m_tRect.left = LONG(m_tInfo.fX - m_tInfo.fCX * 0.5f);
	m_tRect.top = LONG(m_tInfo.fY - m_tInfo.fCY * 0.5f);
	m_tRect.right = LONG(m_tInfo.fX + m_tInfo.fCX * 0.5f);
	m_tRect.bottom = LONG(m_tInfo.fY + m_tInfo.fCY * 0.5f);
}

void CBossMonster::UpdateLHandRect()
{
	m_tLHandRect.left = LONG(m_tLHandInfo.fX - m_tLHandInfo.fCX * 0.5f);
	m_tLHandRect.top = LONG(m_tLHandInfo.fY - m_tLHandInfo.fCY * 0.5f);
	m_tLHandRect.right = LONG(m_tLHandInfo.fX + m_tLHandInfo.fCX * 0.5f);
	m_tLHandRect.bottom = LONG(m_tLHandInfo.fY + m_tLHandInfo.fCY * 0.5f);
}
void CBossMonster::UpdateRHandRect()
{
	m_tRHandRect.left = LONG(m_tRHandInfo.fX - m_tRHandInfo.fCX * 0.5f);
	m_tRHandRect.top = LONG(m_tRHandInfo.fY - m_tRHandInfo.fCY * 0.5f);
	m_tRHandRect.right = LONG(m_tRHandInfo.fX + m_tRHandInfo.fCX * 0.5f);
	m_tRHandRect.bottom = LONG(m_tRHandInfo.fY + m_tRHandInfo.fCY * 0.5f);
}
void CBossMonster::RandMove()
{
	int iRand = (rand() % 5);

	if (iRand == eBFIRE)
	{
		m_eCurBody = eBFIRE;
		m_eCurLHand = eLIDLE;
		m_eCurRHand = eRFIRE;
	}
	else if(iRand == eBSUN)
	{
		m_eCurBody = eBSUN;
		m_eCurLHand = eLSUN;
		m_eCurRHand = eRIDLE;
	}
	else if (iRand == eBICE)
	{
		m_eCurBody = eBICE;
		m_eCurLHand = eLICE;
		m_eCurRHand = eRIDLE;
	}
	else if (iRand == eBTHUNDER)
	{
		m_eCurBody = eBTHUNDER;
		m_eCurLHand = eLIDLE;
		m_eCurRHand = eRTHUNDER;
	}
	else if (iRand == eBCURSE)
	{
		m_eCurBody = eBCURSE;
		m_eCurLHand = eLIDLE;
		m_eCurRHand = eRCURSE;
	}
	m_bDirChange = false;
}





void CBossMonster::DrawBossBody(HDC hDC)
{
	UpdateBodyRect();
	UpdateCollisionRect(400, 300);
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();

	//Rectangle(hDC, m_tCollisionRect.left - fScrollX, m_tCollisionRect.top - fScrollY, m_tCollisionRect.right - fScrollX, m_tCollisionRect.bottom - fScrollY);

	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(m_strFrameBody.c_str());
	NULL_CHECK(hMemDC);
	GdiTransparentBlt(hDC, m_tRect.left - (int)fScrollX, m_tRect.top - (int)fScrollY,
		BOSS_BODYX, BOSS_BODYY,
		hMemDC,
		m_tBodyFrame.m_dwFrameStart * (int)m_tInfo.fCX,
		m_tBodyFrame.m_dwFrameScene * (int)m_tInfo.fCY,
		BOSS_BODYX, BOSS_BODYY, REMOVE_BLUE);
}
void CBossMonster::DrawBossLHand(HDC hDC)
{
	UpdateLHandRect();
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();


	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(m_strFrameLHand.c_str());
	NULL_CHECK(hMemDC);
	GdiTransparentBlt(hDC, m_tLHandRect.left - (int)fScrollX, m_tLHandRect.top - (int)fScrollY,
		BOSS_LHANDX, BOSS_LHANDY,
		hMemDC,
		m_tLHandFrame.m_dwFrameStart * (int)m_tLHandInfo.fCX,
		m_tLHandFrame.m_dwFrameScene * (int)m_tLHandInfo.fCY,
		BOSS_LHANDX, BOSS_LHANDY, REMOVE_BLUE);
}
void CBossMonster::DrawBossRHand(HDC hDC)
{
	UpdateRHandRect();
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();


	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(m_strFrameRHand.c_str());
	NULL_CHECK(hMemDC);
	GdiTransparentBlt(hDC, m_tRHandRect.left - (int)fScrollX, m_tRHandRect.top - (int)fScrollY,
		BOSS_RHANDX, BOSS_RHANDY,
		hMemDC,
		m_tRHandFrame.m_dwFrameStart * (int)m_tRHandInfo.fCX,
		m_tRHandFrame.m_dwFrameScene * (int)m_tRHandInfo.fCY,
		BOSS_RHANDX, BOSS_RHANDY, REMOVE_BLUE);
}




void CBossMonster::BodyChange()
{
	if (m_eCurBody != m_ePreBody)
	{
		switch (m_eCurBody)
		{
		case eBFIRE://26*200
			m_tBodyFrame.m_dwFrameScene = 0;
			m_tBodyFrame.m_dwFrameStart = 0;
			m_tBodyFrame.m_dwFrameEnd = 25;
			m_tBodyFrame.m_dwFrameTime = GetTickCount();
			m_tBodyFrame.m_dwFrameSpd = 200;
			break;

		case eBSUN://22*200
			m_tBodyFrame.m_dwFrameScene = 1;
			m_tBodyFrame.m_dwFrameStart = 0;
			m_tBodyFrame.m_dwFrameEnd = 20;
			m_tBodyFrame.m_dwFrameTime = GetTickCount();
			m_tBodyFrame.m_dwFrameSpd = 200;
			break;

		case eBICE: //24*200
			m_tBodyFrame.m_dwFrameScene = 2;
			m_tBodyFrame.m_dwFrameStart = 0;
			m_tBodyFrame.m_dwFrameEnd = 22;
			m_tBodyFrame.m_dwFrameTime = GetTickCount();
			m_tBodyFrame.m_dwFrameSpd = 200;
			break;

		case eBTHUNDER://23*200
			m_tBodyFrame.m_dwFrameScene = 3;
			m_tBodyFrame.m_dwFrameStart = 0;
			m_tBodyFrame.m_dwFrameEnd = 21;
			m_tBodyFrame.m_dwFrameTime = GetTickCount();
			m_tBodyFrame.m_dwFrameSpd = 200;
			break;

		case eBCURSE://17*200
			m_tBodyFrame.m_dwFrameScene = 4;
			m_tBodyFrame.m_dwFrameStart = 0;
			m_tBodyFrame.m_dwFrameEnd = 15;
			m_tBodyFrame.m_dwFrameTime = GetTickCount();
			m_tBodyFrame.m_dwFrameSpd = 200;
			break;
		}
		m_ePreBody = m_eCurBody;
	}
}



void CBossMonster::RHandChange()
{
	if (m_eCurRHand != m_ePreRHand)
	{
		switch (m_eCurRHand)
		{
		case eRTHUNDER://19*
			m_tRHandFrame.m_dwFrameScene = 0;
			m_tRHandFrame.m_dwFrameStart = 0;
			m_tRHandFrame.m_dwFrameEnd = 18;
			m_tRHandFrame.m_dwFrameTime = GetTickCount();
			m_tRHandFrame.m_dwFrameSpd = 240;
			break;

		case eRFIRE:
			m_tRHandFrame.m_dwFrameScene = 1;
			m_tRHandFrame.m_dwFrameStart = 0;
			m_tRHandFrame.m_dwFrameEnd = 20;
			m_tRHandFrame.m_dwFrameTime = GetTickCount();
			m_tRHandFrame.m_dwFrameSpd = 245;

			break;

		case eRCURSE:
			m_tRHandFrame.m_dwFrameScene = 2;
			m_tRHandFrame.m_dwFrameStart = 0;
			m_tRHandFrame.m_dwFrameEnd = 23;
			m_tRHandFrame.m_dwFrameTime = GetTickCount();
			m_tRHandFrame.m_dwFrameSpd = 140;
			break;

		case eRIDLE:
			m_tRHandFrame.m_dwFrameScene = 3;
			m_tRHandFrame.m_dwFrameStart = 0;
			m_tRHandFrame.m_dwFrameEnd = 23;
			m_tRHandFrame.m_dwFrameTime = GetTickCount();
			m_tRHandFrame.m_dwFrameSpd = 200;
			break;
		}
		m_ePreRHand = m_eCurRHand;
	}
}


void CBossMonster::LHandChange()
{
	if (m_eCurLHand != m_ePreLHand)
	{
		switch (m_eCurLHand)
		{
		case eLICE:
			m_tLHandFrame.m_dwFrameScene = 0;
			m_tLHandFrame.m_dwFrameStart = 0;
			m_tLHandFrame.m_dwFrameEnd = 28;
			m_tLHandFrame.m_dwFrameTime = GetTickCount();
			m_tLHandFrame.m_dwFrameSpd = 165;
			break;

		case eLSUN:
			m_tLHandFrame.m_dwFrameScene = 1;
			m_tLHandFrame.m_dwFrameStart = 0;
			m_tLHandFrame.m_dwFrameEnd = 36;
			m_tLHandFrame.m_dwFrameTime = GetTickCount();
			m_tLHandFrame.m_dwFrameSpd = 116;
			break;

		case eLIDLE:
			m_tLHandFrame.m_dwFrameScene = 2;
			m_tLHandFrame.m_dwFrameStart = 0;
			m_tLHandFrame.m_dwFrameEnd = 25;
			m_tLHandFrame.m_dwFrameTime = GetTickCount();
			m_tLHandFrame.m_dwFrameSpd = 200;
			break;
		}
		m_ePreLHand = m_eCurLHand;
	}
}
