#include "stdafx.h"
#include "MyButton.h"
#include "Mouse.h"


CMyButton::CMyButton()
{
}


CMyButton::~CMyButton()
{
	Release();
}

void CMyButton::Initialize()
{
	m_tInfo.fCX = 150.f;
	m_tInfo.fCY = 150.f;
	m_iDrawID = 0;

	m_eLayer = CObjMgr::LAYER_UI;
}

void CMyButton::LateInit()
{
}

int CMyButton::Update()
{
	CObj::LateInit();
	return NO_EVENT;
}

void CMyButton::LateUpdate()
{
	CObj::UpdateRect();

	POINT pt = CMouse::GetMousePos();
	if (PtInRect(&m_tRect, pt))

	{
		if (CKeyMgr::GetInstance()->KeyDown(VK_LBUTTON))
		{
			if (L"Start" == m_wstrFrameKey)
			{
				CSceneMgr::GetInstance()->SceneChange(CSceneMgr::LOGO);
				return;
			}
			else if (L"Edit" == m_wstrFrameKey)
			{
				CSceneMgr::GetInstance()->SceneChange(CSceneMgr::EDIT);
				return;
			}
			else if (L"Exit" == m_wstrFrameKey)
			{
				DestroyWindow(g_hWnd);
				return;
			}
		}

		m_iDrawID = 1;
	}
	else
		m_iDrawID = 0;
	
}

void CMyButton::Render(HDC hDC)
{
	CObj::UpdateRect();

	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(m_wstrFrameKey.c_str());
	NULL_CHECK(hMemDC);

	GdiTransparentBlt(hDC, m_tRect.left, m_tRect.top, (int)m_tInfo.fCX, (int)m_tInfo.fCY,
		hMemDC, m_iDrawID * (int)m_tInfo.fCX, 0, (int)m_tInfo.fCX, (int)m_tInfo.fCY, RGB(255, 255, 255));
}

void CMyButton::Release()
{
}

bool CMyButton::PtInSphere(const POINT & pt)
{
	float w = pt.x - m_tInfo.fX;
	float h = pt.y - m_tInfo.fY;
	float d = sqrtf(w * w + h * h);

	return d <= m_tInfo.fCX * 0.5f;
}
