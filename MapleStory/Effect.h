#pragma once
#include "Obj.h"

class CEffect :
	public CObj
{
public:
	enum STATE { eATTACK, eBYOEKRYEOK, eNOESEONG, eSEOMMYEOL, eTAEPUNG, eSEUNGCHUN, eEND };//���� ���� ���� ��ǳ ��õ 
public:
	CEffect();
	virtual ~CEffect();

	// CObj��(��) ���� ��ӵ�
	virtual void Initialize() override;
	virtual void LateInit() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	void DrawDamage(HDC hDC);
	virtual void Release() override;
public:
	void SetEnum(int eSkillNum) { m_eSkillNum = (STATE)eSkillNum; }
	void SetDamage(int iDamage) { m_iDamage = iDamage + (rand() % (iDamage / 2)); }

	void SceneChange();
	int m_iDamage;
private:
	CObj* m_pPlayer;
	DWORD m_dwStart;

	STATE m_eSkillNum;
	STATE m_eCurState;//���� ����
	STATE m_ePreState;//���� ����
	wstring m_strFrameKey;
};

