#include "stdafx.h"
#include "UI.h"
#include"Player.h"
#include"Shop.h"

bool g_bEquipWindowOpen = false;
bool g_bStatWindowOpen = false;
CUI::CUI() :m_iDamage(0) ,  m_bSkillWindowOpen(false)
{
	ZeroMemory(&m_tPortalInfo, sizeof(INFO));
	ZeroMemory(&m_tSkillWindowInfo, sizeof(INFO));
	ZeroMemory(&m_tEquipWindowInfo, sizeof(INFO));
}


CUI::~CUI()
{
}

void CUI::Initialize()
{	
	m_tInfo = { 0,0,70,202 };
	m_eLayer = CObjMgr::LAYER_UI;
	m_tCollisionInfo = { 0,0,70,202 };
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Ui/Info_0.bmp", L"MenuBar");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Ui/HpBar.bmp", L"HpBar");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Ui/MpBar.bmp", L"MpBar");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Ui/Exp.bmp", L"Exp");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Ui/Level.bmp", L"Level");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Ui/Equip.bmp", L"Equip");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Ui/Stat2.bmp", L"Stat");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Ui/Skill.bmp", L"Skill");

	m_tFrame.m_dwFrameStart = 0;//비트맵의 그림 시작번호
	m_tFrame.m_dwFrameEnd = 7;//비트맵에서 그림이 있는 마지막 번호
	m_tFrame.m_dwFrameScene = 0;//줄수를 의미함
	m_tFrame.m_dwFrameTime = GetTickCount();
	m_tFrame.m_dwFrameSpd = 200;


	m_tStatWindowInfo = { 100,100,212,318 };
	m_tEquipWindowInfo = { 547,30,184,290 };
	m_tSkillWindowInfo = { 100,100,174,300 };
}

void CUI::LateInit()
{
}

int CUI::Update()
{
	CObj::LateInit();
	CObj::MoveFrame();
	return 0;
}

void CUI::LateUpdate()
{
	m_pPlayer = CObjMgr::GetInstance()->GetPlayer();
	KeyCheck();
	
}
int CUI::DrawHpBar()
{
	float fMaxHp = 105.f;
	float fCurMaxHp = (dynamic_cast<CPlayer*>(m_pPlayer)->GetMaxHp());
	float fCurHp = (dynamic_cast<CPlayer*>(m_pPlayer)->GetHp());
	float fMinusDamage = (fCurMaxHp - fCurHp) / fCurMaxHp;

	return (int)(fMaxHp-(fMaxHp*fMinusDamage));
}
int CUI::DrawMpBar()
{
	float fMaxMp = 105.f;
	float fCurfMaxMp = (dynamic_cast<CPlayer*>(m_pPlayer)->GetMaxMp());
	float fCurMp = (dynamic_cast<CPlayer*>(m_pPlayer)->GetMp());
	float fMinusMp = (fCurfMaxMp - fCurMp) / fCurfMaxMp;
	return (int)(fMaxMp - (fMaxMp*fMinusMp));
}

int CUI::DrawExpBar()
{
	float fMaxExp = 234.f;
	float fCurfMaxExp = (dynamic_cast<CPlayer*>(m_pPlayer)->GetMaxExp());
	float fCurExp = (dynamic_cast<CPlayer*>(m_pPlayer)->GetExp());
	float fExp = (fCurfMaxExp - fCurExp) / fCurfMaxExp;
	return (int)(fMaxExp - (fMaxExp*fExp));
}
void CUI::Render(HDC hDC)
{
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();

	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"MenuBar");
	NULL_CHECK(hMemDC);
	BitBlt(hDC, 0, 544, WINCX, WINCY, hMemDC, 0, 0, SRCCOPY);

	hMemDC = CBmpMgr::GetInstance()->FindImage(L"HpBar");
	NULL_CHECK(hMemDC);
	BitBlt(hDC, 199, 560, DrawHpBar(), WINCY, hMemDC, 0, 0, SRCCOPY);

	hMemDC = CBmpMgr::GetInstance()->FindImage(L"MpBar");
	NULL_CHECK(hMemDC);
	BitBlt(hDC, 327, 560, DrawMpBar(), WINCY, hMemDC, 0, 0, SRCCOPY);

	hMemDC = CBmpMgr::GetInstance()->FindImage(L"Exp");
	NULL_CHECK(hMemDC);
	BitBlt(hDC, 199, 574, DrawExpBar(), WINCY, hMemDC, 0, 0, SRCCOPY);
	

	hMemDC = CBmpMgr::GetInstance()->FindImage(L"Exp");
	NULL_CHECK(hMemDC);
	BitBlt(hDC, 199, 574, DrawExpBar(), WINCY, hMemDC, 0, 0, SRCCOPY);

	TCHAR szBuf[64] = L"";
	RECT rc = { 80, 555, 80, 555 };
	swprintf_s(szBuf, L"정성호");
	DrawText(hDC, szBuf, lstrlen(szBuf), &rc, DT_NOCLIP);
	rc = { 64, 573, 90, 573 };
	swprintf_s(szBuf, L"스트라이커");
	DrawText(hDC, szBuf, lstrlen(szBuf), &rc, DT_NOCLIP);

	DrawLV(hDC);
	DrawUI(hDC);

}

void CUI::DrawUI(HDC hDC)
{
	if (g_bStatWindowOpen)
	{
		HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Stat");
		NULL_CHECK(hMemDC);
		GdiTransparentBlt(hDC, (int)m_tStatWindowInfo.fX, (int)m_tStatWindowInfo.fY, (int)m_tStatWindowInfo.fCX, (int)m_tStatWindowInfo.fCY, hMemDC, 0, 0, (int)m_tStatWindowInfo.fCX, (int)m_tStatWindowInfo.fCY, REMOVE_PINK);
	}
	if (g_bEquipWindowOpen)
	{
		HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Equip");
		NULL_CHECK(hMemDC);
		GdiTransparentBlt(hDC, (int)m_tEquipWindowInfo.fX, (int)m_tEquipWindowInfo.fY, (int)m_tEquipWindowInfo.fCX, (int)m_tEquipWindowInfo.fCY, hMemDC, 0, 0, (int)m_tEquipWindowInfo.fCX, (int)m_tEquipWindowInfo.fCY, REMOVE_PINK);
	}
	if (m_bSkillWindowOpen)
	{
		HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Skill");
		NULL_CHECK(hMemDC);
		GdiTransparentBlt(hDC, (int)m_tSkillWindowInfo.fX, (int)m_tSkillWindowInfo.fY, (int)m_tSkillWindowInfo.fCX, (int)m_tSkillWindowInfo.fCY, hMemDC, 0, 0, (int)m_tSkillWindowInfo.fCX, (int)m_tSkillWindowInfo.fCY, REMOVE_PINK);
	}
}

void CUI::KeyCheck()
{
	

	if (!g_bStatWindowOpen)
	{
		if (CKeyMgr::GetInstance()->KeyUp('T'))//스텟창
		{
			CSoundMgr::GetInstance()->PlaySoundw(L"Window_Open.mp3",CSoundMgr::UI);
			g_bStatWindowOpen = true;
		}
	}
	else
	{
		if (CKeyMgr::GetInstance()->KeyUp('T'))//스텟창
		{
			CSoundMgr::GetInstance()->PlaySoundw(L"Window_Close.mp3", CSoundMgr::UI);
			g_bStatWindowOpen = false;
		}
	}
	
	if (!g_bEquipWindowOpen)
	{

		if (CKeyMgr::GetInstance()->KeyUp('E'))//장비창
		{
			CSoundMgr::GetInstance()->PlaySoundw(L"Window_Open.mp3", CSoundMgr::UI);
			g_bEquipWindowOpen = true;
		}
	}
	else
	{

		if (CKeyMgr::GetInstance()->KeyUp('E'))//장비창
		{
			CSoundMgr::GetInstance()->PlaySoundw(L"Window_Close.mp3", CSoundMgr::UI);
			g_bEquipWindowOpen = false;
		}
	}
	
	if (!m_bSkillWindowOpen)
	{
		if (CKeyMgr::GetInstance()->KeyUp('K'))//스킬창
		{
			CSoundMgr::GetInstance()->PlaySoundw(L"Window_Open.mp3", CSoundMgr::UI);
			m_bSkillWindowOpen = true;
		}
	}
	else
	{
		if (CKeyMgr::GetInstance()->KeyUp('K'))//스킬창
		{
			CSoundMgr::GetInstance()->PlaySoundw(L"Window_Close.mp3", CSoundMgr::UI);
			m_bSkillWindowOpen = false;
		}
	}


	

}
void CUI::DrawLV(HDC hDC)
{
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();

	int iCurLv = dynamic_cast<CPlayer*>(m_pPlayer)->GetPlayerInfo().iLv;
	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Level");
	NULL_CHECK(hMemDC);
	BitBlt(hDC, 30, 566, 10, 12, hMemDC, 10 * (iCurLv / 10), 0, SRCCOPY);
	BitBlt(hDC, 40, 566, 10, 12, hMemDC, 10 * (iCurLv % 10), 0, SRCCOPY);

}
void CUI::Release()
{
	CBmpMgr::GetInstance()->DestroyInstance();
}


