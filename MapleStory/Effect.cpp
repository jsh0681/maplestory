#include "stdafx.h"
#include "Effect.h"
#include "Player.h"

CEffect::CEffect():m_eCurState(eEND), m_ePreState(eEND), m_dwStart(0), m_iDamage(0), m_pPlayer(nullptr)
{
}


CEffect::~CEffect()
{
	Release();
}

void CEffect::Initialize()
{
	m_tInfo = { 0,0,0,0 };
	m_eLayer = CObjMgr::LAYER_EFFECT;

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Effect/ByeokRyeokEffect.bmp", L"ByeokRyeokEffect");//����

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Effect/NoeSeongEffect.bmp", L"NoeSeongEffect");//����

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Effect/SeungChunEffect.bmp", L"SeungChunEffect");//��õ

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Effect/TaePungEffect.bmp", L"TaePungEffect");//��ǳ

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Effect/SeomMyeolEffect.bmp", L"SeomMyeolEffect");//����

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Effect/DamageEffect.bmp", L"DamageEffect");//����

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Effect/HitEffect.bmp", L"HitEffect");//����

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Effect/CriticalDamageEffect.bmp", L"CriticalDamageEffect");//����


	m_eCurState = eEND;
	m_ePreState = m_eCurState;
}

void CEffect::LateInit()
{
	m_dwStart = GetTickCount();
}

int CEffect::Update()
{
	CObj::LateInit();

	if (m_bIsDead)
		return DEAD_OBJ;

	return NO_EVENT;
}

void CEffect::LateUpdate()
{
	m_pPlayer = CObjMgr::GetInstance()->GetPlayer();
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();
	m_eCurState = m_eSkillNum;
	SceneChange();
	CObj::EffectFrame();
}

void CEffect::Render(HDC hDC)
{
	CObj::UpdateRect();
	srand(unsigned(time(NULL)));
	
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();
	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"DamageEffect"); 
	NULL_CHECK(hMemDC); 
	if (m_iDamage / 10000 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 75, (int)(m_tInfo.fY - fScrollY) - 120, 149, 69, hMemDC, 149 * (((m_iDamage+ (rand() % (m_iDamage / 2))) % 100000) / 10000), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 1000 != 0)
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 50, (int)(m_tInfo.fY - fScrollY) - 120, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 10000) / 1000), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 100 != 0)									  
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 25, (int)(m_tInfo.fY - fScrollY) - 120, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 1000) / 100), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 10 != 0)									 
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX)     , (int)(m_tInfo.fY - fScrollY) - 120, 149, 69, hMemDC, 149 *	(((m_iDamage + (rand() % (m_iDamage / 2))) % 100) / 10), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage != 0)											  
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) +25,	(int)(m_tInfo.fY - fScrollY)- 120, 149, 69, hMemDC, 149 *  ((m_iDamage + (rand() % (m_iDamage / 2))) % 10 ), 0, 149, 69, REMOVE_PINK);
																
	if (m_iDamage / 10000 != 0)								
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 75, (int)(m_tInfo.fY - fScrollY)- 90, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 100000) / 10000), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 1000 != 0)									 
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 50, (int)(m_tInfo.fY - fScrollY) - 90, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 10000) / 1000), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 100 != 0)									
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) - 25, (int)(m_tInfo.fY - fScrollY) - 90, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 1000) / 100), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 10 != 0)									  
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX),      (int)(m_tInfo.fY - fScrollY) - 90, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 100) / 10), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage != 0)											  
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX) + 25, (int)(m_tInfo.fY - fScrollY) - 90, 149, 69, hMemDC, 149 * ((m_iDamage + (rand() % (m_iDamage / 2))) % 10), 0, 149, 69, REMOVE_PINK);
																  
	if (m_iDamage / 10000 != 0)									  
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX)- 75,  (int)(m_tInfo.fY - fScrollY) - 60, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 100000) / 10000), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 1000 != 0) 									  
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX)- 50,  (int)(m_tInfo.fY - fScrollY) - 60, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 10000) / 1000), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 100 != 0)  									
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX)- 25,  (int)(m_tInfo.fY - fScrollY) - 60, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 1000) / 100), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage / 10 != 0)   									  
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX),      (int)(m_tInfo.fY - fScrollY) - 60, 149, 69, hMemDC, 149 * (((m_iDamage + (rand() % (m_iDamage / 2))) % 100) / 10), 0, 149, 69, REMOVE_PINK);
	if (m_iDamage != 0)		  									
		GdiTransparentBlt(hDC, (int)(m_tInfo.fX - fScrollX)+ 25,  (int)(m_tInfo.fY - fScrollY) - 60, 149, 69, hMemDC, 149 * ((m_iDamage + (rand() % (m_iDamage / 2))) % 10), 0, 149, 69, REMOVE_PINK);


	hMemDC = CBmpMgr::GetInstance()->FindImage(m_strFrameKey.c_str());
	NULL_CHECK(hMemDC);
	GdiTransparentBlt(hDC, m_tRect.left - (int)fScrollX, m_tRect.top - (int)fScrollY,
		(int)m_tInfo.fCX, (int)m_tInfo.fCY,
		hMemDC,
		m_tFrame.m_dwFrameStart * (int)m_tInfo.fCX,
		m_tFrame.m_dwFrameScene * (int)m_tInfo.fCY,
		(int)m_tInfo.fCX, (int)m_tInfo.fCY, REMOVE_PINK);
}

void CEffect::DrawDamage(HDC hDC)
{
//	BitBlt(hDC, 30, 566, 10, 12, hMemDC, 10 * (iCurLv / 10), 0, SRCCOPY);
//	BitBlt(hDC, 30, 566, 10, 12, hMemDC, 10 * (iCurLv / 10), 0, SRCCOPY);
//	BitBlt(hDC, 30, 566, 10, 12, hMemDC, 10 * (iCurLv / 10), 0, SRCCOPY);
//	BitBlt(hDC, 30, 566, 10, 12, hMemDC, 10 * (iCurLv / 10), 0, SRCCOPY);
//	BitBlt(hDC, 30, 566, 10, 12, hMemDC, 10 * (iCurLv / 10), 0, SRCCOPY);
//	BitBlt(hDC, 40, 566, 10, 12, hMemDC, 10 * (iCurLv % 10), 0, SRCCOPY);

}
void CEffect::Release()
{
}

void CEffect::SceneChange()
{
	if (m_eCurState != m_ePreState)
	{
		switch (m_eCurState)
		{
		case eATTACK:

			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 3;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 100;
			break;

		case eBYOEKRYEOK://6���� 300x200
			m_strFrameKey = L"ByeokRyeokEffect";
			m_tInfo.fCX = 300;
			m_tInfo.fCY = 200;
			m_tInfo.fY -= 60;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 5;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 40;
			break;

		case eNOESEONG://5����  200 x210
			m_strFrameKey = L"NoeSeongEffect";
			m_tInfo.fCX = 200;
			m_tInfo.fCY = 210;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 4;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 30;
			break;

		case eSEOMMYEOL://6���� 250 300
				m_strFrameKey = L"SeomMyeolEffect";
			m_tInfo.fCX = 250;
			m_tInfo.fCY = 300;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 5;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 70;//
			break;

		case eTAEPUNG://8���� 256x256 

			m_strFrameKey = L"TaePungEffect";
			m_tInfo.fCX = 256;
			m_tInfo.fCY = 256;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 7;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 50;//800
			break;

		case eSEUNGCHUN://6���� 250X250
			m_strFrameKey = L"SeungChunEffect";
			m_tInfo.fCX = 250;
			m_tInfo.fCY = 250;
			m_tFrame.m_dwFrameStart = 0;
			m_tFrame.m_dwFrameEnd = 5;
			m_tFrame.m_dwFrameTime = GetTickCount();
			m_tFrame.m_dwFrameSpd = 60;//360
			break;
		}
		m_ePreState = m_eCurState;

	}
}
