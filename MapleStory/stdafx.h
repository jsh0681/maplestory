#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             

#include <windows.h>

// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include<list>
#include<vector>
#include<map>
#include<algorithm>
#include<math.h>
#include<time.h>
#include<string.h>
#include <tchar.h>
#include <list>
#include<map>
#include <algorithm>
#include <iostream>
#include <io.h>
using namespace std;
// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.


// FMOD
#include "fmod.h"
#pragma comment(lib, "fmodex_vc.lib")


#ifdef _DEBUG
// Visual Leak Detector
//#include <vld.h>

// 콘솔창 띄우기
#pragma comment(linker,"/entry:wWinMainCRTStartup /subsystem:console")
#endif

// 멀티미디어 재생에 필요한 헤더와 라이브러리
#include <Vfw.h>
#pragma comment (lib, "vfw32.lib")


//사용자 정의 헤더 
#include"Define.h"
#include"Typedef.h"
#include"Extern.h"
#include"Struct.h"
#include"Function.h"
#include"Enum.h"
//#include <vld.h>



//매니저 함수 헤더 
#include"AbstractFactory.h"
#include"ObjMgr.h"
#include"CollisionMgr.h"
#include"MathMgr.h"
#include"LineMgr.h"
#include"ScrollMgr.h"
#include"BmpMgr.h"
#include"KeyMgr.h"
#include"SceneMgr.h"
#include"TileMgr.h"
#include"SoundMgr.h"


