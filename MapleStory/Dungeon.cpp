#include "stdafx.h"
#include "Dungeon.h"
#include "Player.h"
#include "Monster.h"
#include "UI.h"
#include"Skill.h"
#include "Mouse.h"
#include"Item.h"
#include"Player.h"
#include"Obj.h"
#include"Inven.h"


CDungeon::CDungeon()
{
}


CDungeon::~CDungeon()
{
	Release();

}

void CDungeon::Initialize()
{
	//////TILEMGR////////////////////////////////////
	CTileMgr::GetInstance()->LoadDungeonData();

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Map/DungeonBack.bmp", L"DungeonBack");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Map/Dungeon.bmp", L"Dungeon");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/TileRadder5.bmp", L"Tile");

	CSoundMgr::GetInstance()->PlayBGM(L"Bgm_Dungeon.wav");
	
		CObj* pObject = CAbstactFactory<CMonster>::CreateObj(300,1000);
		CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::MONSTER);
	
		pObject = CAbstactFactory<CMonster>::CreateObj(500, 700);
		CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::MONSTER);

		pObject = CAbstactFactory<CMonster>::CreateObj(900, 500);
		CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::MONSTER);

		pObject = CAbstactFactory<CMonster>::CreateObj(400, 300);
		CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::MONSTER);
		
		pObject = CAbstactFactory<CMonster>::CreateObj(700, 1000);
		CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::MONSTER);

		pObject = CAbstactFactory<CMonster>::CreateObj(700, 700);
		CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::MONSTER);

		pObject = CAbstactFactory<CMonster>::CreateObj(1400, 500);
		CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::MONSTER);

		pObject = CAbstactFactory<CMonster>::CreateObj(800, 300);
		CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::MONSTER);

		pObject = CAbstactFactory<CMonster>::CreateObj(700, 1000);
		CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::MONSTER);

		pObject = CAbstactFactory<CMonster>::CreateObj(400, 700);
		CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::MONSTER);

		pObject = CAbstactFactory<CMonster>::CreateObj(1400, 500);
		CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::MONSTER);

		pObject = CAbstactFactory<CMonster>::CreateObj(800, 300);
		CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::MONSTER);

	m_tFrame.m_dwFrameStart = 0;
	m_tFrame.m_dwFrameEnd = 7;
	m_tFrame.m_dwFrameTime = GetTickCount();
	m_tFrame.m_dwFrameSpd = 200;

	g_bTown = false;
	g_bDungeon = true;
	g_bDungeonTwo = false;
	g_bBoss = false;

}

void CDungeon::Update()
{
	CScene::MoveFrame();
	LockScroll();
	m_pPlayer = CObjMgr::GetInstance()->GetPlayer();
	CTileMgr::GetInstance()->Update();
	CObjMgr::GetInstance()->Update();
}

void CDungeon::LateUpdate()
{
	CObjMgr::GetInstance()->LateUpdate();

}

void CDungeon::LockScroll()
{
	if (5 >= CScrollMgr::GetScrollX())
		CScrollMgr::SetScrollX(5.f);

	if (0.f > CScrollMgr::GetScrollY())
		CScrollMgr::SetScrollY(0.f);

	if (DUNGEON_X - WINCX< CScrollMgr::GetScrollX())
		CScrollMgr::SetScrollX(DUNGEON_X - WINCX);

	if (DUNGEON_Y - WINCY < CScrollMgr::GetScrollY())
		CScrollMgr::SetScrollY(DUNGEON_Y - WINCY);
}

void CDungeon::Render(HDC hDC)
{
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();

	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"DungeonBack");
	NULL_CHECK(hMemDC);
	BitBlt(hDC, 0, 0, 1500, 920, hMemDC, (int)fScrollX, (int)fScrollY, SRCCOPY);

	hMemDC = CBmpMgr::GetInstance()->FindImage(L"Dungeon");
	NULL_CHECK(hMemDC);

	GdiTransparentBlt(hDC, 0, 0,
		800, 600,
		hMemDC,
		(int)fScrollX,
		(int)fScrollY,
		800, 600, REMOVE_PINK);

	//CTileMgr::GetInstance()->Render(hDC);

	hMemDC = CBmpMgr::GetInstance()->FindImage(L"Portal");
	NULL_CHECK(hMemDC);
	GdiTransparentBlt(hDC, 1400- (int)fScrollX, 935 - (int)fScrollY, 70, 202, hMemDC, m_tFrame.m_dwFrameStart * 70, 0, 70, 202, REMOVE_PINK);

	hMemDC = CBmpMgr::GetInstance()->FindImage(L"Portal");
	NULL_CHECK(hMemDC);
	GdiTransparentBlt(hDC, 120 - (int)fScrollX, 213 - (int)fScrollY, 70, 202, hMemDC, m_tFrame.m_dwFrameStart * 70, 0, 70, 202, REMOVE_PINK);

	


	CObjMgr::GetInstance()->Render(hDC);


	if (1420 < m_pPlayer->GetInfo().fX && m_pPlayer->GetInfo().fX < 1450 && 1200>m_pPlayer->GetInfo().fY && m_pPlayer->GetInfo().fY > 1000 && g_bTownToDungeon)
	{
		if (CKeyMgr::GetInstance()->KeyPressing(VK_UP))
		{
			m_pPlayer->SetPos(120, 460);
			g_bTownToDungeon = false;
			CSoundMgr::GetInstance()->PlaySoundw(L"Effect_Portal.mp3", CSoundMgr::UI);
			CSoundMgr::GetInstance()->StopSound(CSoundMgr::BGM);
			CSceneMgr::GetInstance()->SceneChange(CSceneMgr::TOWN);
			return; 
		}
	}
	else if ((1420 > m_pPlayer->GetInfo().fX || m_pPlayer->GetInfo().fX > 1450))
	{
		g_bTownToDungeon = true;
	}







	if (130 < m_pPlayer->GetInfo().fX &&m_pPlayer->GetInfo().fX < 170 && 460>m_pPlayer->GetInfo().fY &&m_pPlayer->GetInfo().fY > 360 && g_bDungeonToDungeonTwo)
	{
		if (CKeyMgr::GetInstance()->KeyPressing(VK_UP))
		{
			g_bDungeonToDungeonTwo = false;
			m_pPlayer->SetPos(1150, 260);
			CScrollMgr::SetScrollX(653);
			CScrollMgr::SetScrollY(88);
			CSoundMgr::GetInstance()->PlaySoundw(L"Effect_Portal.mp3", CSoundMgr::UI);
			CSoundMgr::GetInstance()->StopSound(CSoundMgr::BGM);
			CSceneMgr::GetInstance()->SceneChange(CSceneMgr::DUNGEONTWO);
			return;
		}
	}
	else if ((130>m_pPlayer->GetInfo().fX || m_pPlayer->GetInfo().fX > 170))
	{
		g_bDungeonToDungeonTwo = true;
	}


}

void CDungeon::Release()
{
	CObjMgr::GetInstance()->DeleteGroup(CObjMgr::MONSTER);
	CTileMgr::GetInstance()->DestroyInstance();
}