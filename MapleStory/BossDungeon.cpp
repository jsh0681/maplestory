#include "stdafx.h"
#include "BossDungeon.h"
#include"Player.h"
#include"BossMonster.h"
#include"UI.h"


CBossDungeon::CBossDungeon()
{
}


CBossDungeon::~CBossDungeon()
{
	Release();
}

void CBossDungeon::Initialize()
{
	CTileMgr::GetInstance()->LoadBossData();

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Map/BossDungeon.bmp", L"BossDungeon");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Map/BossDungeonBack.bmp", L"BossDungeonBack");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Ui/Portal3.bmp", L"Portal");

	CSoundMgr::GetInstance()->PlayBGM(L"Bgm_Boss.wav");

	CObj* pObject = CAbstactFactory<CBossMonster>::CreateObj();
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::BOSS);


	pObject = CAbstactFactory<CUI>::CreateObj(84.f, 296.f);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::UI);

	m_tFrame.m_dwFrameStart = 0;//비트맵의 그림 시작번호
	m_tFrame.m_dwFrameEnd = 7;//비트맵에서 그림이 있는 마지막 번호
	m_tFrame.m_dwFrameTime = GetTickCount();
	m_tFrame.m_dwFrameSpd = 200;


	g_bTown = false;
	g_bDungeon = false;
	g_bDungeonTwo = false;
	g_bBoss = true;

}

void CBossDungeon::Update()
{
	CScene::MoveFrame();
	m_pPlayer = CObjMgr::GetInstance()->GetPlayer();
	CTileMgr::GetInstance()->Update();
	CObjMgr::GetInstance()->Update();
	ScrollLock();
}

void CBossDungeon::LateUpdate()
{
	CObjMgr::GetInstance()->LateUpdate();
}

void CBossDungeon::Render(HDC hDC)
{
	float fScrollX = CScrollMgr::GetScrollX();
	float fScrollY = CScrollMgr::GetScrollY();

	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"BossDungeonBack");
	NULL_CHECK(hMemDC);
	BitBlt(hDC, 0, 0, 1550, 900, hMemDC, (int)fScrollX, (int)fScrollY, SRCCOPY);
	

	 hMemDC = CBmpMgr::GetInstance()->FindImage(L"BossDungeon");
	NULL_CHECK(hMemDC);
	GdiTransparentBlt(hDC, 0, 0,800, 600,hMemDC,(int)fScrollX,(int)fScrollY,800, 600, REMOVE_PINK);
	

	hMemDC = CBmpMgr::GetInstance()->FindImage(L"Portal");
	NULL_CHECK(hMemDC);
	GdiTransparentBlt(hDC, 84 - (int)fScrollX, 236 - (int)fScrollY, 70, 202, hMemDC, m_tFrame.m_dwFrameStart * 70, 0, 70, 202, REMOVE_PINK);


	CObjMgr::GetInstance()->Render(hDC);
	

	if (100<m_pPlayer->GetInfo().fX &&m_pPlayer->GetInfo().fX < 140&& g_bDungeonTwoToBoss)
	{
		if (CKeyMgr::GetInstance()->KeyPressing(VK_UP))
		{
			m_pPlayer->SetPos(1636, 1220);
			CScrollMgr::SetScrollX(953);
			CScrollMgr::SetScrollY(823);
			g_bDungeonTwoToBoss = false;
			CSoundMgr::GetInstance()->PlaySoundw(L"Effect_Portal.mp3", CSoundMgr::UI);
			CSoundMgr::GetInstance()->StopSound(CSoundMgr::BGM);
			CSceneMgr::GetInstance()->SceneChange(CSceneMgr::DUNGEONTWO);
			return;
		}
	}
	else if (100 > m_pPlayer->GetInfo().fX || m_pPlayer->GetInfo().fX > 140)
	{
		g_bDungeonTwoToBoss = true;
	}
}

void CBossDungeon::Release()
{
	CObjMgr::GetInstance()->DeleteGroup(CObjMgr::BOSS);
	CTileMgr::GetInstance()->DestroyInstance();
}

void CBossDungeon::ScrollLock()
{
	if (5 >= CScrollMgr::GetScrollX())
		CScrollMgr::SetScrollX(5.f);

	if (0.f > CScrollMgr::GetScrollY())
		CScrollMgr::SetScrollY(0.f);

	if (BOSS_DUNGEON_X - WINCX< CScrollMgr::GetScrollX())
		CScrollMgr::SetScrollX(BOSS_DUNGEON_X - WINCX);

	if (BOSS_DUNGEON_Y - WINCY < CScrollMgr::GetScrollY())
		CScrollMgr::SetScrollY(BOSS_DUNGEON_Y - WINCY);
}
