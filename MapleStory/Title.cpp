#include "stdafx.h"
#include "Title.h"
#include"Player.h"
#include"Mouse.h"
#include"Shop.h"
#include"Inven.h"
#include"UI.h"

bool g_bTown;
bool g_bBoss;
bool g_bDungeon;
bool g_bDungeonTwo;

CTitle::CTitle()
{
}


CTitle::~CTitle()
{
	Release();
}

void CTitle::Initialize()
{
	CObj* pObject = CAbstactFactory<CPlayer>::CreateObj(60.f, 420.f);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::PLAYER);
	
	 pObject = CAbstactFactory<CMouse>::CreateObj();
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::MOUSE);

	pObject = CAbstactFactory<CUI>::CreateObj(84.f, 296.f);
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::UI);

	pObject = CAbstactFactory<CShop>::CreateObj();
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::SHOP);

	pObject = CAbstactFactory<CInven>::CreateObj();
	CObjMgr::GetInstance()->AddObject(pObject, CObjMgr::INVEN);

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Title/Title2.bmp", L"Title");
}

void CTitle::Update()
{
	
}

void CTitle::LateUpdate()
{
}

void CTitle::Render(HDC hDC)
{
	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Title");
	NULL_CHECK(hMemDC);

	BitBlt(hDC, 0, 0, WINCX, WINCY, hMemDC, 0, 0, SRCCOPY);

	if (CKeyMgr::GetInstance()->KeyPressing(VK_SPACE))
	{
		CSoundMgr::GetInstance()->StopSound(CSoundMgr::BGM);
		CSceneMgr::GetInstance()->SceneChange(CSceneMgr::TOWN);
		return;
	}
}

void CTitle::Release()
{

}
