#pragma once
class CTile;
class CTileMgr
{
	DECLARE_SINGLETON(CTileMgr)
private:
	CTileMgr();
	~CTileMgr();

public:
	void Initialize();
	void Render(HDC hDC);
	void Update();
	void Release();
public:
	void TileChange(const POINT & pt, int iDrawID, int iOption);
	void TileChange(const POINT & pt, int iDrawID);	
	void SaveData();
	void LoadTownData();
	void LoadDungeonData();
	void LoadDungeonTwoData();
	void LoadBossData();
};

