#pragma once
#include "Obj.h"
class CInven :
	public CObj
{
	OBJVEC m_vecItem;
public:
	CInven();
	virtual ~CInven();

	// CObj을(를) 통해 상속됨
	virtual void Initialize() override;
	void KeyCheck();
	void DrawInven(HDC hDC);
	virtual void LateInit() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render(HDC hDC) override;
	virtual void Release() override;
	OBJVEC& GetVecItem() { return m_vecItem; }

public:
	void SetItem(CObj* pItem) { m_vecItem.push_back(pItem); };
};

