#include "stdafx.h"
#include "MyMenu.h"
#include "MyButton.h"

CMyMenu::CMyMenu()
{
}


CMyMenu::~CMyMenu()
{
	Release();
}

void CMyMenu::Initialize()
{
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Button/Start.bmp", L"Start");

	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Button/Edit.bmp", L"Edit");
	CBmpMgr::GetInstance()->InsertBmp(L"../Resource/Button/Exit.bmp", L"Exit");

	CObj* pButton = CAbstactFactory<CMyButton>::CreateObj(200.f, 400.f);
	dynamic_cast<CMyButton*>(pButton)->SetFrameKey(L"Start");
	CObjMgr::GetInstance()->AddObject(pButton, CObjMgr::UI);

	pButton = CAbstactFactory<CMyButton>::CreateObj(400.f, 400.f);
	dynamic_cast<CMyButton*>(pButton)->SetFrameKey(L"Edit");
	CObjMgr::GetInstance()->AddObject(pButton, CObjMgr::UI);

	pButton = CAbstactFactory<CMyButton>::CreateObj(600.f, 400.f);
	dynamic_cast<CMyButton*>(pButton)->SetFrameKey(L"Exit");
	CObjMgr::GetInstance()->AddObject(pButton, CObjMgr::UI);
}

void CMyMenu::Update()
{
	CObjMgr::GetInstance()->Update();
}

void CMyMenu::LateUpdate()
{
	CObjMgr::GetInstance()->LateUpdate();
}

void CMyMenu::Render(HDC hDC)
{
	CObjMgr::GetInstance()->Render(hDC);
}

void CMyMenu::Release()
{
	CObjMgr::GetInstance()->DeleteGroup(CObjMgr::UI);
}
