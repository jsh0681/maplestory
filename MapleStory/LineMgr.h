#pragma once
class CLine;

class CLineMgr
{
	DECLARE_SINGLETON(CLineMgr)
private:
	CLineMgr();
	~CLineMgr();
public:
	void Initialize();
	void Render(HDC hDC);
	void Release();

	bool CollisionLine(float fInX, float fInY, float * pOutY);

public:
	bool CollisionLine(float fInX, float* pOutY);
	void LoadData();
private:

	list<CLine*>		m_LineLst;
};

