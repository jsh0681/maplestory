#pragma once
class CTile
{
public:
	CTile();
	~CTile();


	const TILEINFO& GetInfo() const { return m_tInfo; }

	const RECT& GetRect() const { return m_tRect; }
public:
	void SetInfo(const TILEINFO& tInfo) { m_tInfo = tInfo; }
	void SetPos(float x, float y) { m_tInfo.fX = x, m_tInfo.fY = y; }
	void SetDrawID(int iDrawID) { m_tInfo.iDrawID = iDrawID; }
	void SetOption(int iOption) { m_tInfo.iOption = iOption; }
public:
	void Initialize();
	void Render(HDC hDC);
	void Release();
private:
	void UpdateRect();
private:

	TILEINFO	m_tInfo;
	RECT		m_tRect;
};

