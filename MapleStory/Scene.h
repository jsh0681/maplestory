#pragma once
class CObj;

class CScene
{
protected:
	CObj* m_pPlayer;
	INFO m_tInfo;
	FRAME	m_tFrame;
public:
	CScene();
	virtual ~CScene();
	void MoveFrame();
public:
	virtual void Initialize()	= 0;
	virtual void Update() = 0;
	virtual void LateUpdate() = 0;
	virtual void Render(HDC hDC) = 0;
	virtual void Release() = 0;

};

