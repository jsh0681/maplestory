#include "stdafx.h"
#include "Tile.h"


CTile::CTile()
{
}


CTile::~CTile()
{
	Release();
}

void CTile::Initialize()
{
	m_tInfo.fCX = float(TILECX);
	m_tInfo.fCY = float(TILECY);
	m_tInfo.iDrawID = 0;
	m_tInfo.iOption = BACK;
}

void CTile::Render(HDC hDC)
{
	UpdateRect();

	HDC hMemDC = CBmpMgr::GetInstance()->FindImage(L"Tile");
	NULL_CHECK(hMemDC);

	int iScrollX = (int)CScrollMgr::GetScrollX();
	int iScrollY = (int)CScrollMgr::GetScrollY();
	//BitBlt(hDC, m_tRect.left-iScrollX, m_tRect.top-iScrollY, m_tInfo.fCX, m_tInfo.fCY, hMemDC, m_tInfo.iDrawID*m_tInfo.fCX, 0, SRCCOPY);

	GdiTransparentBlt(hDC, m_tRect.left - (int)iScrollX, m_tRect.top - (int)iScrollY,
		m_tInfo.fCX,m_tInfo.fCY,
		hMemDC,
		m_tInfo.iDrawID*m_tInfo.fCX,
		0,
		m_tInfo.fCX, m_tInfo.fCY, REMOVE_PINK);

}

void CTile::Release()
{
}

void CTile::UpdateRect()
{
	m_tRect.left = LONG(m_tInfo.fX - m_tInfo.fCX * 0.5f);
	m_tRect.right = LONG(m_tInfo.fX + m_tInfo.fCX * 0.5f);
	m_tRect.top = LONG(m_tInfo.fY - m_tInfo.fCY * 0.5f);
	m_tRect.bottom = LONG(m_tInfo.fY + m_tInfo.fCY * 0.5f);
}
