#pragma once
class CScene;

class CSceneMgr
{
public:
	enum SCENE_ID { LOGO,TITLE,CREATE, TOWN, DUNGEON,DUNGEONTWO, BOSSDUNGEON, EDIT,MENU, end };
	DECLARE_SINGLETON(CSceneMgr)

private:
	CSceneMgr();
	~CSceneMgr();

public:
		void SceneChange(SCENE_ID eID);
		void Update();
		void LateUpdate();
		void Render(HDC hDC);
		void Release();

private:
	CScene*		m_pScene;//���� ��
	SCENE_ID	m_eCurScene;//������� ��ġ
	SCENE_ID	m_ePreScene;//���� ���� ��ġ
};

