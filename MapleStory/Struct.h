#pragma once
typedef struct tagInfo 
{
	float fX;//x좌표
	float fY;//y좌표
	float fCX;//크기x출
	float fCY;//크기y축

}INFO;

typedef struct tagObjInfo
{
	char strName[NAME_LEN] ;//닉네임
	char strClass[NAME_LEN] ;//직업
	int iLv;			//레벨
	float fHp;			//체력
	float fMaxHp;		//최대 체력
	float fMp;			//마나
	float fMaxMp;		//최대 마나
	float fAtk;			//공격력
	float fExp;			//경험치
	float fMaxExp;		//레벨업 경험치
	float fMoney;		//골드
}OBJINFO;


typedef struct tagItemInfo
{
	char strName[NAME_LEN];//닉네임
	char strClass[NAME_LEN];//직업
	float fHp;
	float fMaxHp;		//최대 체력
	float fMp;
	float fMaxMp;		//최대 마나
	float fAtk;			//공격력
	float fMoney;		//골드
}ITEMINFO;




typedef struct tagLinePos
{
	tagLinePos()
		: fX(0.f), fY(0.f) {}

	tagLinePos(float x, float y)
		: fX(x), fY(y) {}

	float fX;
	float fY;
}LINEPOS;


typedef struct tagLineInfo
{
	tagLineInfo() {}
	tagLineInfo(const LINEPOS& left, const LINEPOS& right)
		: tLeft(left), tRight(right) {}

	LINEPOS	tLeft;
	LINEPOS	tRight;
}LINEINFO;

typedef struct tagFrame
{
	DWORD m_dwFrameStart;	// 스프라이트 이미지의 x축 시작점.
	DWORD m_dwFrameEnd;		// 스프라이트 이미지의 x축 끝점.
	DWORD m_dwFrameScene;	// 스프라이트 이미지의 y축

	DWORD m_dwFrameTime;
	DWORD m_dwFrameSpd;		// 애니메이션 재생 속도

}FRAME;


typedef struct tagTile
{
	float fX;
	float fY;
	float fCX;
	float fCY;

	int iDrawID;
	int iOption;
}TILEINFO;

typedef struct tagScroll
{
	float fX;
	float fY;
}SCROLL;